package info.bitcoinunlimited.multiplatform.platform

internal expect class PlatformLogger() {

    fun info(tag: String, message: String)

    fun debug(tag: String, message: String)

    fun warning(tag: String, message: String)

    fun error(tag: String, message: String)
}

public object Logger {

    private val logger = PlatformLogger()

    public fun info(tag: String, message: String) {
        logger.info(tag, message)
    }

    public fun debug(tag: String, message: String) {
        logger.debug(tag, message)
    }

    public fun warning(tag: String, message: String) {
        logger.warning(tag, message)
    }

    public fun error(tag: String, message: String) {
        logger.error(tag, message)
    }
}