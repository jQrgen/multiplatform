#ifndef LIB2_H_INCLUDED
#define LIB2_H_INCLUDED

void ints(char c, short d, int e, long f);
void units(unsigned char c, unsigned char d, unsigned long f);
void doubles(float a, double b);

#endif