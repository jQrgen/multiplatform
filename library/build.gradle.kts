plugins {
    kotlin("multiplatform")
    id("com.android.library")
    kotlin("plugin.serialization")
    id("org.jetbrains.kotlin.native.cocoapods")
}

group = "info.bitcoinunlimited"
version = "1.0.0"

repositories {
    google()
}

kotlin {
    cocoapods {
        // Specify the required Pod version here. Otherwise, the Gradle project version is used.
        summary = "Some description for the Shared Module"
        homepage = "Link to the Shared Module homepage"
        version = "1.0"
        ios.deploymentTarget = "14.1"

        // Configure the Pod name here instead of changing the Gradle project name
        name = "BitcoinUnlimitedCocoaPod"

        framework {
            // Framework name configuration. Use this property instead of deprecated 'frameworkName'
            baseName = "library"
        }

        // Maps custom Xcode configuration to NativeBuildType
        xcodeConfigurationToNativeBuildType["CUSTOM_DEBUG"] = org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType.DEBUG
        xcodeConfigurationToNativeBuildType["CUSTOM_RELEASE"] = org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType.RELEASE
    }

    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native") {
            val main by compilations.getting
            val interop by main.cinterops.creating

            binaries {
                executable()
            }
        }
        hostOs == "Linux" -> linuxX64("native") {
            val main by compilations.getting
            val interop by main.cinterops.creating

            binaries {
                executable()
            }
        }
        isMingwX64 -> mingwX64("native") {
            val main by compilations.getting
            val interop by main.cinterops.creating

            binaries {
                executable()
            }
        }
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")

    }

    android()

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.2")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting
        val jvmTest by getting
        val nativeMain by getting
        val nativeTest by getting
        val androidMain by getting {
            dependencies {
                implementation("com.google.android.material:material:1.5.0")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation("junit:junit:4.13.2")
            }
        }
    }
}

android {
    compileSdkVersion(32)
    sourceSets["main"].manifest.srcFile("../android/src/main/AndroidManifest.xml")
    defaultConfig {
        // applicationId = "info.bitcoinunlimited.library"
        minSdkVersion(26)
        targetSdk = 30
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}
dependencies {
    implementation("androidx.core:core-ktx:+")
}
