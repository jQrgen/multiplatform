#!/usr/bin/env python3
from pathlib import Path
import logging
import os
import sys
import urllib.request
import zipfile
import argparse

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "build")

RINGSIG_NAME = "fujisaki-ringsig"
RINGSIG_RELEASE = "https://github.com/dagurval/fujisaki-ringsig/archive/master.zip"
RINGSIG_ARCHIVE_NAME = "ringsig.zip"
RINGSIG_ARCHIVE_PATH = os.path.join(ROOT_DIR, RINGSIG_ARCHIVE_NAME)
RINGSIG_SRC_PATH = os.path.join(ROOT_DIR, RINGSIG_NAME)
RINGSIG_ROOT_DIR = os.path.join(RINGSIG_SRC_PATH, "fujisaki-ringsig-master")

parser = argparse.ArgumentParser(description='Build fujisaki-ringsig library for Android.')
parser.add_argument('--ndk_root', help='Path to Android NDK', required=True)
NDK_HOME = parser.parse_args().ndk_root

logging.basicConfig(stream = sys.stdout, level = logging.DEBUG)
logger = logging.getLogger("build")

# Fake User-Agent, urllib gets 403 forbidden from Gitlab
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
urllib.request.install_opener(opener)

def find_compiler_dir():
    import platform
    if platform.system() == "Darwin":
        host_folder = "darwin-x86_64"
    elif platform.system() == "Linux":
        host_folder = 'linux-x86_64'
    else:
        raise Exception(f"Platform folder is not configured for {platform.system()} in {__file__}")

    compiler_dir = os.path.join(NDK_HOME, "toolchains", "llvm", "prebuilt", host_folder, "bin")
    logger.info(f"Looking for compiler in path: {compiler_dir}")
    if os.path.exists(compiler_dir):
        logger.info(f"Found compiler path {compiler_dir}")
        return compiler_dir

    # Could also be in NDK_HOME/<ndk version>
    import glob
    version_dirs = glob.glob(f"{NDK_HOME}/[0-9]*")

    # Try newest version first
    version_dirs.sort(reverse = True)

    for d in version_dirs:
        compiler_dir = os.path.join(d, "toolchains", "llvm", "prebuilt", host_folder, "bin")
        logger.info(f"Looking for compiler in path: {compiler_dir}")
        if os.path.exists(compiler_dir):
            logger.info(f"Found compiler path {compiler_dir}")
            return compiler_dir

    raise Exception(f"Unable to find Android toolchains")


def get_ndk_toolchain_env():
    if not os.path.exists(NDK_HOME):
        raise Exception(f"NDK_HOME path '{NDK_HOME}' does not exist")

    compiler_dir = find_compiler_dir()
    assert os.path.exists(compiler_dir)

    # This is usually set in .cargo/config.toml, but cargo also supports environment settings.
    # Environment settings override config.toml settings.

    env = os.environ

    def bin_path(tool):
        path = os.path.join(NDK_HOME, compiler_dir, tool)
        logger.warning(path)
        if not os.path.exists(path):
            raise Exception(f"Could not find tool {tool}, expected path: {path}")
        return path

    # [target.aarch64-linux-android]
    # ar = ".NDK/arm64/bin/aarch64-linux-android-ar"
    # linker = ".NDK/arm64/bin/aarch64-linux-android-clang"
    env["CARGO_TARGET_AARCH64_LINUX_ANDROID_AR"] = bin_path("aarch64-linux-android-ar")
    env["CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER"] = bin_path("aarch64-linux-android26-clang")

    # [target.armv7-linux-androideabi]
    # ar = ".NDK/arm/bin/arm-linux-androideabi-ar"
    # linker = ".NDK/arm/bin/arm-linux-androideabi-clang"
    env["CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_AR"] = bin_path("arm-linux-androideabi-ar")
    env["CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER"] = bin_path("armv7a-linux-androideabi26-clang")

    # [target.i686-linux-android]
    # ar = ".NDK/x86/bin/i686-linux-android-ar"
    # linker = ".NDK/x86/bin/i686-linux-android-clang"
    env["CARGO_TARGET_I686_LINUX_ANDROID_AR"] = bin_path("i686-linux-android-ar")
    env["CARGO_TARGET_I686_LINUX_ANDROID_LINKER"] = bin_path("i686-linux-android26-clang")

    #[target.x86_64-linux-android]
    env["CARGO_TARGET_X86_64_LINUX_ANDROID_AR"] = bin_path("x86_64-linux-android-ar")
    env["CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER"] = bin_path("x86_64-linux-android26-clang")
    return env

def download_ringsig():
    if not os.path.exists(RINGSIG_ARCHIVE_PATH):
        logger.info(f"Fetching {RINGSIG_NAME} from to {RINGSIG_ARCHIVE_PATH}")
        urllib.request.urlretrieve(RINGSIG_RELEASE, RINGSIG_ARCHIVE_PATH)
    else:
        logger.info(f"Using existing {RINGSIG_NAME} at {RINGSIG_ARCHIVE_PATH}")

    logger.info(f"Extracting {RINGSIG_ARCHIVE_NAME} to {RINGSIG_ARCHIVE_PATH}")
    with zipfile.ZipFile(RINGSIG_ARCHIVE_PATH, 'r') as fh:
        fh.extractall(RINGSIG_SRC_PATH)


def output_reader(pipe, queue):
    try:
        with pipe:
            for l in iter(pipe.readline, b''):
                queue.put(l)
    finally:
        queue.put(None)

def run_cmd(*, cmd, args, cwd, env = None):
    import subprocess
    from threading import Thread
    from queue import Queue

    args = [cmd] + args
    logger.info("Running %s", args)

    p = subprocess.Popen(args, cwd = cwd,
        stdout = subprocess.PIPE, stderr = subprocess.PIPE, env = env)

    q = Queue()
    Thread(target = output_reader, args = [p.stdout, q]).start()
    Thread(target = output_reader, args = [p.stderr, q]).start()

    for line in iter(q.get, None):
        logger.info(line.decode('utf-8').rstrip())

    p.wait()
    rc = p.returncode
    assert rc is not None
    if rc != 0:
        raise Exception(f"{args} failed with return code {rc}")

def run_ringsig_build():
    targets = [
        'aarch64-linux-android',
        'armv7-linux-androideabi',
        'i686-linux-android',
        'x86_64-linux-android',
    ]
    env = get_ndk_toolchain_env()
    for t in targets:
        try:
            run_cmd(
                cmd = "cargo",
                args = ["build", "--release", f"--target={t}"],
                cwd = RINGSIG_ROOT_DIR,
                env = env)
        except Exception as e:
            logger.warning(f"building {RINGSIG_NAME} failed. Is Rust installed? https://rustup.rs")
            raise e

def run_generate_headers():
    try:
        run_cmd(
            cmd = "bash",
            args = ["./generate-c-header.sh"],
            cwd = RINGSIG_ROOT_DIR)
    except Exception as e:
        logger.warning("failed to generate C headers")
        raise e

def main():
    Path(ROOT_DIR).mkdir(parents = True, exist_ok = True)
    download_ringsig()
    run_ringsig_build()
    run_generate_headers()

main()
