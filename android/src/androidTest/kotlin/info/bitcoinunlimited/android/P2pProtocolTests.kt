package info.bitcoinunlimited.android
// TODO: Resolve NexaRpc dependency
/*
import Nexa.NexaRpc.HashId
import Nexa.NexaRpc.NexaRpc
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.*
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.channels.*

import kotlinx.coroutines.*
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import Nexa.NexaRpc.NexaRpcFactory

class P2pProtocolTests
{
    companion object
    {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary(blockchainLibName)
            Initialize.LibBitcoinCash(ChainSelector.NEXAREGTEST.v)
        }
    }

    val chainSelector = ChainSelector.NEXAREGTEST
    val port = NexaRegtestPort

    @Test
    fun connectToP2P()
    {
        Logger.info(TAG_ANDROID_TEST,"This test requires a bitcoind full node running on regtest at ${EMULATOR_HOST_IP}:${port}")

        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)

        var cnxn = P2pClient(chainSelector, EMULATOR_HOST_IP, port, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { cnxn.processForever() }

        var txValResponses = 0

        cnxn.waitForReady()

        cnxn.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = Hash256()
        var headers: MutableList<out iBlockHeader>? = null
        val waiter = ThreadCond()
        cnxn.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delay(5000)
        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            Logger.info(TAG_ANDROID_TEST,hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        var tx : NexaTransaction = NexaTransaction(chainSelector)
        cnxn.sendTxVal(tx) {
            s -> Logger.info(TAG_ANDROID_TEST,"tx Validation response: " + s)
            check("bad-txns-vout-empty" in s)
            check("txn-undersize" in s)
            txValResponses++
        }

        var rawAddr: ByteArray = ByteArray(20)
        tx._outputs.add(NexaTxOutput(10000, SatoshiScript.p2pkh(rawAddr, chainSelector)))
        cnxn.sendTxVal(tx) {
            s -> Logger.info(TAG_ANDROID_TEST,"tx2 Validation response: " + s)
            check("txn-undersize" in s)
            check("Coinbase is only valid in a block" in s)
            check("mempool min fee not met" in s)
            txValResponses++
        }

        tx._inputs.add(NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(Hash256()), 10000),SatoshiScript(chainSelector) + OP.TRUE))
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, chain))
        cnxn.sendTxVal(tx) {
            s -> Logger.info(TAG_ANDROID_TEST,"tx3 Validation response: " + s)
            check("input-does-not-exist" in s)
            check("inputs-are-missing" in s)
            txValResponses++
        }

        // set up a quick way to get blocks back from a node
        var blk: iBlock? = null
        val blkChannel = Channel<iBlock>()
        cnxn.onBlockCallback.add({ incomingblk, _ -> blkChannel.send(incomingblk) })

        // Spend an immature coinbase
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 1].hash)))
        launch { blk = blkChannel.receive() }
        while(blk == null) Thread.sleep(100)

        // Spend the coinbase of a block that can't be spent yet
        tx._inputs[0] = NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(blk!!.txes[0].idem, 0), 5000000000),SatoshiScript(chainSelector) + OP.TRUE)
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, chain))
        cnxn.sendTxVal(tx) {
            s -> Logger.info(TAG_ANDROID_TEST,"tx4 Validation response: " + s)
            check("bad-txns-premature-spend-of-coinbase" in s)
            check("mandatory-script-verify-flag-failed" in s)
            txValResponses++
        }

        // Since I'm looking now at the coinbase 100 blocks ago, this must be spendable and unspent
        blk = null
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 100].hash)))
        launch { blk = blkChannel.receive() }
        while(blk == null) Thread.sleep(100)

        // Spend the coinbase of a block that just matured, so we know it can't have been spent.  Spend too much to ensure that we still check sigs
        tx._outputs[0].amount = 10000000000
        tx._inputs[0] = NexaTxInput(chainSelector, Spendable(chainSelector, NexaTxOutpoint(blk!!.txes[0].idem, 0), 1000000000),SatoshiScript(chainSelector) + OP.TRUE)
        cnxn.sendTxVal(tx) {
            s -> Logger.info(TAG_ANDROID_TEST,"tx5 Validation response: " + s)
            check("mandatory-script-verify-flag-failed (Bad template operation)" in s)
            check("bad-txns-in-belowout" in s)
            txValResponses++
            waiter.wake()
        }

        waiter.delay(30000)

        if (txValResponses == 0) LogIt.severe("TX validation request is being ignored.  DID YOU ENABLE IT IN THE FULL NODE? (by setting net.allowp2pTxVal=1)")
        check(txValResponses == 5)
        Logger.info(TAG_ANDROID_TEST,"shutting down")
        cnxn.close()
        Logger.info(TAG_ANDROID_TEST,"TestCompleted")
    }


    @Test
    fun testRpc()
    {
        Logger.info(TAG_ANDROID_TEST,"This test requires a full node running on regtest at ${EMULATOR_HOST_IP} and port ${NexaRegtestRpcPort}")

        // Set up RPC connection
        val rpcConnection = "http://" + SimulationHostIP + ":" + NexaRegtestRpcPort
        Logger.info(TAG_ANDROID_TEST,"Connecting to: " + rpcConnection)

        val nexaRpc: NexaRpc = NexaRpcFactory.create(rpcConnection)

        // Try the hard way (manual parsing returned parameter)
        val retje = nexaRpc.callje("listunspent")
        val result = (retje as JsonObject)["result"] as JsonArray
        check(result.size > 0)  // This could fail if you haven't created at least 101 blocks

        // Try the easy way
        val ret = nexaRpc.listunspent()
        check(ret.size > 0)
    }

    @Test
    fun rpcAndWalletSetup()
    {
        Logger.info(TAG_ANDROID_TEST,"This test requires a bitcoind full node running on " +
                "regtest at ${EMULATOR_HOST_IP} and ports ${port} and ${NexaRegtestRpcPort}")

        val rpcConnection = "http://" + SimulationHostIP + ":" + NexaRegtestRpcPort
        Logger.info(TAG_ANDROID_TEST,"Connecting to: " + rpcConnection)
        val rpc = NexaRpcFactory.create(rpcConnection)
        var unspent = rpc.listunspent()
        println(unspent.toString())

        // Generate blocks until we get coins to spend. This is needed inside the ci testing.
        // But the code checks first so that lots of extra blocks aren't created during dev testing
        val peerinfo:String = rpc.calls("getpeerinfo")
        println(peerinfo)

        // Generate blocks until there is some mature balance in this wallet
        var rpcBalance = rpc.getbalance()
        Logger.info(TAG_ANDROID_TEST,rpcBalance.toPlainString())
        while (rpcBalance < BigDecimal(50))
        {
            rpc.generate(1)
            rpcBalance = rpc.getbalance()
        }


        // Set up connection between full and light nodes
        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)
        var client = P2pClient(chainSelector, EMULATOR_HOST_IP, port, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { client.processForever() }
        client.waitForReady()
        client.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        // Set up blockchain
        var applicationContext = ApplicationProvider.getApplicationContext<Context>()
        val ctxt = PlatformContext(applicationContext)
        val cnxnMgr: CnxnMgr = MultiNodeCnxnMgr("regtest", chainSelector, arrayOf(EMULATOR_HOST_IP))
        val chain = Blockchain(
                chainSelector,
                "regtest",
                cnxnMgr,
                // If checkpointing the genesis block, set the prior block id to the genesis block as well
                Hash256(),
                Hash256(),
                Hash256(),
                0,
                0.toBigInteger(),
                ctxt, dbPrefix
        )
        val chainThread = Thread {
            println("Starting blockchain")
            chain.start()
        }
        chainThread.start()

        // Set up wallet
        val kvp = OpenKvpDB(ctxt, "db")
        val wallet = Bip44Wallet(kvp!!, "test", chainSelector, "secret word")
        wallet.addBlockchain(chain, chain.checkpointHeight, 0)
        val walletThread = Thread {
            println("Starting wallet")
            wallet.run()
        }
        walletThread.start()

        // Generate new address
        val addr = wallet.getnewaddress()

        // Add address to bloom filter and send it to full node
        val data = Array<Any>(1, { Unit })
        data[0] = addr.data;
        val bloom = Wallet.CreateBloomFilter(data, 0.1, data.size * 10, Wallet.MAX_BLOOM_SIZE, Wallet.Companion.BloomFlags.BLOOM_UPDATE_ALL.v, 1)
        client.sendBloomFilter(bloom, 1)

        // Full node sends 1000 satoshi to light
        val spend = BigDecimal(1000)
        Logger.info(TAG_ANDROID_TEST,"Sending ${spend} to address ${addr.toString()}")
        rpc.sendtoaddress(addr.toString(), spend)

        // Full node generates a block
        val generated: List<HashId> = rpc.generate(1)
        val latestBlock = Hash256(generated[0].hash)

        // Light client asks for header of new block
        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = latestBlock
        var headers: MutableList<out iBlockHeader>? = null
        val waiter = ThreadCond()
        client.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delay(5000)

        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            Logger.info(TAG_ANDROID_TEST,hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        // Cleanup
        Logger.info(TAG_ANDROID_TEST,"shutting down")
        wallet.stop()
        chain.stop()
        client.close()
        walletThread.join()
        chainThread.join()
        Logger.info(TAG_ANDROID_TEST,"TestCompleted")
    }
}
*/