package info.bitcoinunlimited.android.vote

import com.google.common.truth.Truth.assertThat
import info.bitcoinunlimited.android.*
import org.junit.Test
import kotlin.random.Random
import kotlin.test.assertContentEquals
import kotlin.test.assertFailsWith

class RingSignatureVoteTests {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.NEXA.v)
        }
    }

    val allContracts = VoteTestUtil.ringSignatureVoteInstances()
    val chain: ChainSelector = ChainSelector.NEXA
    val privateKey: ByteArray = VoteTestUtil.mocPrivateKey()
    val changeAddress: PayAddress = Pay2PubKeyHashDestination(chain, UnsecuredSecret(VoteTestUtil.mocPrivateKey('B'))).address
    val payload = "dummy vote payload".toByteArray()
    val payloadDeployPrivKey = VoteTestUtil.mocPrivateKey('Z')
    val payloadDeployPK = VoteTestUtil.mocPublicKey(payloadDeployPrivKey)

    /**
     * Seed to private key used for voting in tests.
     */
    val privateKeySeed = allContracts.second[0]

    /**
     * Test that when input is enough, but low, that all coins are sent to the
     * contract.
     */
    @Test
    fun fundWithoutChangeOutput() {
        val contract = allContracts.first.get(0)
        // 1 sat below adding change output
        val inputAmount = contract.calculateFundingAmountRequired(1) + RingSignatureVote.MIN_FUNDING_CHANGE_AMOUNT - 1
        val coin = VoteTestUtil.mocInput(this.chain, UnsecuredSecret(this.privateKey), inputAmount.toLong())

        val (tx, _) = contract.fundPayloadContract(
            chain,
            arrayOf(coin), payloadDeployPrivKey, payload, changeAddress
        )
        assertThat(tx.inputs.size).isEqualTo(1)

        assertThat(tx.outputs.size).isEqualTo(1)
        assertThat(tx.outputs[0].amount).isEqualTo(RingSignatureVote.minimumPayloadContractInput())

        val expectedLockingScript = PayloadContract.lockingScript(
            chain,
            payloadDeployPK,
            PayloadContract.calcPayloadHash(payloadDeployPK, payload)
        )

        assertThat(tx.outputs[0].script.flatten()).isEqualTo(expectedLockingScript.flatten())
    }

    /**
     * Test that change output is created if we have >= MIN_FUNDING_CHANGE_AMOUNT extra sats in the input.
     */
    @Test
    fun fundWithChangeOutput() {
        val contract = allContracts.first.get(0)
        val inputAmount = contract.calculateFundingAmountRequired(1) + RingSignatureVote.MIN_FUNDING_CHANGE_AMOUNT
        val coin = VoteTestUtil.mocInput(this.chain, UnsecuredSecret(this.privateKey), inputAmount.toLong())
        val (tx, redeemScript) = contract.fundPayloadContract(
            chain,
            arrayOf(coin), payloadDeployPrivKey, payload, changeAddress
        )
        assertThat(tx.outputs.size).isEqualTo(2)
        assertThat(tx.outputs[0].amount).isEqualTo(RingSignatureVote.minimumPayloadContractInput())

        val expectedLockingScript = PayloadContract.lockingScript(
            chain,
            payloadDeployPK,
            PayloadContract.calcPayloadHash(payloadDeployPK, payload)
        )

        assertThat(tx.outputs[0].script.flatten()).isEqualTo(expectedLockingScript.flatten())
        assertThat(tx.outputs[1].amount).isEqualTo(RingSignatureVote.MIN_FUNDING_CHANGE_AMOUNT)
        assertThat(tx.inputs.size).isEqualTo(1)
    }

    /**
     * Test that exception is thrown when input is too small.
     */
    @Test
    fun fundTooLowInput() {
        val contract = allContracts.first.get(0)

        // 1 sat below limit
        val inputAmount = contract.calculateFundingAmountRequired(1) - 1

        // IllegalArgumentException because input is too low.
        assertFailsWith(IllegalArgumentException::class) {
            val coin = VoteTestUtil.mocInput(this.chain, UnsecuredSecret(this.privateKey), inputAmount.toLong())
            contract.fundPayloadContract(chain, arrayOf(coin), payloadDeployPK, payload, changeAddress)
        }
    }

    /**
     * Test that exception is thrown when invalid option for contract is passed.
     */
    @Test
    fun castInvalidOption() {
        val contract = allContracts.first.get(0)

        val invalidOption = ByteArray(20) { _ -> 'F'.code.toByte() }
        val mocSignature = ByteArray(600)

        assertFailsWith(IllegalArgumentException::class) {
            val coin = VoteTestUtil.mocInput(chain, UnsecuredSecret(privateKey), 100)
            contract.castVoteTransaction(
                chain,
                arrayOf(Pair(coin, invalidOption + mocSignature)),
                true
            )
        }
    }

    /**
     * Test casting a valid vote.
     */
    @Test
    fun castVoteTest() {
        val contract = allContracts.first[0]

        val inputAmount = contract.calculateFundingAmountRequired(1)
        val fundCoin = VoteTestUtil.mocInput(chain, UnsecuredSecret(privateKey), inputAmount.toLong())
        val vote = contract.createVotePayload(
            contract.hashVote(contract.voteOptions[0]),
            privateKeySeed
        )
        val (_, payloadContractInputs) = contract.fundPayloadContract(chain, arrayOf(fundCoin), payloadDeployPrivKey, vote, changeAddress)

        val castTx = contract.castVoteTransaction(chain, payloadContractInputs, false)

        assertThat(castTx.inputs.size).isEqualTo(1)

        // Check that the casting tx used the payload contract correctly.
        val payloadParsed = PayloadContract.getPayloadFromTx(chain, castTx)
        assertThat(payloadParsed).isEqualTo(vote)

        assertThat(castTx.outputs.size).isEqualTo(1)
        assertThat(castTx.outputs[0].amount).isEqualTo(NetworkConstants.DEFAULT_DUST_THRESHOLD)
        assertThat(castTx.outputs[0].script).isEqualTo(
            contract.getNotificationAddress(chain).constraintScript()
        )
    }

    @Test
    fun largePayloadSizeTest() {
        val numberOfParticipants = 1024

        val salt = "unittest".toByteArray()
        val description = "Foo?"
        val options = arrayOf("Bar", "Baz", "Qux")
        val beginHeight = 100
        val endHeight = 200

        val participantSeeds = (1..numberOfParticipants).map {
            ByteArray(32) { _ -> it.toByte() }
        }
        val participantPubkeys = participantSeeds.map {
            val keypair = RingSignature.newKeyPairFromBits(it)
            val pubkey = RingSignature.getPubKey(keypair)
            RingSignature.freeKeyPair(keypair)
            pubkey
        }.toTypedArray()

        val contract = RingSignatureVote(
            salt, description, beginHeight, endHeight,
            options,
            participantPubkeys
        )

        val payload = contract.createVotePayload(
            contract.hashVote(options[0]),
            participantSeeds[0]
        )

        assertThat(payload.size).isEqualTo(20 /* vote */ + 65584 /* signature */)
    }

    @Test
    fun calculateInputsRequiredTests() {
        assertThat(1.toInt()).isEqualTo(RingSignatureVoteBase.calculateInputsRequired(1))
        assertThat(1.toInt()).isEqualTo(RingSignatureVoteBase.calculateInputsRequired(22))
        assertThat(2.toInt()).isEqualTo(RingSignatureVoteBase.calculateInputsRequired(23))
    }

    @Test
    fun getPayloadContractAddressesTest() {

        // 1 byte payload
        run {
            val payload: ByteArray = Random.nextBytes(1)
            val addresses = RingSignatureVoteBase.getPayloadContractAddresses(
                chain, payloadDeployPK, payload
            )

            assertThat(addresses.size).isEqualTo(1)
            val (_, resultPayload) = addresses[0]
            assertContentEquals(payload, resultPayload)
        }

        // Payload that requires 10 inputs
        run {
            val bigPayload = Random.nextBytes(
                PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE * 10
            )
            val addresses = RingSignatureVoteBase.getPayloadContractAddresses(
                chain, payloadDeployPK, bigPayload
            )
            assertThat(addresses.size).isEqualTo(10)
            var resultPayload = ByteArray(0)
            addresses.map { (_, payload) ->
                resultPayload += payload
            }
            assertContentEquals(bigPayload, resultPayload)
        }

        // Payload that is 1 byte above 9 inputs. Should be rounded
        // up to 10 inputs.
        run {
            val payload = Random.nextBytes(
                (PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE * 9) + 1
            )
            val addresses = RingSignatureVoteBase.getPayloadContractAddresses(
                chain, payloadDeployPK, payload
            )
            assertThat(addresses.size).isEqualTo(10)
            var resultPayload = ByteArray(0)
            addresses.map { (_, payload) ->
                resultPayload += payload
            }
            assertContentEquals(payload, resultPayload)
            assertThat(addresses[9].second.size).isEqualTo(1)
        }
    }
}