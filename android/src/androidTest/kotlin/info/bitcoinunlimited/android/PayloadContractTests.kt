package info.bitcoinunlimited.android

import info.bitcoinunlimited.android.vote.VoteTestUtil
import info.bitcoinunlimited.android.UtilStringEncoding
import org.junit.Test
import java.lang.IllegalArgumentException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class PayloadContractTests {
    companion object {
        init {
            System.loadLibrary(blockchainLibName)
            Initialize.LibBitcoinCash(ChainSelector.NEXAREGTEST.v)
        }
    }

    /**
     * Test that when input is enough, but low, that all coins are sent to the
     * contract.
     */
    @Test
    fun testPayload() {
        val chain = ChainSelector.NEXA
        val signature = ByteArray(NetworkConstants.SCHNORR_SIGNATURE_SIZE) { _ -> 0xAA.toByte() }
        val pubkey = ByteArray(NetworkConstants.PUBLIC_KEY_COMPACT_SIZE) { _ -> 0xBB.toByte() }

        val payload = ByteArray(PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE) { _ -> 0xCC.toByte() }

        val redeemScript = PayloadContract.redeemScript(
            chain,
            pubkey, PayloadContract.calcPayloadHash(pubkey, payload)
        )

        assertEquals(71, redeemScript.toByteArray().size)

        PayloadContract.lockingScript(chain, pubkey, Hash.hash160(payload))

        val unlockingScript = PayloadContract.unlockingScript(
            chain,
            signature, pubkey,
            payload
        )

        assertEquals(NetworkConstants.MAX_TX_IN_SCRIPT_SIG_SIZE, unlockingScript.toByteArray().size)
    }

    @Test
    fun testPayloadToStackItems() {
        val push1 = ByteArray(PayloadContract.MAX_PUSH1_SIZE) { _ -> 0xCC.toByte() }
        val push2 = ByteArray(PayloadContract.MAX_PUSH2_SIZE) { _ -> 0xDD.toByte() }
        val push3 = ByteArray(PayloadContract.MAX_PUSH3_SIZE) { _ -> 0xEE.toByte() }
        val payload = push1 + push2 + push3

        val (push1A, push2A, push3A) = PayloadContract.payloadToStackItems(
            payload
        )

        assertTrue(push1.contentEquals(push1A))
        assertTrue(push2.contentEquals(push2A))
        assertTrue(push3.contentEquals(push3A))

        val p2 = ByteArray(42) { _ -> 0xFF.toByte() }
        val (push1B, push2B, push3B) = PayloadContract.payloadToStackItems(
            push1 + p2
        )

        assertTrue(push1.contentEquals(push1B))
        assertTrue(p2.contentEquals(push2B))
        assertTrue(push3B.isEmpty())
    }

    @Test
    fun testCalcPayloadHash() {
        val payload = ByteArray(PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE) { _ -> 0xAA.toByte() }
        val pubkey = ByteArray(33) { _ -> 0xBB.toByte() }
        val hash = PayloadContract.calcPayloadHash(pubkey, payload)

        assertEquals("3751c49910a95c3a221e9374c208e6ea507a3de9", UtilStringEncoding.toHexString(hash))
    }

    @Test
    fun testGetPayloadFromTx() {
        val chain = ChainSelector.NEXA
        val privkey = VoteTestUtil.mocPrivateKey()

        val pubkey = VoteTestUtil.mocPublicKey(privkey)
        val signature = ByteArray(NetworkConstants.SCHNORR_SIGNATURE_SIZE) { _ -> 0xff.toByte() }
        val payload = "a tiny payload".toByteArray()

        val input = VoteTestUtil.mocInput(chain, UnsecuredSecret(privkey), NetworkConstants.DEFAULT_DUST_THRESHOLD.toLong())
        input.script = PayloadContract.unlockingScript(chain, signature, pubkey, payload)

        val tx = NexaTransaction(chain)
        tx._inputs.add(input as NexaTxInput)

        val result = PayloadContract.getPayloadFromTx(chain, tx)
        assertEquals(
            UtilStringEncoding.toHexString(payload),
            UtilStringEncoding.toHexString(result)
        )
    }

    @Test
    fun testGetPayloadFromTxInvalid() {
        val chain = ChainSelector.NEXA
        val privkey = VoteTestUtil.mocPrivateKey()

        val pubkey = VoteTestUtil.mocPublicKey(privkey)
        val signature = ByteArray(NetworkConstants.SCHNORR_SIGNATURE_SIZE) { _ -> 0xff.toByte() }
        val payload = "a tiny payload".toByteArray()

        val input = VoteTestUtil.mocInput(chain, UnsecuredSecret(privkey), NetworkConstants.DEFAULT_DUST_THRESHOLD.toLong())
        val tx = NexaTransaction(chain)
        tx._inputs.add(input as NexaTxInput)

        val (push1, push2, push3) = PayloadContract.payloadToStackItems(payload)

        // Test that we detect invalid payload hash
        val exception1 = assertFailsWith<IllegalArgumentException>(
            message = "Payload Hash mismatch",
            block = {
                tx.inputs[0].script = SatoshiScript(
                    chain, SatoshiScript.Type.SATOSCRIPT,
                    OP.push(signature), OP.push(push1), OP.push(push2), OP.push(push3),
                    OP.push(PayloadContract.redeemScript(chain, pubkey, ByteArray(PayloadContract.PAYLOAD_HASH_SIZE)).toByteArray())
                )
                PayloadContract.getPayloadFromTx(chain, tx)
            }
        )
        assertEquals(exception1.message, "Payload Hash mismatch")

        // Test that we detect modified redeemscript
        val exception2 = assertFailsWith<IllegalArgumentException>(
            block = {
                var redeem = PayloadContract.redeemScript(chain, pubkey, PayloadContract.calcPayloadHash(pubkey, payload))
                redeem.add("garbage".toByteArray())
                tx.inputs[0].script = SatoshiScript(
                    chain, SatoshiScript.Type.SATOSCRIPT,
                    OP.push(signature), OP.push(push1), OP.push(push2), OP.push(push3),
                    OP.push(redeem.toByteArray())
                )
                PayloadContract.getPayloadFromTx(chain, tx)
            }
        )
        assertEquals(exception2.message, "Incorrect redeemscript for payload contract")
    }
}