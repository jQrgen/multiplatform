package info.bitcoinunlimited.android

import info.bitcoinunlimited.android.PayAddress.Companion.DecodeCashAddr
import info.bitcoinunlimited.multiplatform.BUException

open class UnknownBlockchainException() : BUException(
  "Could not determine the a blockchain from the input",
  "Unknown cryptocurrency/blockchain")

// Define units for Nexa currency names
const val SAT = 1L
const val NEX = SAT*100L
const val KEX = NEX*1000L
const val MEX = KEX*1000L

/***
 * Identify the cryptocurrency/blockchain.
 */
enum class ChainSelector(val v: Byte)
{
    NEXA(1), NEXATESTNET(2), NEXAREGTEST(3), BCH(4), BCHTESTNET(5), BCHREGTEST(6);

    // Helper functions to determine blockchain functionality
    val hasGroupTokens: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)
    val hasP2SH: Boolean
        get() = (this == BCH) or (this == BCHTESTNET) or (this == BCHREGTEST)
    val hasP2PKH: Boolean
        get() = true
    val hasTemplates: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)

    val isNexaFamily: Boolean
        get() = (this == NEXA) or (this == NEXATESTNET) or (this == NEXAREGTEST)
    val isBchFamily: Boolean
        get() = (this == BCH)  or (this == BCHTESTNET)  or (this == BCHREGTEST)

    val hasBitcoinLikeHeader: Boolean
        get() = isBchFamily
}

fun ChainSelectorFromValue(v: Byte): ChainSelector
{
    return when (v)
    {
        ChainSelector.BCH.v -> ChainSelector.BCH
        ChainSelector.BCHTESTNET.v -> ChainSelector.BCHTESTNET
        ChainSelector.BCHREGTEST.v -> ChainSelector.BCHREGTEST
        ChainSelector.NEXATESTNET.v -> ChainSelector.NEXATESTNET
        ChainSelector.NEXAREGTEST.v -> ChainSelector.NEXAREGTEST
        ChainSelector.NEXA.v -> ChainSelector.NEXA
        else -> throw UnknownBlockchainException()
    }
}

/** Convert a ChainSelector to its uri and address prefix */
val chainToURI: Map<ChainSelector, String> = mapOf(
  ChainSelector.NEXATESTNET to "nexatest", ChainSelector.NEXAREGTEST to "nexareg", ChainSelector.NEXA to "nexa",
  ChainSelector.BCH to "bitcoincash", ChainSelector.BCHTESTNET to "bchtest", ChainSelector.BCHREGTEST to "bchreg"
)

/** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val uriToChain: Map<String, ChainSelector> = chainToURI.invert()

/** Convert a ChainSelector to its approx currency code at 100M units */
val chainToCurrencyCode: Map<ChainSelector, String> = mapOf(
  ChainSelector.NEXATESTNET to "tMEX", ChainSelector.NEXAREGTEST to "rMEX", ChainSelector.NEXA to "MEX",
  ChainSelector.BCH to "BCH", ChainSelector.BCHTESTNET to "tBCH", ChainSelector.BCHREGTEST to "rBCH"
)

  /** Convert a ChainSelector to its currency code at 100M/1000 units */
val chainToMilliCurrencyCode: Map<ChainSelector, String> = mapOf(
    ChainSelector.NEXATESTNET to "tKEX", ChainSelector.NEXAREGTEST to "rKEX", ChainSelector.NEXA to "KEX",
    ChainSelector.BCH to "mBCH", ChainSelector.BCHTESTNET to "tmBCH", ChainSelector.BCHREGTEST to "rmBCH"
  )


    /** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val currencyCodeToChain: Map<String, ChainSelector> = chainToURI.invert()


fun ChainSelectorFromAddress(address: String): ChainSelector
{
    try
    {
        if (address.contains(":"))
        {
            val (chain, _) = address.split(":")
            return uriToChain[chain] ?: throw UnknownBlockchainException()
        }
        else
        {
            if ((address[0] == '1') || (address[0] == '3')) throw UnknownBlockchainException()  // TODO bitcoin address
            // Multiple forks use cash addr address format.  Some of them change how the checksum is calculated, making it very likely that an address
            // will not decode properly for a different blockchain.  User SHOULD use the blockchain prefix to remove all ambiguity, but major services do not.
            if ((address[0] == 'q') || (address[0] == 'p'))
            {
                // If multiple blockchains do not change the checksum, best we can do it return the first one we decode -- ChainSelector should order its
                // blockchains based on what's most important to this wallet
                for (chain in ChainSelector.values())
                {
                    try
                    {
                        DecodeCashAddr(chain.v, address)
                        return chain
                    }
                    catch (e: Exception)
                    {
                    }
                }

            }
        }
    }
    catch (e: IndexOutOfBoundsException)
    {
        // no colon means missing the blockchain identifier prefix used in bitcoincash
        // TODO handle the case where people forget to use the chain identifier
        // LogIt.info("Bad address, cannot determine blockchain: '$address'")
        throw UnknownBlockchainException()
    }
    throw UnknownBlockchainException()
}