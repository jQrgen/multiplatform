// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.

// Android Persistence
package info.bitcoinunlimited.android

import android.database.sqlite.SQLiteConstraintException
import androidx.room.*
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import info.bitcoinunlimited.multiplatform.platform.Logger

private val TAG = "BU.nexapersist"

@Entity
open class PNexaBlockHeader() : NexaBlockHeader()
{
    @PrimaryKey
    @ColumnInfo(name = "id")
    var dbkey: ByteArray = byteArrayOf()

    constructor(b: NexaBlockHeader) : this()
    {
        // repeat the search columns because IDK how to get room to work for base class fields
        dbkey = b.hash.hash
        initialize(b)
    }

    /** This constructor lets you override the hash and canned values are used for "known" blocks like the tip */
    constructor(h: ByteArray, b: NexaBlockHeader) : this()
    {
        dbkey = h
        initialize(b)
    }

    fun initialize(b: NexaBlockHeader)
    {
        // hexHash = b.hexHash
        assignHashData(b.hash)
        hashPrevBlock = b.hashPrevBlock
        diffBits = b.diffBits
        hashAncestor = b.hashAncestor
        hashMerkleRoot = b.hashMerkleRoot
        hashTxFilter = b.hashTxFilter
        time = b.time
        height = b.height
        chainWork = b.chainWork
        size = b.size
        txCount = b.txCount
        feePoolAmt = b.feePoolAmt
        utxoCommitment = b.utxoCommitment
        minerData = b.minerData
        nonce = b.nonce
    }
}

@Dao
interface NexaBlockHeaderDao: BlockHeaderPersist
{
    @Query("SELECT * FROM pnexaBlockHeader")
    fun getAll(): List<PNexaBlockHeader>

    @Query("SELECT * FROM pnexaBlockHeader WHERE height IN (:heights)")
    fun loadAllByHeight(heights: IntArray): List<PNexaBlockHeader>

    @Query("SELECT * FROM pnexaBlockHeader WHERE height = :height")
    fun get(height: Long): PNexaBlockHeader?

    @Query("SELECT * FROM pnexaBlockHeader WHERE height = :height")
    fun getAtHeight(height: Long): List<PNexaBlockHeader>

    @Query("SELECT * FROM pnexaBlockHeader WHERE id = :blockid")
    abstract fun get(blockid: ByteArray): PNexaBlockHeader?

    //@Query("SELECT * FROM blockHeader x INNER JOIN (SELECT height, MAX(height) FROM blockHeader GROUP BY height) y ON x.height = y.height")
    @Query("SELECT a.* FROM pnexaBlockHeader a LEFT OUTER JOIN pnexaBlockHeader b ON a.height < b.height WHERE b.height IS NULL")
    fun getLast(): PNexaBlockHeader

    @Query("SELECT a.* FROM pnexaBlockHeader a LEFT OUTER JOIN pnexaBlockHeader b ON a.chainWork < b.chainWork WHERE b.chainWork IS NULL")
    fun getMostWork(): List<PNexaBlockHeader>

    @Insert
    fun insert(vararg bh: PNexaBlockHeader)

    @Update
    fun update(bh: PNexaBlockHeader)

    @Delete
    fun delete(bh: PNexaBlockHeader)

    @Query("DELETE FROM pnexaBlockHeader WHERE height = :height")
    fun delete(height: Long)

    @Query("DELETE FROM pnexaBlockHeader")
    fun deleteAll()


    override fun insertHeader(header: iBlockHeader)
    {
        val h = header as NexaBlockHeader
        h.calcHash()  // Make absolutely sure we've calculated the hash before storing!
        insert(PNexaBlockHeader(h))
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val ret = get(height)
        return ret
    }

    override fun getHeader(blockid: ByteArray): iBlockHeader?
    {
        val ret = get(blockid)
        return ret
    }
    override fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    {
        val ret = getAtHeight(height)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader?
    {
        val ret = get(hash.hash)
        return ret
    }

    override fun getMostWorkHeaders(): List<iBlockHeader> = getMostWork()

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        setNexaCachedTip(header as NexaBlockHeader)
    }

    override fun getCachedTipHeader(): iBlockHeader?
    {
        return this.getNexaCachedTip()
    }

    override fun clear()
    {
        deleteAll()
    }

    companion object
    {
        val lock = ThreadCond()
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        synchronized(lock)
        {
            val h = header as NexaBlockHeader
            h.calcHash()
            val curbh = get(h.hash.hash)
            // This can happen legitimately when additional fields are calculated (that are not actually part of the blockchain header)
            if (curbh == null)
            {
                try
                {
                    insert(PNexaBlockHeader(h))
                }
                catch (e: Exception)
                {
                    update(PNexaBlockHeader(h))
                }
            }
            else if (curbh != header)
                update(PNexaBlockHeader(h))
        }
    }
}

fun NexaBlockHeaderDao.getNexaCachedTip() = get(byteArrayOf(0))

fun NexaBlockHeaderDao.setNexaCachedTip(header: NexaBlockHeader)
{
    val pbh = PNexaBlockHeader(byteArrayOf(0), header as NexaBlockHeader)
    try
    {
        insert(pbh)
    }
    catch (exception: SQLiteConstraintException)
    {
        update(pbh)
    }

}

fun PersistInsert(dbdao: BlockHeaderPersist, bh: iBlockHeader)
{
    while (true)
    {
        try
        {
            dbdao.insertHeader(bh)
            return
        }
        catch (e: android.database.sqlite.SQLiteConstraintException)
        {
            print(e.toString())
            val sqle = e
            if (sqle.toString().contains("SQLITE_CONSTRAINT_PRIMARYKEY"))
            {
                Logger.info(TAG,"Inserting duplicate block header")
                return  // Its ok to insert the same block header, but a waste
            }
            if (!sqle.toString().contains("SQLITE_BUSY"))
                throw(e)
        }
    }
}

@Database(entities = arrayOf(PNexaBlockHeader::class), version = 3)
@TypeConverters(Hash256Converters::class, BigIntegerConverters::class)
abstract class NexaBlockHeaderDatabase : RoomDatabase()
{
    abstract fun blockHeaderDao(): NexaBlockHeaderDao
}

fun NexaOpenBlockHeaderDB(context: PlatformContext, name: String): NexaBlockHeaderDatabase
{
    val db = Room.databaseBuilder(context.context, NexaBlockHeaderDatabase::class.java, name).fallbackToDestructiveMigration().build()
    return db
}

/** Delete all block headers in the database -- used in regression testing */
fun NexaDeleteBlockHeaders(name: String, dbPrefix: String, context: PlatformContext)
{
    val db = NexaOpenBlockHeaderDB(context, dbPrefix + name)
    val dbao = db!!.blockHeaderDao()
    dbao.deleteAll()
}