package info.bitcoinunlimited.android.vote

import info.bitcoinunlimited.android.*
import info.bitcoinunlimited.android.UtilStringEncoding
import info.bitcoinunlimited.multiplatform.platform.Logger

/**
 * TwoOptionVote is a on-chain transparent election protocol.
 * Specification: https://nerdekollektivet.gitlab.io/votepeer-documentation/two-option-vote-contract.html
 *
 * Class for working with a single two option vote election.
 */
private val TAG = "BU.blockchain"

class TwoOptionVoteContract(
    var proposalID: ByteArray,
    var optA: ByteArray,
    var optB: ByteArray,
    var voterPKH: ByteArray
) {

    companion object {
        /**
         * Minium amount in satoshis for funding an contract
         */
        @JvmField val MIN_CONTRACT_INPUT: Long = 1000

        /**
         * Minimum amount in satoshis required to add a change output when
         * funding a contract
         */
        @JvmField val MIN_CHANGE_OUTPUT: Long = 600

        /**
         * Fee for the transaction funding the contract.
         */
        @JvmField val FUND_FEE: Long = 217

        /**
         * Fee for the transction casting a vote.
         */
        @JvmField val CAST_FEE: Long = 441

        /**
         * Vote option for casting blank vote
         */
        @JvmField val BLANK_VOTE: ByteArray = UtilStringEncoding.hexToByteArray(
            "beefffffffffffffffffffffffffffffffffffff"
        )
    }

    fun redeemScript(): ByteArray {
        return TwoOptionVote.redeemScript(
            this.proposalID,
            this.optA, this.optB, this.voterPKH
        )
    }

    fun address(chain: ChainSelector): PayAddress {
        return TwoOptionVote.deriveContractAddress(
            chain,
            this.proposalID, this.optA, this.optB, this.voterPKH
        )
    }

    /**
     * The P2SH script used for locking coins into this contract.
     *
     * When funding the contract, coins are locked in this output script (scriptPubKey).
     */
    fun outputScript(chain: ChainSelector): SatoshiScript {
        return SatoshiScript(
            chain, SatoshiScript.Type.SATOSCRIPT,
            OP.HASH160, OP.push(Hash.hash160(this.redeemScript())), OP.EQUAL
        )
    }

    /**
     * The script to unlock coins in this contract, and thus casting a vote (scriptSig)
     */
    fun inputScript(
        chain: ChainSelector,
        pubkey: ByteArray,
        txSig: ByteArray,
        msg: ByteArray,
        msgSig: ByteArray
    ): SatoshiScript {
        return SatoshiScript(
            chain, SatoshiScript.Type.PUSH_ONLY,
            OP.push(msgSig),
            OP.push(msg),
            OP.push(txSig),
            OP.push(pubkey),
            OP.push(this.redeemScript())
        )
    }

    /**
     * Fund a voting contract
     *
     * @param chain
     * @param input An input funding the contract.
     * @param changeAddress A change address to pass coins in input to.
     */
    fun fundContract(
        chain: ChainSelector,
        input: iTxInput,
        changeAddress: PayDestination
    ): iTransaction {

        /**
         * The minimum satoshis the vote output should contain for it to
         * be spendable (without requiring additional inputs)
         */
        val inputAmount = input.spendable.amount

        if (inputAmount < MIN_CONTRACT_INPUT + FUND_FEE) {
            throw IllegalArgumentException("Input amount too low to fund vote contract")
        }
        val addChange = inputAmount >= (MIN_CONTRACT_INPUT + MIN_CHANGE_OUTPUT + FUND_FEE)

        val tx = txFor(chain)
        tx.add(input)

        // The vote output
        val voteOutput = txOutputFor(chain)
        voteOutput.amount = if (addChange) MIN_CONTRACT_INPUT else inputAmount - FUND_FEE
        voteOutput.script = this.outputScript(chain)
        tx.add(voteOutput)

        // Change output
        if (addChange) {
            val changeOutput = NexaTxOutput(chain)
            changeOutput.amount = inputAmount - MIN_CONTRACT_INPUT - FUND_FEE
            changeOutput.script = changeAddress.ungroupedOutputScript()
            tx.add(changeOutput)
        }

        val inputSecret = input.spendable.secret ?: error("input secret missing")

        val pubkey = PayDestination.GetPubKey(inputSecret.getSecret())
        val sig = UtilVote.signInput(tx, 0)

        // Assumes P2PKH input
        tx.inputs[0].script = SatoshiScript(chain, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(pubkey))
        return tx
    }

    /**
     * Spend a input from the contract (casting a vote)
     */
    fun castVote(
        chain: ChainSelector,
        input: iTxInput,
        changeAddress: PayDestination,
        voteOption: ByteArray
    ): iTransaction {

        if (!voteOption.contentEquals(this.optA) &&
            !voteOption.contentEquals(this.optB) &&
            !voteOption.contentEquals(BLANK_VOTE)
        ) {
            val err = "Cannot cast vote. Invalid vote option for contract"
            throw IllegalArgumentException(err)
        }

        input.spendable.priorOutScript = SatoshiScript(chain, SatoshiScript.Type.SATOSCRIPT, this.redeemScript())

        val tx = txFor(chain)
        tx.add(input)

        val changeOutput = txOutputFor(chain)
        changeOutput.amount = input.spendable.amount - CAST_FEE
        changeOutput.script = changeAddress.ungroupedOutputScript()
        tx.add(changeOutput)

        val inputSecret = input.spendable.secret?.getSecret() ?: error("input secret missing")
        val pubkey = PayDestination.GetPubKey(inputSecret)
        val txSig = UtilVote.signInput(tx, 0)

        val msg = TwoOptionVote.createVoteMessage(proposalID, voteOption)
        val msgSig = TwoOptionVote.signVoteMessage(inputSecret, msg)

        Logger.info(TAG,"Vote: Pubkey ${pubkey.toHex()} Sig ${txSig.toHex()} VoteMsg: ${msg.toHex()} Sig: ${msgSig.toHex()}")

        val castVoteScript = this.inputScript(chain, pubkey, txSig, msg, msgSig)
        tx.inputs[0].script = castVoteScript

        return tx
    }
}