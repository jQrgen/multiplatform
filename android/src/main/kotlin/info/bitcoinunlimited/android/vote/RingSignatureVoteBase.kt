package info.bitcoinunlimited.android.vote

import info.bitcoinunlimited.android.*
import kotlin.math.ceil
import kotlin.math.min

/**
 * Functions implementing primitives for voting on Bitcoin Cash using traceable ring signatures.
 *
 * Specification: https://nerdekollektivet.gitlab.io/votepeer-documentation/ring-signature-vote-contract.html
 */
class RingSignatureVoteBase {
    companion object {
        /**
         * Size of public key used in ring signatures.
         */
        const val RINGSIG_PUBLIC_KEY_LEN: Int = 32
        const val ELECTION_ID_LEN: Int = 20
        const val VOTE_OPTION_LEN: Int = 20

        /**
         * Calculate the Election ID.
         *
         * The Election ID consist of a hash of all the data belonging to a vote.
         */
        fun calculateElectionId(
            salt: ByteArray,
            description: String,
            beginHeight: Int,
            endHeight: Int,
            voteOptionsHashed: Array<ByteArray>,
            participants: Array<ByteArray>
        ): ByteArray {
            var participantsCombined = ByteArray(0)
            var voteOptionsCombined = ByteArray(0)

            for (p in UtilVote.sortByteArray(participants)) {
                if (p.size != RINGSIG_PUBLIC_KEY_LEN) {
                    throw IllegalArgumentException(
                        "Participant public key must be $RINGSIG_PUBLIC_KEY_LEN bytes (is ${p.size})"
                    )
                }
                participantsCombined += p
            }
            for (o in UtilVote.sortByteArray(voteOptionsHashed)) {
                if (o.size != VOTE_OPTION_LEN) {
                    throw IllegalArgumentException("Vote option hash must be $VOTE_OPTION_LEN bytes (got ${o.size})")
                }
                voteOptionsCombined += o
            }
            val electionIDParts: Array<ByteArray> = arrayOf<ByteArray>(
                salt,
                description.toByteArray(),
                UtilVote.longToUInt32ByteArray(beginHeight.toLong()),
                UtilVote.longToUInt32ByteArray(endHeight.toLong()),
                voteOptionsCombined,
                participantsCombined
            )

            var electionID = ByteArray(0)
            for (p in electionIDParts) {
                electionID += p
            }

            return Hash.hash160(electionID)
        }

        /**
         * This output is unique to the election. Its purpose is to be an
         * address all participants can observe to detect votes.
         */
        fun getNotificationAddress(
            chain: ChainSelector,
            electionID: ByteArray
        ): PayAddress {
            val hash = Hash.hash160(notificationRedeemScript(chain, electionID).toByteArray())
            return PayAddress(chain, PayAddressType.P2SH, hash)
        }

        /**
         * Redeemscript for notification output. Used to generate the notification address.
         */
        fun notificationRedeemScript(
            chain: ChainSelector,
            proposalID: ByteArray,
        ): SatoshiScript {
            return SatoshiScript(chain, SatoshiScript.Type.SATOSCRIPT, OP.push(proposalID), OP.EQUALVERIFY)
        }

        private fun isParticipating(pubkey: ByteArray, participants: Array<ByteArray>): Boolean {
            for (p in participants) {
                if (p.contentEquals(pubkey)) {
                    return true
                }
            }
            return false
        }

        /**
         * Create a traceable ring signature for an election vote.
         */
        fun signVoteMessage(
            electionID: ByteArray,
            vote: ByteArray,
            participants: Array<ByteArray>,
            privkeySeed: ByteArray,
        ): ByteArray {
            if (electionID.size != ELECTION_ID_LEN) {
                throw IllegalArgumentException("Invalid electionID length")
            }
            if (vote.size != VOTE_OPTION_LEN) {
                throw IllegalArgumentException("Invalid vote message length, expected $VOTE_OPTION_LEN, got ${vote.size}")
            }

            var keypair: KeyPairPtr? = null
            var tag: TagPtr? = null
            try {
                keypair = RingSignature.newKeyPairFromBits(privkeySeed)
                tag = RingSignature.createTag(electionID, participants)

                val pubkey = RingSignature.getPubKey(keypair)
                if (!isParticipating(pubkey, participants)) {
                    throw IllegalArgumentException("Cannot sign, pubkey is not in list of participants")
                }
                return RingSignature.signMessage(vote, tag!!, RingSignature.getPrivKey(keypair!!))
            } finally {
                if (keypair != null) {
                    RingSignature.freeKeyPair(keypair)
                }
                if (tag != null) {
                    RingSignature.freeTag(tag)
                }
            }
        }

        fun getPayloadContractAddress(chain: ChainSelector, pubkey: ByteArray, payload: ByteArray): PayAddress {
            if (payload.size > PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE) {
                throw IllegalArgumentException(
                    (
                        "Payload is too large for a single contract. " +
                            "${payload.size} > ${PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE}. " +
                            "Use `getPayloadContractAddresses`."
                        )
                )
            }
            val payloadHash = PayloadContract.calcPayloadHash(pubkey, payload)
            return PayloadContract.contractAddress(chain, pubkey, payloadHash)
        }

        fun parseVote(chain: ChainSelector, tx: iTransaction): Pair<ByteArray, ByteArray> {
            val payload = PayloadContract.getPayloadFromTx(chain, tx)
            if (payload.size <= 21) {
                throw Error("Invalid payload size. Transaction does not contain a ring signature vote.")
            }
            return Pair(
                payload.sliceArray(0 until 20),
                payload.sliceArray(20 until payload.size)
            )
        }

        /**
         * Calculate how many inputs will be required for the vote casting payload
         */
        fun calculateInputsRequired(participants: Int): Int {
            val payloadSize = 20 /* vote hash */ + 44 /* some constant */ + (64 * participants)
            return ceil(payloadSize.toDouble() / PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE.toDouble()).toInt()
        }

        /**
         * Create list of of outputs and chunks of payload associated with.
         * Spending these outputs will deliver the full payload.
         */
        fun getPayloadContractAddresses(
            chain: ChainSelector,
            pubkey: ByteArray,
            payload: ByteArray
        ): List<Pair<PayAddress, ByteArray>> {
            val contracts: MutableList<Pair<PayAddress, ByteArray>> = mutableListOf()
            var chunkStart = 0

            while (true) {
                val chunk = payload.sliceArray(
                    IntRange(
                        chunkStart,
                        min(
                            chunkStart + PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE - 1,
                            payload.size - 1 /* 0 index */
                        )
                    )
                )
                val contract = getPayloadContractAddress(chain, pubkey, chunk)
                contracts.add(Pair(contract, chunk))

                chunkStart += PayloadContract.MAX_PAYLOAD_PER_INPUT_SIZE
                if (chunkStart >= payload.size) break
            }
            return contracts
        }
    }
}