package info.bitcoinunlimited.android.vote

import info.bitcoinunlimited.android.*
import info.bitcoinunlimited.android.UtilStringEncoding
import java.math.BigInteger

/**
 * Class for voting on Bitcoin Cash using traceable ring signatures.
 *
 * Specification: https://nerdekollektivet.gitlab.io/votepeer-documentation/ring-signature-vote-contract.html
 */
class RingSignatureVote(
    /**
     * Random data unique to this election.
     */
    val salt: ByteArray,
    /**
     * What is being voted on?
     */
    val description: String,
    /**
     * At what block height does the election start?
     */
    val beginHeight: Int,
    /**
     * At what block height does the election end?
     */
    val endHeight: Int,
    /**
     * Possible vote options for this election ("yes", "no", "maybe", etc)
     */
    val voteOptions: Array<String>,
    /**
     * Public keys of all participants
     */
    val participants: Array<ByteArray>,
) {
    companion object {
        /**
         * The payload input needs to pay for:
         * - Its own input and redeemscript with payload, which is limited by
         *   NetworkConstants.MAX_TX_IN_SCRIPT_SIG_SIZE
         * - An P2PKH output + dust
         * - Other tx overhead.
         */
        fun minimumPayloadContractInput(): Int {
            return (
                NetworkConstants.EMPTY_TX_INPUT_SIZE +
                    3 + /* extra bytes for the scriptsig size varint */
                    NetworkConstants.MAX_TX_IN_SCRIPT_SIG_SIZE +
                    + NetworkConstants.P2PKH_OUTPUT_SIZE +
                    NetworkConstants.DEFAULT_DUST_THRESHOLD +
                    UtilVote.getBaseTxSize(1, 1)
                )
        }

        const val MIN_FUNDING_CHANGE_AMOUNT = NetworkConstants.DEFAULT_DUST_THRESHOLD
    }

    init {

        val correctSizedPubkeys = {
            var ok = true
            for (p in participants) {
                if (p.size != RingSignatureVoteBase.RINGSIG_PUBLIC_KEY_LEN) {
                    ok = false
                    break
                }
            }
            ok
        }

        require(beginHeight >= 0) { "Begin height must be a positive number (was $beginHeight)" }
        require(endHeight >= beginHeight) { "End height must be larger or equal to begin height ($endHeight < $beginHeight)" }
        require(salt.isNotEmpty()) { "Salt is empty" }
        require(voteOptions.isNotEmpty()) { "No vote options" }
        require(participants.isNotEmpty()) { "No participants" }
        require(correctSizedPubkeys()) { "Incorrectly sized pubkeys" }
    }

    fun getElectionID(): ByteArray {
        return RingSignatureVoteBase.calculateElectionId(
            salt,
            description,
            beginHeight,
            endHeight,
            getHashedSortedVoteOptions(),
            participants
        )
    }

    /**
     * Hashes vote options and returns them sorted by hash value.
     */
    fun getHashedSortedVoteOptions(): Array<ByteArray> {
        val hashedVoteOptions: Array<ByteArray> = voteOptions.map {
            UtilVote.hash160Salted(salt, it.toByteArray())
        }.toTypedArray()
        val sorted = UtilVote.sortByteArray(hashedVoteOptions)
        for (s in sorted) {
            println(UtilStringEncoding.toHexString(s))
        }
        return sorted
    }

    /**
     * Get the hash of the vote
     */
    fun hashVote(vote: String): ByteArray {
        if (!voteOptions.contains(vote)) {
            throw IllegalArgumentException("Invalid vote '$vote for election")
        }
        return UtilVote.hash160Salted(salt, vote.toByteArray())
    }

    /**
     * Generate a keypair to use with the payload contract.
     *
     * See section "Deterministic private/public key for payload" in
     * https://nerdekollektivet.gitlab.io/votepeer-documentation/locating-own-ringsignature-vote/
     */
    fun createDeterministicKeypair(secret: ByteArray): Pair<ByteArray, ByteArray> {
        val maxPrivateKeyValue = BigInteger("FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551", 16)
        var privateKey: ByteArray
        do {
            privateKey = Hash.sha256(secret + getElectionID())
        } while (BigInteger(privateKey.toHex(), 16) > maxPrivateKeyValue)

        val pubkey = PayDestination.GetPubKey(privateKey)
        return Pair(privateKey, pubkey)
    }

    fun calculateFundingAmountRequired(numberOfInputs: Int): Int {
        val fundingInputsRequired = RingSignatureVoteBase.calculateInputsRequired(this.participants.size)

        val txSize = UtilVote.getBaseTxSize(numberOfInputs, fundingInputsRequired)
        + (numberOfInputs * NetworkConstants.P2PKH_SCHNORR_INPUT_SIZE)
        + (fundingInputsRequired * NetworkConstants.P2SH_OUTPUT_SIZE)

        return txSize + (fundingInputsRequired * minimumPayloadContractInput())
    }

    /**
     * Fund the contract used to deliver the a vote on the blockchain. Returns a transaction
     * and a list of inputs for spending the outputs of said transaction. Each of these inputs
     * is paired with its "share of the payload".
     *
     * DANGER: If the p2pkhInput can be traced to the voter, then anonymity is compromised.
     */
    fun fundPayloadContract(
        chain: ChainSelector,
        p2pkhInputs: Array<iTxInput>,
        privateKey: ByteArray,
        payload: ByteArray,
        changeAddress: PayAddress
    ): Pair<iTransaction, Array<Pair<iTxInput, ByteArray>>> {
        val minimumRequiredInput = this.calculateFundingAmountRequired(p2pkhInputs.size)
        val inputAmount = p2pkhInputs.sumOf { it.spendable.amount }

        if (inputAmount < minimumRequiredInput) {
            throw IllegalArgumentException(
                (
                    "Input amount too low to fund vote contract with given payload and number of inputs. " +
                        "Minimum required amount is $minimumRequiredInput. Inputs had $inputAmount."
                    )
            )
        }
        var changeAmount = inputAmount - minimumRequiredInput
        - 8 - changeAddress.outputScript().flatten().size /* fee for change output */

        if (changeAmount < MIN_FUNDING_CHANGE_AMOUNT) {
            changeAmount = 0
        }

        val tx: iTransaction = txFor(chain)
        for (i in p2pkhInputs) {
            tx.add(i)
        }

        // Payload outputs
        val pubkey = PayDestination.GetPubKey(privateKey)
        val payloadContracts = RingSignatureVoteBase.getPayloadContractAddresses(chain, pubkey, payload)
        for ((contract, _) in payloadContracts) {
            val output = NexaTxOutput(chain)
            output.amount = minimumPayloadContractInput().toLong()
            output.script = contract.outputScript()
            tx.add(output)
        }

        // Change output
        if (changeAmount != 0L) {
            val changeOutput = NexaTxOutput(chain)
            changeOutput.amount = changeAmount
            changeOutput.script = changeAddress.outputScript()
            tx.add(changeOutput)
        }

        // Sign inputs
        tx.inputs.forEachIndexed { i, input ->
            val inputSecret = input.spendable.secret ?: error("input secret missing")
            val inputPubkey = PayDestination.GetPubKey(inputSecret.getSecret())
            val sig = UtilVote.signInput(tx, i.toLong())

            // Unlock P2PKH input
            tx.inputs[i].script = SatoshiScript(chain, SatoshiScript.Type.PUSH_ONLY, OP.push(sig), OP.push(inputPubkey))
        }

        // Collect the data needed to spend the outputs we just created.
        tx.changed()
        val txidem = tx.idem
        val inputs: MutableList<Pair<NexaTxInput, ByteArray>> = mutableListOf()
        payloadContracts.forEachIndexed { i, (_, payload) ->
            val input = NexaTxInput(chain)
            val payloadHash = PayloadContract.calcPayloadHash(pubkey, payload)
            input.spendable.priorOutScript = PayloadContract.redeemScript(chain, pubkey, payloadHash)
            input.spendable.amount = tx.outputs[i].amount
            input.spendable.outpoint = NexaTxOutpoint(txidem, i.toLong())
            input.spendable.secret = UnsecuredSecret(privateKey)
            inputs.add(Pair(input, payload))
        }
        return Pair(tx, inputs.toTypedArray())
    }

    /**
     * Create the payload to be delivered to the blockchain in order to cast
     * the vote.
     * This payload consists of the vote itself + a ring signature proving the
     * user is part of the election.
     */
    fun createVotePayload(
        vote: ByteArray,
        privateKeySeed: ByteArray
    ): ByteArray {
        checkHashedVote(vote)
        val voteSignature = RingSignatureVoteBase.signVoteMessage(
            getElectionID(),
            vote,
            participants,
            privateKeySeed
        )

        return vote + voteSignature
    }

    fun checkHashedVote(vote: ByteArray) {
        if (vote.size != RingSignatureVoteBase.VOTE_OPTION_LEN) {
            throw java.lang.IllegalArgumentException(
                "Invalid length for hashed vote ${vote.size}, expected ${RingSignatureVoteBase.VOTE_OPTION_LEN}"
            )
        }
        for (hashed in getHashedSortedVoteOptions()) {
            if (vote.contentEquals(hashed)) {
                return
            }
        }
        throw IllegalArgumentException("Vote '${UtilStringEncoding.toHexString(vote)}' is not valid for this election")
    }

    /**
     * Validates that the vote and signature is valid for this
     * election.
     *
     * NOTE! Does not trace for duplicate votes.
     */
    fun checkPayload(votePayload: ByteArray) {

        val vote = votePayload.sliceArray(0 until 20)
        checkHashedVote(vote)
        val signature = votePayload.sliceArray(20 until votePayload.size)
        var tag: TagPtr? = null
        try {
            tag = RingSignature.createTag(getElectionID(), participants)
            if (!RingSignature.verify(vote, tag, signature)) {
                throw IllegalArgumentException("Signature does not verify in payload")
            }
        } finally {
            if (tag != null) {
                RingSignature.freeTag(tag)
            }
        }
    }

    /**
     * Create a transaction that delivers the vote to the blockchain.
     *
     * This method assumes the input is from a "Input Payload Contract".
     *
     * WARNING: There is no change! For privacy reasons, the whole input is spent.
     */
    fun castVoteTransaction(
        chain: ChainSelector,
        inputsWithVotePayload: Array<Pair<iTxInput, ByteArray>>,
        allowBigFee: Boolean
    ): iTransaction {
        var fullPayload = ByteArray(0)
        inputsWithVotePayload.map { (_, payloadChunk) ->
            fullPayload += payloadChunk
        }
        val inputAmount = inputsWithVotePayload.sumOf { (input, _) -> input.spendable.amount }

        checkPayload(fullPayload)
        val requiredInput = minimumPayloadContractInput() * inputsWithVotePayload.size
        if (!allowBigFee && inputAmount > requiredInput * 3) {
            throw IllegalArgumentException("Inputs are too large. Inputs are in total $inputAmount, while we require at most $requiredInput")
        }

        // Add inputs
        val tx = txFor(chain)
        for (input in inputsWithVotePayload) {
            tx.add(input.first)
        }

        // Add output to notification address
        val output = txOutputFor(chain)
        output.amount = NetworkConstants.DEFAULT_DUST_THRESHOLD.toLong()
        output.script = getNotificationAddress(chain).constraintScript()
        tx.add(output)

        // Unlock and sign inputs
        tx.inputs.forEachIndexed { index: Int, input: iTxInput ->
            val signature = UtilVote.signInput(tx, index.toLong())
            val inputPubkey = PayDestination.GetPubKey(input.spendable.secret?.getSecret()!!)
            val votePayloadChunk = inputsWithVotePayload[index].second
            input.script = PayloadContract.unlockingScript(
                chain,
                signature,
                inputPubkey,
                votePayloadChunk
            )
        }
        return tx
    }

    fun getNotificationAddress(chain: ChainSelector): PayAddress {
        return RingSignatureVoteBase.getNotificationAddress(chain, getElectionID())
    }
}