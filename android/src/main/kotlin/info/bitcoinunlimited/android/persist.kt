// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.

// Android Persistence
package info.bitcoinunlimited.android

import android.database.sqlite.SQLiteBlobTooBigException
import android.database.sqlite.SQLiteConstraintException
import androidx.room.*
import java.math.BigInteger

import androidx.room.Entity
import androidx.room.PrimaryKey
import info.bitcoinunlimited.multiplatform.BUException
import info.bitcoinunlimited.multiplatform.platform.Logger

open class DataMissingException(msg: String) : BUException(msg, appI18n(0xf00d + 20)) // R.string.dataMissing))


private val TAG = "BU.persist"

fun clearAllTables(db: RoomDatabase)
{
    db.clearAllTables()
}

@Entity
class KvpData()
{
    @PrimaryKey
    var id: ByteArray = byteArrayOf()
    var value: ByteArray = byteArrayOf()

    constructor(k: ByteArray, v: ByteArray) : this()
    {
        id = k
        value = v
    }
}

@Dao
interface KvpDao
{
    @Query("SELECT * FROM KvpData WHERE id = :key")
    abstract fun get(key: ByteArray): KvpData?

    @Insert
    fun insert(bh: KvpData)

    @Update
    fun update(bh: KvpData)

    @Delete
    fun delete(bh: KvpData)

    @Query("DELETE FROM KvpData")
    fun deleteAll()
}

fun KvpDao.upsert(d: KvpData): Boolean
{
    try
    {
        insert(d)
    }
    catch (exception: SQLiteConstraintException)
    {
        update(d)
    }
    return true
}

@Database(entities = arrayOf(KvpData::class), version = 1)
abstract class KvpDatabase : RoomDatabase()
{
    abstract fun dao(): KvpDao

    /** update or insert a key value pair into the database */
    fun set(key: ByteArray, value: ByteArray) = dao().upsert(KvpData(key, value))

    /** look up the passed key, throwing DataMissingException if it does not exist */
    fun get(key: ByteArray): ByteArray
    {
        try
        {
            val kvp = dao().get(key)
            if (kvp == null) throw DataMissingException("Missing key: " + String(key))
            return kvp.value
        }
        catch (e: SQLiteBlobTooBigException)
        {
            Logger.info(TAG, "Stored data is corrupt, rediscovering: ${e.toString()}")
            throw DataMissingException("Blob too big: " + String(key))
        }
    }

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: ByteArray): ByteArray?
    {
        val kvp = dao().get(key)
        if (kvp == null) return null
        return kvp.value
    }

    /** update or insert a key value pair into the database */
    fun set(key: String, value: ByteArray) = set(key.toByteArray(), value)

    /** look up the passed key, returning the value or throwing DataMissingException */
    fun get(key: String): ByteArray = get(key.toByteArray())

    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: String): ByteArray? = getOrNull(key.toByteArray())

    /** delete a record */
    fun delete(key: String) = delete(key.toByteArray())

    /** delete a record */
    fun delete(key: ByteArray)
    {
        dao().delete(KvpData(key, byteArrayOf()))
    }
    /*
    fun saveBip44Wallet(wd: Bip44WalletData)
    {
        val dbao = dao()
        while (true)
        {
            try
            {
                //TODO dbao.upsert(wd)
            }
            catch (e: android.database.sqlite.SQLiteConstraintException)
            {
                val sqle = e
                if (sqle.toString().contains("SQLITE_CONSTRAINT_PRIMARYKEY"))
                {
                    LogIt.info("Inserting duplicate block header")
                    return  // Its ok to insert the same block header, but a waste
                }
                if (!sqle.toString().contains("SQLITE_BUSY"))
                    throw(e)
            }
        }
    }

    fun loadBip44Wallet(id: String): Bip44WalletData
    {
        val dbao = dao()
        val ret = dbao.get(id)
        if (ret == null)
            throw(DataMissingException())
        var result:Bip44WalletData = Bip44WalletData()
        result.BCHdeserialize(BCHserialized(ret.value,SerializationType.DISK))
        return result
    }
    */
}

fun OpenKvpDB(context: PlatformContext, name: String): KvpDatabase?
{
    val db = Room.databaseBuilder(context.context, KvpDatabase::class.java, name).build()
    return db
}

class Hash256Converters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): Hash256?
    {
        return value?.let { Hash256(it) }
    }

    @TypeConverter
    fun toByteArray(bid: Hash256?): ByteArray?
    {
        return bid?.hash
    }
}

class BigIntegerConverters
{
    @TypeConverter
    fun fromByteArray(value: ByteArray?): BigInteger?
    {
        return value?.let {
            var ret = 0.toBigInteger()
            for (b in it)
            {
                ret = ret.shiftLeft(8)
                ret += b.toPositiveInt().toBigInteger()
            }
            ret
        }
    }

    @TypeConverter
    fun toByteArray(bid: BigInteger?): ByteArray?
    {
        if (bid == null) return null
        var ret = ByteArray(32)
        var value: BigInteger = bid
        for (i in 1..32)  // By converting by hand we are sure that the byte order means that lexicographical compare is equivalent to numerical compare
        {
            ret[32 - i] = value.and(255.toBigInteger()).toByte()
            value = value.shiftRight(8)

        }
        return ret
    }
}
