// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.platform.Logger

private val TAG = "BU.block"


/** Header of a block for Nexa */
@cli(Display.Simple, "Block header")
open class NexaBlockHeader() : CommonBlockHeader()
{
    //@cli(Display.Simple, "previous block hash")
    //var hashPrevBlock = Hash256()

    //@cli(Display.Simple, "difficulty in 'bits' representation")
    //var diffBits: Long = 0L // nBits

    //@cli(Display.Simple, "ancestor")
    //var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved


    //@cli(Display.Simple, "merkle root hash")
    //var hashMerkleRoot = Hash256()

    @cli(Display.Simple, "transaction filter hash")
    var hashTxFilter = Hash256()

    //@cli(Display.Simple, "block timestamp")
    //var time: Long = 0L

    //@cli(Display.Simple, "block height")
    //var height: Long = -1L

    //@cli(Display.Simple, "cumulative work in the chain")
    //var chainWork: BigInteger = 0.toBigInteger()

    //@cli(Display.Simple, "block size in bytes")
    //var size: Long = -1L

    //@cli(Display.Simple, "number of transactions in block")
    //var txCount: Long = 0L

    @cli(Display.Simple, "fee pool amount")
    var feePoolAmt: Long = 0L

    @cli(Display.Simple, "ledger state commitment")
    var utxoCommitment = byteArrayOf()

    @cli(Display.Simple, "miner data")
    var minerData = byteArrayOf()

    @cli(Display.Simple, "nonce")
    var nonce = byteArrayOf()

    companion object
    {
        @JvmStatic
        fun fromHex(hex: String, serializationType: SerializationType = SerializationType.UNKNOWN): NexaBlockHeader
        {
            val bytes = hex.fromHex()
            var header = NexaBlockHeader()
            header.deserHeaderFields(BCHserialized(bytes, serializationType))
            return header
        }

        @JvmStatic
        external fun blockHash(data: ByteArray): ByteArray

    }

    /** Force recalculation of hash. To access the hash just use #hash */
    override fun calcHash(): Hash256
    {
        val ser = serializeHeader(SerializationType.HASH).flatten()
        val ba = blockHash(ser)
        val h = Hash256(ba)
        hashData = h
        return h

        // call header serialize explicitly in case derived class has overridden BCHserialize
        /*
        var ser = serializeHeader(SerializationType.HASH)
        ser.flatten()
        val h = Hash256(bitcoinunlimited.libbitcoincash.Hash.hash256(ser.flatten()))
        hashData = h
        return h
         */
    }

    /** assignment constructor */
    constructor(_time: Long, _diffBits: Long, _nonce: ByteArray, _hashMerkleRoot: Hash256, _hashPrevBlock: Hash256) : this()
    {
        // hexHash = hash.toHex()
        time = _time
        diffBits = _diffBits
        nonce = _nonce
        hashMerkleRoot = _hashMerkleRoot
        hashPrevBlock = _hashPrevBlock
    }

    constructor(stream: BCHserialized) : this() //!< deserializing constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = serializeHeader(format) //!< Serializer

    fun serializeHeader(format: SerializationType): BCHserialized
    {
        var serialized = BCHserialized(format) +
          hashPrevBlock +
          BCHserialized.uint32(diffBits) +
          hashAncestor +
          hashMerkleRoot +
          hashTxFilter +

          BCHserialized.uint32(time) +
          BCHserialized.varint(height) +
          BCHserialized.uint256(chainWork) +
          size +
          BCHserialized.varint(txCount) +

          BCHserialized.varint(feePoolAmt) +
          variableSized(utxoCommitment) +
          variableSized(minerData) +
          variableSized(nonce)

        if (format == SerializationType.DISK)
        {
            // Other details
        }
        return serialized
    }

    /** Deserialize just the header fields (used when this header is included in other objects/messages */
    fun deserHeaderFields(stream: BCHserialized): BCHserialized
    {
        // header fields
        hashPrevBlock.BCHdeserialize(stream)
        diffBits = stream.deuint32()
        hashAncestor.BCHdeserialize(stream)
        hashMerkleRoot.BCHdeserialize(stream)
        hashTxFilter.BCHdeserialize(stream)

        time = stream.deuint32()
        height = stream.devarint()
        chainWork = stream.deuint256()
        size = stream.deuint64()
        txCount = stream.devarint()
        feePoolAmt = stream.devarint()

        utxoCommitment = stream.deByteArray()
        minerData = stream.deByteArray()
        nonce = stream.deByteArray()
        return stream
    }

    /** Deserializer
    Note that this includes the number of tx in NETWORK serialization as a legacy of the P2P network protocol.
    In DISK serialization, the block size in bytes is included.

    When the header is part of the block, the numTx is not included as an independent field so use deserHeaderFields() API or HASH SerializationType
     */
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        deserHeaderFields(stream)

        var numTx = 0L
        // BCH only: if (stream.format == SerializationType.NETWORK) numTx = stream.decompact()  // P2P Network only

        if (stream.format == SerializationType.DISK)
        {
        }
        return stream
    }
}


/** A block that contains a subset of the total number of transactions, and includes a merkle proof that the provided transactions are part of the block */
class NexaMerkleBlock(val chainSelector: ChainSelector) : NexaBlockHeader(), iMerkleBlock
{
    // Hashes of either transactions or merkle sub-trees
    //var hashes: List<Hash256>? = null
    // Bit data that describes how the hashes are used to form a merkle proof.
    //var merkleProofPath: ByteArray? = null

    //* the id of the transactions provided by this merkle block
    override val txHashes: MutableSet<Hash256> = mutableSetOf()

    //* the actual transactions provided by this merkle block
    override val txes: MutableList<NexaTransaction> = mutableListOf()


    override val complete: Boolean  //*< return true if all needed transactions are available in this merkle block
        @Synchronized
        get()
        {
            if (txHashes.size == 0)
            {
                //LogIt.finer("Merkle block provided ${txes.size} of ${numTx} total transactions")
                return true
            }  // If we are out of txHashes, txes should be full

            assert(txes.size < txCount) // otherwise it should not be full
            return false
        }

    /** Call to offer a transaction that this merkleblock might contain.
     * @return true if this merkleblock consumed this transaction */
    @Synchronized
    override fun txArrived(tx: iTransaction): Boolean
    {
        val id = tx.id
        if (txHashes.contains(id))
        {
            txes.add(tx as NexaTransaction)
            txHashes.remove(id)
            Logger.info(TAG,sourceLoc() + ": Merkle block ${id.toHex()} tx arrived ${id.toHex()}.  ${txHashes.size} left to find (of ${txes.size + txHashes.size}).")
            return true
        }
        return false
    }

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector) //!< stream constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        var numTx = stream.deuint32()
        val h: Array<ByteArray> = stream.delist { Hash256(it).hash }.toTypedArray()  // The merkle block proof data, containing either inner hashes or tx hashes
        val mpp = stream.deByteArray()

        val result = Extract(numTx.toInt(), mpp, h)  // Validate the merkle block proof, extracting the transaction hashes
        if ((result == null) || (result.size == 0) || (Hash256(result[0]) != hashMerkleRoot))
        {
            throw DeserializationException("Merkle block inconsistent", "network error")
        }

        //val logstr = StringBuilder()
        //logstr.append("Deserialize merkle block: ${hash.toHex()}, requires ${result.size-1} (${numTx}) TX: ")
        // Fill with the hashes we need.  result[0] is the merkle root
        for (i in 1 until result.size)
        {
            txHashes.add(Hash256(result[i]))
            //logstr.append(Hash256(result[i]).toHex())
            //logstr.append(" ")
        }

        //LogIt.finer(sourceLoc() + " " + logstr.toString())
        return stream
    }

    companion object
    {
        @JvmStatic
        external fun Extract(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?
    }
}

class NexaBlock(val chainSelector: ChainSelector) : iBlock, NexaBlockHeader()
{
    override val txes: MutableList<NexaTransaction> = mutableListOf()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(stream)
    }


    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        // serialization for hash calc is only the header, not the tx.  merkle root in header captures tx entropy
        val data = if (format == SerializationType.HASH)
            super.BCHserialize(format)
        else super.BCHserialize(format) + txes
        return data
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        txes.clear()
        txes.addAll(stream.delist { NexaTransaction(chainSelector, it) })
        return stream
    }
}
