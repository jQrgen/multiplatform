// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android
import info.bitcoinunlimited.multiplatform.*
import java.util.*


open class PayAddressException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Expected) : BUException(msg, shortMsg, severity)
open class PayAddressDecodeException(msg: String) : PayAddressException(msg, appI18n(RbadAddress))
open class PayAddressBlankException(msg: String) : PayAddressException(msg, appI18n(RblankAddress), ErrorSeverity.Expected)


fun DbgRender(obj: Hash256): String
{
    return obj.toHex()
}

data class Hash256(val hash: ByteArray = ByteArray(32, { _ -> 0 })) : BCHserializable
{
    init
    {
        assert(hash.size == 32)
    }

    constructor(hex: String) : this()
    {
        val hsh = hex.fromHex()
        hsh.reverse()
        assert(hsh.size == 32)
        hsh.copyInto(hash)
    }

    constructor(stream: BCHserialized) : this()
    {
        BCHdeserialize(stream)
    }

    operator fun get(i: Int) = hash[i]
    operator fun set(i: Int, b: Byte)
    {
        hash[i] = b
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return cpy.toHex()
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = this.toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is Hash256) return hash contentEquals other.hash
        return false
    }

    operator fun compareTo(h: Any?): Int
    {
        if (h is Hash256)
        {
            for (i in 0..32)
            {
                if (hash[i] < h.hash[i]) return -1
                else if (hash[i] > h.hash[i]) return 1
            }
            return 0
        }
        return -1
    }

    override fun hashCode(): Int
    {
        return hash[1].toUint() shl 24 or hash[7].toUint() shl 16 or hash[17].toUint() shl 8 or hash[30].toUint()
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserialized(hash, format)

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        stream.debytes(32).copyInto(hash)
        return stream
    }
}

fun BCHserialized.denullHash(): Hash256?
{
    val v = Hash256()
    v.BCHdeserialize(this)
    if (v == Hash256()) return null
    return v
}


// Globally unique identifier, implemented as the hash256 of the object
class Guid(var data: Hash256 = Hash256()) : BCHserializable
{
    val GUID_LEN_BYTES: Int = 32

    constructor(dat: String) : this(Hash256(dat.fromHex()))
    {
        assert(dat.length == GUID_LEN_BYTES * 2)
    }

    constructor(dat: ByteArray) : this()
    {
        assert(dat.size == GUID_LEN_BYTES)
        data = Hash256(dat)
    }

    fun toHex(): String = data.toHex()

    // Serialization
    constructor(dat: BCHserialized) : this()
    {
        data = Hash256(dat.debytes(GUID_LEN_BYTES.toLong()))
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format) + exactBytes(data.hash)  //.reversedArray()
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        data = Hash256(stream.debytes(GUID_LEN_BYTES.toLong()))
        return stream
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Guid)
        {
            return data.equals(other.data)
        }
        return false
    }

}


/**
An array of block hashes, starting at the current block, and ending at the genesis block.  The creator can skip
blocks, populating whatever hashes he feels is most likely to identify a specific chain, closest to the splitoff point.
Typically, some kind of exponential backoff is used.  For example:
The next 10 hashes are the previous 10 blocks (gap 1).
After these, the gap is multiplied by 2 for each hash, until the genesis block is added
 */
class BlockLocator : BCHserializable
{
    var have: MutableList<Hash256> = mutableListOf()

    fun add(b: Hash256): BlockLocator
    {
        have.add(b)
        return this
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized.int32(0, format) + have
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw UnimplementedException("blockLocator deserialize")  // We only send these locators to full nodes
    }

    override fun toString(): String
    {
        var ret: String = "["
        for (h in have)
        {
            ret += h.toHex() + " "
        }
        ret += "]"
        return ret
    }

}


// Payment Address abstraction
enum class PayAddressType(val v: Byte)
{
    // Must match C++ cashlib.cpp PayAddressType
    NONE(0),
    P2PUBKEY(1),
    P2PKH(2),
    P2SH(3),
    TEMPLATE(4), // Generalized pay to script template
    P2PKT(5)  // Pay to well-known script template 1 (pay-to-pub-key-template)
}

fun LoadPayAddressType(v: Byte): PayAddressType
{
    return when (v)
    {
        0.toByte() -> PayAddressType.NONE
        1.toByte() -> PayAddressType.P2PUBKEY
        2.toByte() -> PayAddressType.P2PKH
        3.toByte() -> PayAddressType.P2SH
        4.toByte() -> PayAddressType.TEMPLATE
        5.toByte() -> PayAddressType.P2PKT
        else -> throw PayAddressDecodeException("unknown address type")
    }
}

data class GroupId(var data: ByteArray) : BCHserializable
{
    companion object
    {
        val GROUP_ID_MIN_SIZE = 32
        val GROUP_ID_MAX_SIZE = 520  // stack size

        @JvmStatic
        external fun ToAddr(chainSelector: Byte, data: ByteArray): String
        @JvmStatic
        external fun FromAddr(chainSelector: Byte, addr: String): ByteArray
    }

    init
    {
        assert(data.size >= GROUP_ID_MIN_SIZE)
        assert(data.size <= GROUP_ID_MAX_SIZE)
    }

    // Accept either a hex string or an address format
    constructor(s: String) : this(
      try
      {
          FromAddr(ChainSelectorFromAddress(s).v, s)
      }
      catch (e: UnknownBlockchainException)  // Its not an address, try hex
      {
          s.fromHex()
      }
      catch (e: java.lang.IllegalStateException)  // decoding from address failed, try hex
      {
          s.fromHex()
      }
    )
    {
    }

    // == compares the groupId bytes for equality
    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is GroupId || !data.contentEquals(other.data)) return false
        return true
    }

    // Returns true is this group is a subgroup
    fun isSubgroup(): Boolean = data.size > GROUP_ID_MIN_SIZE

    // Returns the parent group of this group (or the group itself if this group is not a subgroup)
    fun parentGroup(): GroupId = GroupId(data.sliceArray(0 until GROUP_ID_MIN_SIZE))

    // Returns the unique data associated with this subgroup
    fun subgroupData(): ByteArray = data.sliceArray(GROUP_ID_MIN_SIZE until data.size)

    fun subgroup(subData: ByteArray): GroupId
    {
        return GroupId(data + subData)
    }

    // Returns true if this group is covenanted (grouped output script must = input script)
    fun isCovenanted(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toUint() and 1) == 1
    }

    // Returns true if this group is holding the native crypto rather than tokens
    @Deprecated("use isFenced()")
    fun isHoldingNative(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toUint() and 2) == 2
    }

    // Returns true if this group is holding the native crypto rather than tokens
    fun isFenced(): Boolean
    {
        return (data[GROUP_ID_MIN_SIZE - 1].toUint() and 2) == 2
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + variableSized(data)
        }
        else if (format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            data = stream.deByteArray()
        }
        else if (stream.format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
        }
        return stream
    }

    fun toHex() = data.toHex()

    // TODO fix chainselector
    override fun toString(): String = ToAddr(ChainSelector.NEXA.v, data)

    fun toByteArray() = data

}

// Need efficient bit manipulation so not using enum
@kotlin.ExperimentalUnsignedTypes
class GroupAuthorityFlags
{
    companion object
    {
        // TODO fix when kotlin figures out ulong
        //const val NO_AUTHORITY = 0x0000000000000000UL
        val NO_AUTHORITY = 0x0000000000000000L.toULong()

        //const val AUTHORITY = 0x8000000000000000UL
        val AUTHORITY = 0x4000000000000000L.toULong().shl(1)

        //const val MINT      = 0x4000000000000000L
        val MINT = 0x4000000000000000L.toULong()

        //const val MELT      = 0x2000000000000000L
        val MELT = 0x2000000000000000L.toULong()

        //const val BATON     = 0x1000000000000000L
        val BATON = 0x1000000000000000L.toULong()

        //const val RESCRIPT  = 0x0800000000000000L
        val RESCRIPT = 0x0800000000000000L.toULong()

        //const val SUBGROUP  = 0x0400000000000000L
        val SUBGROUP = 0x0400000000000000L.toULong()

        //const val ALL_AUTHORITY_BITS = 0xffff000000000000U
        val ALL_AUTHORITIES = AUTHORITY or MINT or MELT or BATON or RESCRIPT or SUBGROUP

        fun toString(authBits: ULong): String
        {
            val ret = StringJoiner(",")
            val zero = 0L.toULong()
            if ((authBits and GroupAuthorityFlags.AUTHORITY) == zero) return ""
            if ((authBits and MINT) != zero) ret.add("MINT")
            if ((authBits and MELT) != zero) ret.add("MELT")
            if ((authBits and RESCRIPT) != zero) ret.add("RESCRIPT")
            if ((authBits and SUBGROUP) != zero) ret.add("SUBGROUP")
            if ((authBits and BATON) != zero) ret.add("BATON")
            return ret.toString()
        }
    }
}

@kotlin.ExperimentalUnsignedTypes
data class GroupInfo(var groupId: GroupId, var tokenAmt: Long, var authorityFlags: ULong = 0.toULong())
{
    // Returns true is this group is a subgroup
    fun isSubgroup(): Boolean = groupId.isSubgroup()
    fun isAuthority(): Boolean = (authorityFlags and GroupAuthorityFlags.AUTHORITY) > 0.toULong()

    override fun toString(): String
    {
        val ret = StringBuilder()
        if (isAuthority())
        {
            ret.append(GroupAuthorityFlags.toString(authorityFlags))
            ret.append(" for ")
        }
        else
        {
            ret.append(tokenAmt)
            ret.append(" of ")
        }
        ret.append(groupId)
        return ret.toString()
    }
}

@cli(Display.Simple, "A means to communicate constraint scripts between people or applications.  You can send coins or tokens to an address.")
data class PayAddress(var blockchain: ChainSelector, var type: PayAddressType, var data: ByteArray) : BCHserializable
{
    constructor(chain: ChainSelector, stream: BCHserialized) : this(chain, PayAddressType.P2PUBKEY, ByteArray(0))
    {
        BCHdeserialize(stream)
    }

     @cli(Display.Simple, "Construct from a string address")
    constructor(address: String) : this(ChainSelectorFromAddress(address), PayAddressType.P2PUBKEY, ByteArray(0))
    {
        fromString(address)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + BCHserialized.uint8(blockchain.v) + BCHserialized.uint8(type.v) + variableSized(data)
        }
        else if (format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // return BCHserialized(format) + exactBytes(data)
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            blockchain = ChainSelectorFromValue(stream.debyte())
            type = LoadPayAddressType(stream.debyte())
            data = stream.deByteArray()
        }
        else if (stream.format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // data = stream.debytes(20)
        }
        return stream
    }

    @cli(Display.Simple, "Load this object from a string address")
    fun fromString(address:String)
    {
        if (address == "") throw PayAddressBlankException("")
        val decoded = DecodeCashAddr(blockchain.v, address)
        type = LoadPayAddressType(decoded[0])
        data = decoded.sliceArray(IntRange(1, decoded.size-1))
        /*
        // script template payment text addresses contain the output script serialized as a byte array.  Strip off the serialization for insertion into data.
        // Other stuff in the address (after the first array) can be ignored -- may be app-level data
        if (type == PayAddressType.P2PKT || type == PayAddressType.TEMPLATE)
        {
            val strm = BCHserialized(data, SerializationType.NETWORK)
            data = strm.deByteArray()
        }
         */
    }

    @cli(Display.Simple, "Convert to this address's string representation")
    override fun toString(): String
    {
        if (type == PayAddressType.NONE)
        {
            throw java.lang.IllegalArgumentException("Invalid address type")
        }
        if (type == PayAddressType.P2PKH || type == PayAddressType.P2SH)
        {
            if (data.size != 20)
            {
                throw java.lang.IllegalArgumentException("Invalid dataload. Expected 20 bytes in a P2PKH address, got " + data.size)
            }
        }
        return EncodeCashAddr(blockchain.v, type.v, data)
    }

    /** return the prefix for this address, e.g. "bchreg" in "bchreg:qpvdragqrmvashle90kjjx7hx87aq6xe75jlnlxc9c" */
    @cli(Display.Simple, """return the prefix for this address, e.g. "nexa" in "nexa:qpvdragqrmvashle90kjjx7hx87aq6xe75jlnlxc9c"""")
    val addressUriScheme: String
        get() = chainToURI[blockchain]!!  // !! because its a coding error if this dictionary doesn't contain every ChainSelector enum item

    @cli(Display.Simple, "Create an output script that is constrained to this address")
    fun outputScript(): SatoshiScript
    {
        val script = when (type)
        {
            PayAddressType.NONE -> throw WalletNotSupportedException("Cannot create payment unconstrained by an address")
            PayAddressType.P2PUBKEY -> throw WalletNotSupportedException("Pay to public key outputs not supported")
            PayAddressType.P2PKH -> SatoshiScript.p2pkh(data, blockchain)
            PayAddressType.P2SH -> SatoshiScript.p2sh(data, blockchain)
            PayAddressType.TEMPLATE -> SatoshiScript(blockchain, SatoshiScript.Type.TEMPLATE, BCHserialized(data, SerializationType.NETWORK).deByteArray())
            PayAddressType.P2PKT -> SatoshiScript(blockchain, SatoshiScript.Type.TEMPLATE, BCHserialized(data, SerializationType.NETWORK).deByteArray())
            // unnecessary: else                    -> throw PayAddressException("Payment address not supported")
        }
        return script
    }

    /** synonym for outputScript()
     * @return the script that constrains spending to this address
     */
    @cli(Display.Simple, "Create the constraint script that corresponds to this address")
    fun constraintScript(): SatoshiScript = outputScript()

    @cli(Display.Simple, "Create a grouped constraint script that corresponds to this address")
    fun groupedConstraintScript(grp: GroupId, amt: Long): SatoshiScript
    {
        val script = when (type)
        {
            PayAddressType.NONE -> throw WalletNotSupportedException("Cannot create payment unconstrained by an address")
            PayAddressType.P2PUBKEY -> throw WalletNotSupportedException("Pay to public key outputs not supported")
            PayAddressType.P2PKH -> throw WalletNotSupportedException("grouped P2PKH not supported")
            PayAddressType.P2SH -> throw WalletNotSupportedException("grouped P2SH not supported")
            PayAddressType.TEMPLATE -> throw WalletNotSupportedException("general script templates not supported")
            PayAddressType.P2PKT -> SatoshiScript.gp2pkt(blockchain, grp, amt, BCHserialized(data, SerializationType.NETWORK).deByteArray())
        }
        return script
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
        //return super.equals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is PayAddress)
        {
            return ((type == other.type) && (data.contentEquals(other.data)))
        }
        return false
    }

    override fun hashCode(): Int
    {
        val dhc = data.contentHashCode()
        val hc = type.ordinal.toInt().xor(dhc)
        return hc
    }

    companion object
    {
        // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
        @JvmStatic
        external fun EncodeCashAddr(chainSelector: Byte, type: Byte, data: ByteArray): String

        // data is the flattened "raw" script bytes.  "raw" means not wrapped into a BCHserialized
        @JvmStatic
        external fun DecodeCashAddr(chainSelector: Byte, addr: String): ByteArray
    }
}
