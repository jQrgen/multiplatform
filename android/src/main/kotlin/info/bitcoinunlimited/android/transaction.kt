// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.BUException
import info.bitcoinunlimited.multiplatform.ErrorSeverity
import info.bitcoinunlimited.multiplatform.RbadTransaction
import info.bitcoinunlimited.multiplatform.platform.Logger
import java.lang.IndexOutOfBoundsException

private val TAG = "BU.transaction"

open class TransactionException(msg: String) : BUException(msg, appI18n(RbadTransaction), ErrorSeverity.Expected)

const val OWNERSHIP_CHALLENGE_VERSION_MASK = 0x80

data class NexaTxOutpoint(val hash: Hash256) : iTxOutpoint
{
    companion object
    {
        fun hashOf(h:Hash256, idx:Long):Hash256
        {
            val data = h.BCHserialize(SerializationType.HASH) + BCHserialized.uint32(idx)
            return data.sha256()
        }
    }

    constructor(stream: BCHserialized) : this(Hash256(stream))
    {
    }

    constructor(txhash: Hash256, outputIdx: Int): this(hashOf(txhash, outputIdx.toLong()))
    constructor(txhash: Hash256, outputIdx: Long): this(hashOf(txhash, outputIdx))
    constructor(outpointHex: String) : this(Hash256(outpointHex))

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        return BCHserialized(format) + hash
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        hash.BCHdeserialize(stream)
        return stream
    }

    /*
    fun getTransactionOrder(): Long
    {
        return idx
    }*/

    override fun toString(): String
    {
        return "TxOutpoint(\"" + hash.toHex() + "\")"
    }
    override fun toHex(): String = hash.toHex()
}

//@Serializable
class Spendable(val chainSelector: ChainSelector) : BCHserializable
{
    val SPENDABLE_ID = 0x58581.toInt()
    var secret: Secret? = null
    var outpoint:iTxOutpoint? = null
    var priorOutScript = SatoshiScript(chainSelector)   // The script that constrains spending these coins
    var addr: PayAddress? = null  // We could get this from the priorOutScript, but for ease of use its replicated here.  This could be null, post initialization, if priorOutscript is anyone-can-spend.
    var amount: Long = -1
    var redeemScript = SatoshiScript(chainSelector) //ByteArray( 0)

    val prevout:iTxOutput
      get() = txOutputFor(chainSelector, amount, priorOutScript)


    // TODO: secret and addr could/should be replaced by a PayDestination object in
    // BCHspendable.  This is transitional
    var backingPayDestination: PayDestination? = null  // RAM only
    val payDestination: PayDestination?
        get()
        {
            // P2SH multisig is an interesting problem here.  The P2SH address will depend on external data (the pubkeys of cooperating wallets)
            // so the wallet cannot determine if an address is spendable by this wallet without knowing cooperating wallets' pubkeys.

            // Allow override
            if (backingPayDestination != null) return backingPayDestination
            // Otherwise try to figure out the script type to construct the appropriate destination
            // We can short-cut this if we already know the address
            var a = addr
            if (a == null)
            {
                a = priorOutScript.address  // We can attempt to recognize the prior out script.  In this case we can extract an address from it.
            }

            if (a != null)  // Now use that address to determine the script type to create a payment destination
            {
                val sec = secret ?: return null // Can't build a payment if don't know the secret
                if (a.type == PayAddressType.P2PKH)
                {
                    backingPayDestination = Pay2PubKeyHashDestination(chainSelector, sec)
                    return backingPayDestination
                }
                if (a.type == PayAddressType.P2PKT)
                {
                    backingPayDestination = Pay2PubKeyTemplateDestination(chainSelector, sec)
                    return backingPayDestination
                }

                // We can't construct the other types because we don't know info (like the redeem script)
                // We could guess the redeem script if there are P2SH redeem script types.  One common type that would be useful is to wrap every P2PKH into P2SH
                // to increase payment anonymity for those using p2sh (and make the UTXO set a tiny bit smaller per UTXO)
            }
            return null
        }

    var commitHeight: Long = -1
    var commitBlockHash: Guid = Guid()
    var commitTxIdem: Guid = Guid()
    var commitTx: iTransaction? = null

    var spentHeight: Long = -1
    var spentBlockHash: Guid = Guid()
    var spentTxHash: Guid = Guid()
    var spentUnconfirmed: Boolean = false  // True if an unconfirmed spend exists. False if spend tx exists and is confirmed or if spend tx does not exist.
    var spendableUnconfirmed: Long = 0 // > 0 if this UTXO is unconfirmed.  Number indicates the length of the longest spend chain

    var reserved: Long = 0 //*< Has an in-progress (being readied by this wallet) transaction used this?  0 means no, > 0 means yes.  RAM ONLY

    constructor(chainSelector: ChainSelector, out_point: iTxOutpoint, amount: Long) : this(chainSelector)
    {
        // Throw something if the wrong outpoint type is given
        if (chainSelector.isNexaFamily)
        {
            (out_point as NexaTxOutpoint)!!
        }
        if (chainSelector.isBchFamily)
        {
            (out_point as BchTxOutpoint)!!
        }

        outpoint = out_point
        this.amount = amount
    }

    // Serialization
    constructor(chainSelector: ChainSelector, data: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(data)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        if ((format == SerializationType.NETWORK) || (format == SerializationType.HASH))
        {
            return outpoint!!.BCHserialize(format)
        }
        else if (format == SerializationType.DISK)
        {
            val sec = secret?.getSecret() ?: byteArrayOf()
            val a = addr ?: PayAddress(chainSelector, PayAddressType.NONE, byteArrayOf())
            val ret = BCHserialized(format) + SPENDABLE_ID + variableSized(sec) + outpoint!!.BCHserialize(format)
            ret.add(priorOutScript)
            ret.addInt64(amount)
            ret.add(redeemScript)
            ret.addInt64(commitHeight)
            ret.add(commitBlockHash)
            ret.add(commitTxIdem)
              /* commitTx +  since the commitTx contains this BCHinput, that would recurse -- when loading you need to restore this from an alternate source using the commitTxIdem */
            ret.addUint64(spentHeight).add(spentBlockHash)
            ret.add(spentTxHash)
            ret.add(spentUnconfirmed)
            ret.addUint64(spendableUnconfirmed)
            assert(ret.format == format)
            ret.add(a.BCHserialize(format))
            return ret
        }
        else
        {
            throw NotImplementedError()
        }
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        if (stream.format == SerializationType.NETWORK)
        {
            outpoint = outpointFor(chainSelector, stream)
        }
        else if (stream.format == SerializationType.DISK)
        {
            val id = stream.deint32()
            if (id != SPENDABLE_ID)
            {
                Logger.info(TAG,"deserialization corruption!")
                throw DeserializationException("Spendable identifier incorrect")
            }
            val secBytes = stream.deByteArray()
            if (secBytes.size != 0) secret = UnsecuredSecret(secBytes)
            else secret = null
            outpoint = outpointFor(chainSelector, stream)
            priorOutScript.BCHdeserialize(stream)
            amount = stream.deuint64()
            redeemScript.BCHdeserialize(stream)
            commitHeight = stream.deuint64()
            commitBlockHash.BCHdeserialize(stream)
            commitTxIdem.BCHdeserialize(stream)
            spentHeight = stream.deuint64()
            spentBlockHash.BCHdeserialize(stream)
            spentTxHash.BCHdeserialize(stream)
            spentUnconfirmed = stream.deboolean()
            spendableUnconfirmed = stream.deuint64()
            val p = PayAddress(chainSelector, stream)
            addr = if (p.type != PayAddressType.NONE) p else null
        }
        else
        {
            throw NotImplementedError()
        }
        return stream
    }

    override fun toString(): String
    {
        return amount.toString() + " from UTXO " + outpoint.toString()
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = priorOutScript.groupInfo(amount)
}

/** Spend a coin (aka UTXO, prevout) in a transaction */
@cli(Display.Simple, "Input of a bitcoin transaction")
class NexaTxInput(val chainSelector: ChainSelector) : iTxInput
{
    var type:Byte = 0

    @cli(Display.Simple, "What UTXO is being spent")
    override var spendable = Spendable(chainSelector) //!< What prevout to spend

    @cli(Display.Simple, "Satisfier script")
    override var script = SatoshiScript(chainSelector) //!< Satisfier script that proves you can spend this prevout

    @cli(Display.Dev, "enables locktime if not 0xffffffff")
    var sequence: Long = 0xffffffff //!< enable locktime if not 0xffffffff

    override fun toString(): String
    {
        return """{ "utxo" : ${spendable.toString()}, "satisfier" : "${script.toHex()}" }"""
    }

    override fun copy(): iTxInput = BchTxInput(this)
    constructor(copy: iTxInput): this(copy as NexaTxInput)
    /** Copy constructor */
    constructor(copy: NexaTxInput) : this(copy.chainSelector)
    {
        spendable = Spendable(copy.chainSelector, copy.BCHserialize(SerializationType.NETWORK))
        val ser = copy.script.BCHserialize(SerializationType.NETWORK)
        ser.flatten()
        script = SatoshiScript(copy.chainSelector, ser)
        sequence = copy.sequence
    }

    @cli(Display.Dev, "constructor")
    constructor(chainSelector: ChainSelector, spend: Spendable, scriptp: SatoshiScript, seq: Long = 0xffffffff) : this(chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor")
    constructor(spend: Spendable, scriptp: SatoshiScript, seq: Long = 0xffffffff) : this(scriptp.chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor, fill in the satisfier script later")
    constructor(spend: Spendable, seq: Long = 0xffffffff) : this(spend.chainSelector)
    {
        spendable = spend
        script = SatoshiScript(spend.chainSelector)
        sequence = seq
    }

    /** deserialization constructor */
    @cli(Display.Dev, "deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.Dev, "serialization")
    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        var ret = (BCHserialized(format) + BCHserialized.uint8(type) + spendable + script + BCHserialized.uint32(sequence)).addUint64(spendable.amount)
        return ret
    }

    @cli(Display.Dev, "deserialization")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        type = stream.debyte()
        spendable = Spendable(chainSelector, stream)
        script = SatoshiScript(chainSelector, stream)
        sequence = stream.deuint32()
        spendable.amount = stream.deint64()
        return stream
    }
}

/** Convert a script type into the type field needed for the script to be used in a transaction output */
fun cvt(s: SatoshiScript.Type): NexaTxOutput.Type
{
    if (s == SatoshiScript.Type.TEMPLATE)
        return NexaTxOutput.Type.TEMPLATE
    else
        return NexaTxOutput.Type.SATOSCRIPT
}

/** Convert transaction output type into a script type (for correct annotation of the script contained in the output) */
fun cvt(s: NexaTxOutput.Type): SatoshiScript.Type
{
    return when(s)
    {
        NexaTxOutput.Type.TEMPLATE -> SatoshiScript.Type.TEMPLATE
        NexaTxOutput.Type.SATOSCRIPT -> SatoshiScript.Type.SATOSCRIPT
    }
}

/** Convert a TxOutput type raw byte into a type enum */
fun cvt(s: Byte): NexaTxOutput.Type = when (s)
{
    NexaTxOutput.Type.SATOSCRIPT.v -> NexaTxOutput.Type.SATOSCRIPT
    NexaTxOutput.Type.TEMPLATE.v -> NexaTxOutput.Type.TEMPLATE
    else -> throw DeserializationException("Illegal TxOutput type")
}



//@Serializable
/** Output of a bitcoin transaction */
@cli(Display.Simple, "Output of a bitcoin transaction")
class NexaTxOutput(val chainSelector: ChainSelector) : iTxOutput
{
    enum class Type(val v: Byte)
    {
        SATOSCRIPT(0),
        TEMPLATE(1);
    }

    var type = Type.SATOSCRIPT

    @cli(Display.Simple, "Spend quantity in satoshis")
    override var amount: Long = 0 //!< Amount in satoshis assigned to this output

    private var _script = SatoshiScript(chainSelector)

    @cli(Display.Simple, "Constraint script")
    override var script: SatoshiScript //!< The "predicate" script that controls spendability
    get() = _script
    set(s: SatoshiScript)
    {
        _script = s
        type = cvt(s.type)
    }

    /** explicit constructor */
    @cli(Display.Dev, "Constructor")
    constructor(chainSelector: ChainSelector, amt: Long, scr: SatoshiScript = SatoshiScript(chainSelector)) : this(chainSelector)
    {
        amount = amt
        script = scr
        type = cvt(script.type)
    }

    @cli(Display.Dev, "Constructor")
    constructor(amt: Long, scr: SatoshiScript) : this(scr.chainSelector)
    {
        amount = amt
        script = scr
        type = cvt(script.type)
    }

    /** Deserialization constructor */
    @cli(Display.Dev, "Deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    /** Serializer */
    @cli(Display.Dev, "Serialize this output")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format) + type.v + amount + script
        return ret
    }

    @cli(Display.Dev, "Deserializer: Overwrite this object with the contents in the passed stream")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        type = cvt(stream.debyte())
        amount = stream.deint64()
        _script = SatoshiScript(chainSelector, stream)
        _script.type = cvt(type)
        return stream
    }

    @cli(Display.User, "Convert to a human readable form")
    override fun toString(): String
    {
        return amount.toString() + " to " + (_script.address?.toString() ?: ("script " + _script.toString()))
    }

    @cli(Display.User, "Convert to hex")
    fun toHex(): String
    {
        var ser = BCHserialize(SerializationType.NETWORK)
        return ser.flatten().toHex()
    }

    override fun hashCode(): Int
    {
        return amount.hashCode() xor script.hashCode()  // java 1.7 (android API 23-) does not contain Long.hashCode
    }

    override fun equals(other: Any?): Boolean
    {
        if (other !is NexaTxOutput) return false
        if (other.amount != amount) return false
        if (!other.script.contentEquals(this.script)) return false
        return true
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = script.groupInfo(amount)
}




//@Serializable
/** blockchain transaction */
@cli(Display.Simple, "A Nexa blockchain transaction")
class NexaTransaction(override val chainSelector: ChainSelector) : iTransaction
{
    protected var idData: Hash256? = null  //!< cached id of this transaction
    protected var idemData: Hash256? = null  //!< cached idem of this transaction
    protected var sizeData: Long? = null  //!< cached size of this transaction

    @cli(Display.Simple, "version")
    override var version: Int = 0  //!< transaction version
    @cli(Display.Simple, "transaction lock time")
    override var lockTime: Long = 0  //!< transaction lock time

    // These lists allow you type-safe access to the Nexa Tx inputs and outputs.  They are exposed via ichain interfaces as inputs:iTxInput and outputs:iTxOutput.
    // It is better to use the ichain APIs whenever possible so your code will work on multiple blockchains.
    var _inputs = mutableListOf<NexaTxInput>()
    var _outputs = mutableListOf<NexaTxOutput>()

    @cli(Display.Simple, "inputs to the transaction")
    override val inputs: MutableList<out iTxInput>
      get()
      {
          return _inputs
      }
    @cli(Display.User, "erase existing inputs and set to the passed list (which must contain inputs of the correct derived class (NexaTxInput) for this blockchain)")
    override fun setInputs(ins: MutableList<out iTxInput>):iTransaction
    {
        _inputs.clear()
        for (i in ins)
        {
            _inputs.add(i as NexaTxInput)  // throw an exception if an input of the wrong type is in the list
        }
        return this
    }

    @cli(Display.Simple, "transaction outputs")
    override val outputs: MutableList<out iTxOutput>
        get()
        {
            return _outputs
        }
    @cli(Display.User, "erase existing outputs and set to the passed list (which must contain outputs of the correct derived class (NexaTxOutput) for this blockchain)")
    override fun setOutputs(outs: MutableList<out iTxOutput>):iTransaction
    {
        _outputs.clear()
        for (i in outs)
        {
            _outputs.add(i as NexaTxOutput)  // throw an exception if an input of the wrong type is in the list
        }
        return this
    }

    @cli(Display.User, "Append the passed input to the list of inputs.  'input' must be the correct derived class for this blockchain (i.e. NexaTxInput)")
    override fun add(input: iTxInput): iTransaction
    {
        assert(input is NexaTxInput)
        assert(input.spendable.outpoint is NexaTxOutpoint)
        _inputs.add(input as NexaTxInput)
        return this
    }

    @cli(Display.User, "Append the passed output to the list of outputs.  'output' must be the correct derived class for this blockchain (i.e. NexaTxOutput)")
    override fun add(output: iTxOutput):iTransaction
    {
        assert(output is NexaTxOutput)
        _outputs.add(output as NexaTxOutput)
        return this
    }

    companion object
    {
        @JvmStatic
        fun fromHex(chain: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): NexaTransaction
        {
            assert(chain.isNexaFamily)
            val bytes = hex.fromHex()
            return NexaTransaction(chain, BCHserialized(bytes, serializationType))
        }

        @JvmStatic
        external fun txid(data: ByteArray): ByteArray
        @JvmStatic
        external fun txidem(data: ByteArray): ByteArray
    }

    /** Return the outpoints in this transaction */
    @cli(Display.Dev, "transaction outpoints")
    override val outpoints: Array<NexaTxOutpoint>
        get()
        {
            val txIdem = idem
            val ret = mutableListOf<NexaTxOutpoint>()
            for (count in 0 until outputs.size)
            {
                ret.add(NexaTxOutpoint(txIdem, count.toLong()))
            }
            return ret.toTypedArray()
        }

    @cli(Display.Dev, "Satoshis being spent by this transaction")
    val inputSatoshis: Long
        get()
        {
            return inputs.fold(0L) { sum, inp -> sum + inp.spendable.amount }
        }

    @cli(Display.Dev, "transaction outpoints")
    fun spendable(index: Int): Spendable
    {
        if (index >= outputs.size || index < 0)
        {
            throw IndexOutOfBoundsException("illegal transaction output index")
        }
        val ret = Spendable(chainSelector, NexaTxOutpoint(idem, index), outputs[index].amount)
        ret.priorOutScript = outputs[index].script
        ret.commitTxIdem = Guid(this.idem)
        ret.commitTx = this
        return ret
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(s: SatoshiScript): Int
    {
        for ((idx, out) in outputs.withIndex())
        {
            if (out.script.contentEquals(s)) return idx
        }
        throw NoSuchElementException("output script not found")
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(addr: PayAddress): Int = findOutput(addr.constraintScript())

    /** Force recalculation of hash. To access the hash just use #hash */
    @Synchronized
    fun calcId(): Hash256
    {
        val serbytes = BCHserialize(SerializationType.HASH).flatten()
        val hash = Hash256(txid(serbytes))
        idData = hash
        sizeData = serbytes.size.toLong()
        return hash
    }

    /** Force recalculation of hash. To access the hash just use #hash */
    @Synchronized
    fun calcIdem(): Hash256
    {
        val serbytes = BCHserialize(SerializationType.HASH).flatten()
        val hash = Hash256(txidem(serbytes))
        idemData = hash
        sizeData = serbytes.size.toLong()
        return hash
    }

    /** If you change transaction data, call this function to force lazy recalculation of the hash */
    @Synchronized
    override fun changed()
    {
        idData = null
        idemData = null
        sizeData = null
    }


    @cli(Display.Simple, "transaction size in bytes")
    override val size: Long
        @Synchronized
        get()
        {
            while (true)  // in theory at least a thread could nullify sizeData between my calcHash() and loading into ret, so while loop
            {
                val ret = sizeData
                if (ret != null) return ret
                calcId()
            }
        }

    @cli(Display.Simple, "transaction fee")
    override val fee: Long
        get()
        {
            var fee: Long = 0
            for (i in inputs)
            {
                fee += i.spendable.amount
            }
            for (out in outputs)
            {
                fee -= out.amount
            }
            return fee
        }

    /** Return this transaction's id  Uses a cached value if one exists.  If you change the
     * transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    override val id: Hash256
        @Synchronized
        get()
        {
            val temp = idData
            if (temp != null) return temp
            return calcId()
        }
        //set(value)
        //{
         //   idData = value
        //}

    /** Return this transaction's id  Uses a cached value if one exists.  If you change the
     * transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    override val idem: Hash256
        @Synchronized
        get()
        {
            val temp = idemData
            if (temp != null) return temp
            return calcIdem()
        }


    /** Returns true if this transaction is a coinbase tx */
    @cli(Display.Dev, "is this transaction a coinbase?")
    override fun isCoinbase(): Boolean
    {
        // As per c++ codebase
        if (inputs.size == 0) return true  // there must no inputs
        return false
    }

    @cli(Display.Simple, "Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain.")
    override fun isOwnershipChallenge(): Boolean
    {
        if ((version and OWNERSHIP_CHALLENGE_VERSION_MASK) != 0) return true
        return false
    }


    /** Serialize this transaction and return it as a hex encoded string */
    @cli(Display.Simple, "return the serialized hex representation")
    override fun toHex(): String
    {
        var ser = BCHserialize(SerializationType.NETWORK)
        return ser.flatten().toHex()
    }

    /** Default display: print the transaction hash */
    override fun toString(): String = idem.toHex()

    override fun debugDump()
    {
        Logger.info(TAG,"size: $size fee: $fee feerate: ${feeRate} inputs: ${inputs.size} outputs: ${outputs.size}")
        Logger.info(TAG,"INPUTS:")
        for (i in inputs)
        {
            Logger.info(TAG,"  ${i.spendable.amount} ${i.spendable.outpoint?.toHex() ?: "null"} confirmed at: ${i.spendable.commitHeight}")
        }
    }

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: ByteArray, format: SerializationType) : this(chainSelector, BCHserialized(buf, format))

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized  //!< Serializer
    {
        for (i in _inputs)
        {
            assert(i.spendable.outpoint is NexaTxOutpoint)
        }
        // If HASH serialization differs from NETWORK, then the "size" calculation will be incorrect!
        var ret = BCHserialized(format)
        ret.addUint8(version)
        ret.addlist(_inputs)
        ret.addlist(_outputs)
        ret.addUint32(lockTime)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        version = stream.deuint8()
        _inputs = stream.delist({ b -> NexaTxInput(chainSelector, b) })
        _outputs = stream.delist({ b -> NexaTxOutput(chainSelector, b) })
        lockTime = stream.deuint32()
        changed()
        return stream
    }

    /** Return the sighash that allows new inputs and/or outputs to be appended to the transaction (but commits to all existing inputs and outputs) */
    override fun appendableSighash(extendInputs:Boolean, extendOutputs:Boolean): ByteArray
    {
        val inoutFlag:Byte = ((if (extendInputs) 0x10 else 0x00) or (if (extendOutputs) 0x01 else 0x00)).toByte()
        val ret = mutableListOf(inoutFlag)
        if (extendInputs) ret.add(inputs.size.toByte())
        if (extendOutputs) ret.add(inputs.size.toByte())
        return ret.toByteArray()
    }

}
