// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.

// Android Persistence
package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.platform.Logger
import android.database.sqlite.SQLiteConstraintException
import androidx.room.*

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

private val TAG = "BU.bchpersist"

@Entity
open class PBchBlockHeader() : BchBlockHeader()
{

    @PrimaryKey
    @ColumnInfo(name = "id")
    var dbkey: ByteArray = byteArrayOf()

    constructor(b: BchBlockHeader) : this()
    {
        // repeat the search columns because IDK how to get room to work for base class fields
        dbkey = b.hash.hash
        initialize(b)
    }

    /** This constructor lets you override the hash and canned values are used for "known" blocks like the tip */
    constructor(h: ByteArray, b: BchBlockHeader) : this()
    {
        dbkey = h
        initialize(b)
    }

    fun initialize(b: BchBlockHeader)
    {
        // hexHash = b.hexHash
        assignHashData(b.hash)
        hashPrevBlock = b.hashPrevBlock
        diffBits = b.diffBits
        hashAncestor = b.hashAncestor
        hashMerkleRoot = b.hashMerkleRoot
        time = b.time
        height = b.height
        chainWork = b.chainWork
        size = b.size
        txCount = b.txCount
        nonce = b.nonce
    }
}

@Dao
interface BchBlockHeaderDao: BlockHeaderPersist
{
    @Query("SELECT * FROM pBCHBlockHeader")
    fun getAll(): List<PBchBlockHeader>

    @Query("SELECT * FROM pBCHBlockHeader WHERE height IN (:heights)")
    fun loadAllByHeight(heights: IntArray): List<PBchBlockHeader>

    @Query("SELECT * FROM pBCHBlockHeader WHERE height = :height")
    fun get(height: Long): PBchBlockHeader?

    @Query("SELECT * FROM pBCHBlockHeader WHERE height = :height")
    fun getAtHeight(height: Long): List<PBchBlockHeader>

    @Query("SELECT * FROM pBCHBlockHeader WHERE id = :blockid")
    abstract fun get(blockid: ByteArray): PBchBlockHeader?

    //@Query("SELECT * FROM blockHeader x INNER JOIN (SELECT height, MAX(height) FROM blockHeader GROUP BY height) y ON x.height = y.height")
    @Query("SELECT a.* FROM pBCHBlockHeader a LEFT OUTER JOIN pBCHBlockHeader b ON a.height < b.height WHERE b.height IS NULL")
    fun getLast(): PBchBlockHeader

    @Query("SELECT a.* FROM pBCHBlockHeader a LEFT OUTER JOIN pBCHBlockHeader b ON a.chainWork < b.chainWork WHERE b.chainWork IS NULL")
    fun getMostWork(): List<PBchBlockHeader>

    @Insert
    fun insert(vararg bh: PBchBlockHeader)

    @Update
    fun update(bh: PBchBlockHeader)

    @Delete
    fun delete(bh: PBchBlockHeader)

    @Query("DELETE FROM pBCHBlockHeader WHERE height = :height")
    fun delete(height: Long)

    @Query("DELETE FROM pBCHBlockHeader")
    fun deleteAll()

    override fun insertHeader(header: iBlockHeader)
    {
        insert(PBchBlockHeader(header as BchBlockHeader))
    }

    override fun getHeader(height: Long): iBlockHeader?
    {
        val ret = get(height)
        return ret
    }

    override fun getHeader(blockid: ByteArray): iBlockHeader?
    {
        val ret = get(blockid)
        return ret
    }

    override fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    {
        val ret = getAtHeight(height)
        return ret
    }

    override fun getHeader(hash: Hash256): iBlockHeader?
    {
        val ret = get(hash.hash)
        return ret
    }

    override fun getMostWorkHeaders(): List<iBlockHeader> = getMostWork()

    override fun setCachedTipHeader(header: iBlockHeader)
    {
        setBchCachedTip(header as BchBlockHeader)
    }
    override fun getCachedTipHeader(): iBlockHeader?
    {
        return this.getBchCachedTip()
    }

    override fun clear()
    {
        deleteAll()
    }

    companion object
    {
        val lock = ThreadCond()
    }

    override fun diffUpsert(header: iBlockHeader)
    {
        synchronized(lock)
        {
            val h = header as BchBlockHeader
            h.calcHash()
            val curbh = get(h.hash.hash)
            // This can happen legitimately when additional fields are calculated (that are not actually part of the blockchain header)
            if (curbh == null)
            {
                try
                {
                    insert(PBchBlockHeader(h))
                }
                catch (e: Exception)
                {
                    update(PBchBlockHeader(h))
                }
            }
            else if (curbh != header)
                update(PBchBlockHeader(h))
        }
    }
}

fun BchBlockHeaderDao.getBchCachedTip() = get(byteArrayOf(0))

fun BchBlockHeaderDao.setBchCachedTip(header: BchBlockHeader)
{
    val pbh = PBchBlockHeader(byteArrayOf(0), header)
    try
    {
        insert(pbh)
    }
    catch (exception: SQLiteConstraintException)
    {
        update(pbh)
    }

}

/*
fun PersistInsert(dbdao: BCHBlockHeaderDao, bh: iBlockHeader)
{
    while (true)
    {
        try
        {
            dbdao.insert(PBCHBlockHeader(bh as BCHBlockHeader))
            return
        }
        catch (e: android.database.sqlite.SQLiteConstraintException)
        {
            print(e.toString())
            val sqle = e
            if (sqle.toString().contains("SQLITE_CONSTRAINT_PRIMARYKEY"))
            {
                Logger().info(TAG, "Inserting duplicate block header")
                return  // Its ok to insert the same block header, but a waste
            }
            if (!sqle.toString().contains("SQLITE_BUSY"))
                throw(e)
        }
    }
}
 */

@Database(entities = arrayOf(PBchBlockHeader::class), version = 3)
@TypeConverters(Hash256Converters::class, BigIntegerConverters::class)
abstract class BchBlockHeaderDatabase : RoomDatabase()
{
    abstract fun blockHeaderDao(): BchBlockHeaderDao
}

fun BchOpenBlockHeaderDB(context: PlatformContext, name: String): BchBlockHeaderDatabase
{
    val db = Room.databaseBuilder(context.context, BchBlockHeaderDatabase::class.java, name).fallbackToDestructiveMigration().build()
    return db
}

/** Delete all block headers in the database -- used in regression testing */
fun BchDeleteBlockHeaders(name: String, dbPrefix: String, context: PlatformContext)
{
    val db = BchOpenBlockHeaderDB(context, dbPrefix + name)
    val dbao = db.blockHeaderDao()
    dbao.deleteAll()
}