package info.bitcoinunlimited.android

import java.math.BigInteger

interface iBlockHeader: BCHserializable
{
    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    val hash: Hash256

    /** Force recalculation of hash. To access the hash just use #hash, to force recalc call "changed()" */
    abstract fun calcHash(): Hash256

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    abstract fun changed()

    @cli(Display.Simple, "previous block hash")
    var hashPrevBlock: Hash256

    @cli(Display.Simple, "difficulty in 'bits' representation")
    var diffBits: Long

    @cli(Display.Simple, "ancestor")
    open var hashAncestor: Hash256 // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    var hashMerkleRoot: Hash256

    @cli(Display.Simple, "block timestamp")
    var time: Long

    @cli(Display.Simple, "block height")
    var height: Long

    @cli(Display.Simple, "cumulative work in the chain")
    var chainWork: BigInteger

    @cli(Display.Simple, "block size in bytes")
    var size: Long

    @cli(Display.Simple, "number of transactions in block")
    var txCount: Long

    @cli(Display.Simple, "expected # of hashes to solve this block")
    val work: BigInteger
      get() = Blockchain.workFromDifficultyBits(diffBits)
}

open abstract class CommonBlockHeader: iBlockHeader
{
    //protected var hexHash = String()
    protected var hashData: Hash256? = null

    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    override val hash: Hash256
        get()
        {
            var temp = hashData
            if (temp == null) // ((temp == null) || (temp == Hash256()))
            {
                temp = calcHash()
                hashData = temp
            }
            return temp
        }

    /** Communicate that this object has been changed, so recalculation of cached data is needed */
    override fun changed()
    {
        hashData = null
    }

    // For efficiency only, this manually assigns the hash rather than recalculating it.  Used in disk load functions
    fun assignHashData(h: Hash256) { hashData = h }


    @cli(Display.Simple, "previous block hash")
    override var hashPrevBlock = Hash256()

    @cli(Display.Simple, "difficulty in 'bits' representation")
    override var diffBits: Long = 0L // nBits

    @cli(Display.Simple, "ancestor")
    override open var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved

    @cli(Display.Simple, "merkle root hash")
    override var hashMerkleRoot = Hash256()

    @cli(Display.Simple, "block timestamp")
    override var time: Long = 0L

    @cli(Display.Simple, "block height")
    override var height: Long = -1L

    @cli(Display.Simple, "cumulative work in the chain")
    override var chainWork: BigInteger = 0.toBigInteger()

    @cli(Display.Simple, "block size in bytes")
    override var size: Long = -1L

    @cli(Display.Simple, "number of transactions in block")
    override var txCount: Long = 0L
}

interface iMerkleBlock: iBlockHeader
{
    abstract val txHashes: MutableSet<Hash256>
    abstract val txes: MutableList<out iTransaction>
    @cli(Display.Simple, "Returns true if all transactions defined in this merkle block have arrived (via calls to txArrived())")
    abstract val complete: Boolean

    fun txArrived(tx: iTransaction): Boolean

}

/** Reference to a UTXO */
interface iTxOutpoint : BCHserializable
{
    @cli(Display.Simple, "return the serialized hex representation")
    abstract fun toHex(): String
}

/** defines new UTXOs */
interface iTxOutput : BCHserializable
{
    var amount: Long //!< Amount in satoshis assigned to this output
    var script: SatoshiScript  // !< Constraints to spend this output
}

/** defines what UTXOs are being spent and proves ability to spend */
interface iTxInput : BCHserializable
{
    var spendable: Spendable
    var script: SatoshiScript

    /** Make a shallow copy of this object */
    fun copy(): iTxInput
}

interface iTransaction: BCHserializable
{
    @cli(Display.Simple, "Which blockchain is this transaction for")
    val chainSelector: ChainSelector

    @cli(Display.Simple, "transaction inputs")
    val inputs:MutableList<out iTxInput> //!< inputs to this transaction
    //!< Add an input to this transaction (at the end)
    fun add(input: iTxInput):iTransaction
    fun setInputs(inputs: MutableList<out iTxInput>):iTransaction

    @cli(Display.Simple, "transaction outputs")
    val outputs: MutableList<out iTxOutput> //!< outputs of this transaction
    //!< Add an output to this transaction (at the end)
    fun add(output: iTxOutput):iTransaction
    fun setOutputs(outputs: MutableList<out iTxOutput>):iTransaction

    @cli(Display.Dev, "transaction outpoints")
    val outpoints: Array<out iTxOutpoint>

    @cli(Display.Simple, "version")
    var version: Int //!< transaction version

    @cli(Display.Simple, "transaction lock time")
    var lockTime: Long //!< transaction lock time

    @cli(Display.Simple, "transaction size in bytes")
    val size: Long //!< transaction size in bytes

    /** Return this transaction's idem. If you change the transaction be sure to call changed() to update this value.
     * Returns the id for blockchains that do not have an idem. */
    @cli(Display.Simple, "transaction idem")
    val idem: Hash256

    /** Return this transaction's id. If you change the transaction be sure to call changed() to update this value */
    @cli(Display.Simple, "transaction id")
    val id: Hash256

    /** Communicate that this transaction has been changed, so recalculation of id and idem are needed */
    abstract fun changed()

    @cli(Display.Simple, "return true if this is a coinbase transaction")
    abstract fun isCoinbase(): Boolean

    @cli(Display.Simple, "Return true if this is an ownership challenge transaction.  This means that this transaction is invalid and so can never be actually committed to the blockchain.")
    abstract fun isOwnershipChallenge(): Boolean

    @cli(Display.Simple, "return the serialized hex representation")
    abstract fun toHex(): String

    @cli(Display.Simple, "transaction fee")
    abstract val fee: Long

    @cli(Display.Simple, "transaction fee rate in satoshi/byte")
    val feeRate: Double
        get()
        {
            return fee.toDouble() / size.toDouble()
        }

    val inputTotal: Long
        get()
        {
            var total = 0L
            for (i in inputs) total += i.spendable.amount
            return total
        }

    /** Return the sighash that allows new inputs and/or outputs to be appended to the transaction (but commits to all existing inputs and outputs) */
    abstract fun appendableSighash(extendInputs:Boolean = true, extendOutputs:Boolean=true): ByteArray

    abstract fun debugDump()
}

fun txFor(cs: ChainSelector): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction(cs)
    if (cs.isBchFamily) return BchTransaction(cs)
    throw IllegalArgumentException()
}

fun txFor(cs: ChainSelector, ser: BCHserialized): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction(cs, ser)
    if (cs.isBchFamily) return BchTransaction(cs, ser)
    throw IllegalArgumentException()
}

fun outpointFor(cs: ChainSelector, ser: BCHserialized): iTxOutpoint
{
    if (cs.isNexaFamily) return NexaTxOutpoint(ser)
    if (cs.isBchFamily) return BchTxOutpoint(ser)
    throw IllegalArgumentException()
}

fun outpointFor(cs: ChainSelector, idem: Hash256, idx:Long): iTxOutpoint
{
    if (cs.isNexaFamily) return NexaTxOutpoint(idem, idx)
    if (cs.isBchFamily) return BchTxOutpoint(idem, idx)
    throw IllegalArgumentException()
}

fun txFromHex(cs: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): iTransaction
{
    if (cs.isNexaFamily) return NexaTransaction.fromHex(cs, hex, serializationType)
    if (cs.isBchFamily) return BchTransaction.fromHex(cs, hex, serializationType)
    throw IllegalArgumentException()
}

fun blockHeaderFromHex(cs: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.NETWORK): iBlockHeader
{
    if (cs.isNexaFamily) return NexaBlockHeader.fromHex(hex, serializationType)
    if (cs.isBchFamily) return BchBlockHeader.fromHex(hex, serializationType)
    throw IllegalArgumentException()
}

fun txInputFor(s: Spendable): iTxInput
{
    if (s.chainSelector.isNexaFamily) return NexaTxInput(s)
    if (s.chainSelector.isBchFamily) return BchTxInput(s)
    throw IllegalArgumentException()
}
fun txInputFor(cs: ChainSelector): iTxInput
{
    if (cs.isNexaFamily) return NexaTxInput(cs)
    if (cs.isBchFamily) return BchTxInput(cs)
    throw IllegalArgumentException()
}

fun txOutputFor(cs: ChainSelector): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs)
    if (cs.isBchFamily) return BchTxOutput(cs)
    throw IllegalArgumentException()
}

fun txOutputFor(cs: ChainSelector,ser: BCHserialized): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs, ser)
    if (cs.isBchFamily) return BchTxOutput(cs, ser)
    throw IllegalArgumentException()
}

fun txOutputFor(cs: ChainSelector, amount: Long, script: SatoshiScript): iTxOutput
{
    if (cs.isNexaFamily) return NexaTxOutput(cs, amount, script)
    if (cs.isBchFamily) return BchTxOutput(cs, amount, script)
    throw IllegalArgumentException()
}

fun blockHeaderFor(cs: ChainSelector, ser: BCHserialized): iBlockHeader
{
    if (cs.isNexaFamily) return NexaBlockHeader(ser)
    if (cs.isBchFamily) return BchBlockHeader(ser)
    throw IllegalArgumentException()
}

fun merkleBlockFor(cs: ChainSelector, ser: BCHserialized): iMerkleBlock
{
    if (cs.isNexaFamily) return NexaMerkleBlock(cs, ser)
    if (cs.isBchFamily) return BchMerkleBlock(cs, ser)
    throw IllegalArgumentException()
}


interface iBlock: iBlockHeader
{
    val txes: List<out iTransaction>
}

// common type for Nexa and Bch header DAOs
interface BlockHeaderPersist
{
    // Get a main chain (a chain with the most work) header by its height
    fun getHeader(height: Long): iBlockHeader?
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: ByteArray): iBlockHeader?
    // Multiple chains may exist at this height so a list is returned
    fun getHeadersAtHeight(height: Long): List<iBlockHeader>
    // Multiple chain tips might have the same total work so a list is returned
    fun getMostWorkHeaders(): List<iBlockHeader>
    // If a header identified by this hash does not exist, return null
    fun getHeader(hash: Hash256): iBlockHeader?

    // For quick access, the current main chain tip can be specially stored
    fun setCachedTipHeader(header: iBlockHeader)
    // Get the current main chain tip.
    fun getCachedTipHeader(): iBlockHeader?
    // Add a header into the DB.
    fun insertHeader(header: iBlockHeader)
    // Add a header into the DB, update the existing header if it does not match what is passed.  This limits database writes.
    fun diffUpsert(header: iBlockHeader)   // Read header, if different or nonexistent insert or update
    // Delete all headers.
    fun clear()
}