package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.BUException
import info.bitcoinunlimited.multiplatform.RbadCryptoCode
import info.bitcoinunlimited.multiplatform.platform.Logger

private val TAG = "BU.init"
open class BadCryptoException(msg: String = "bad crypto code") : BUException(msg, appI18n(RbadCryptoCode))

var runningTheTests = false
val SimulationHostIP = "10.0.2.2"
val LanHostIP = "192.168.1.100"
var dbPrefix = if (runningTheTests) "guitest_" else if (REG_TEST_ONLY == true) "regtest_" else ""

var cnxnMgrs: MutableMap<ChainSelector, CnxnMgr> = mutableMapOf()
fun GetCnxnMgr(chain: ChainSelector, name: String? = null, start:Boolean = true): CnxnMgr
{
    synchronized(cnxnMgrs)
    {
        Logger.info(TAG,sourceLoc() + " " + "Get Cnxn Manager")
        val existing = cnxnMgrs[chain]
        if (existing != null) return existing

        val n = name ?: chainToURI[chain] ?: "unknown"
        val result = when (chain)
        {
            ChainSelector.NEXATESTNET ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf("68.183.223.81", "testnetseeder.nexa.org"))
                cmgr.desiredConnectionCount = 1 // nexa testnet won't have lots of nodes
                cmgr
            }
            ChainSelector.NEXAREGTEST ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXAREGTEST, arrayOf(SimulationHostIP, LanHostIP))
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.NEXA ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXA, arrayOf("seed.nexa.org","68.183.223.81","159.203.179.109"))
                cmgr.desiredConnectionCount = 3
                cmgr
            }
            ChainSelector.BCHTESTNET ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.NEXATESTNET, arrayOf("testnet4-seed-bch.bitcoinforks.org", "testnet4-seed-bch.toom.im", "seed.tbch4.loping.net"))
                cmgr.desiredConnectionCount = 2
                cmgr
            }
            ChainSelector.BCHREGTEST ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCHREGTEST, arrayOf(SimulationHostIP, LanHostIP))
                cmgr.desiredConnectionCount = 1 // Regtest will just be running on 1 node
                cmgr
            }
            ChainSelector.BCH ->
            {
                val cmgr = MultiNodeCnxnMgr(n, ChainSelector.BCH, arrayOf("seed.bitcoinunlimited.info","seed.bchd.cash"))
                cmgr.desiredConnectionCount = 2 // Regtest will just be running on 1 node
                cmgr
            }

            else -> throw BadCryptoException("bad crypto code")
        }
        result.getElectrumServerCandidate = { chain -> ElectrumServerOn(chain) }
        if (start) result.start()
        cnxnMgrs[chain] = result
        return result
    }
}

fun ElectrumServerOn(chain: ChainSelector): IpPort
{
    return when (chain)
    {
        ChainSelector.BCH -> IpPort("electrum.seed.bitcoinunlimited.net", DEFAULT_TCP_ELECTRUM_PORT)
        ChainSelector.BCHREGTEST -> IpPort(SimulationHostIP, 60401)
        ChainSelector.NEXAREGTEST -> IpPort(SimulationHostIP, 30401)
        // TODO: point these IPs to a seeder
        ChainSelector.NEXA -> IpPort("electrum.nexa.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT)
        ChainSelector.NEXATESTNET -> IpPort(SimulationHostIP, DEFAULT_NEXATEST_TCP_ELECTRUM_PORT)
            //IpPort("electrumserver.seeder.nexa.org", 7229)
        else -> throw BadCryptoException()
    }
}

var blockchains: MutableMap<ChainSelector, Blockchain> = mutableMapOf()
fun GetBlockchain(chainSelector: ChainSelector, cnxnMgr: CnxnMgr, context: PlatformContext, name: String? = null, start:Boolean = true): Blockchain
{

    synchronized(blockchains)
    {
        Logger.info(TAG,sourceLoc() + " " + "Get Blockchain")
        val existing = blockchains[chainSelector]
        if (existing != null) return existing
        val nexaRegTestGb = Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")
        val nexaTestnetGb = Hash256("508c843a4b98fb25f57cf9ebafb245a5c16468f06519cdd467059a91e7b79d52")
        val result = when (chainSelector)
        {
            ChainSelector.BCHTESTNET -> Blockchain(
              ChainSelector.BCHTESTNET,
              name ?: "TBCH",
              cnxnMgr,
              Hash256("000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943"),
              Hash256("000000000003cab8d8465f4ea4efcb15c28e5eed8e514967883c085351c5b134"),
              Hash256("000000000005ae0f3013e89ce47b6f949ae489d90baf6621e10017490f0a1a50"),
              1348366,
              "52bbf4d7f1bcb197f2".toBigInteger(16),
              context, dbPrefix
            )

            // Regtest for use alongside testnet
            ChainSelector.BCHREGTEST -> Blockchain(
              ChainSelector.BCHREGTEST,
              name ?: "RBCH",
              cnxnMgr,
              // If checkpointing the genesis block, set the prior block id to the genesis block as well
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              0,
              0.toBigInteger(),
              context, dbPrefix
            )

            // Bitcoin Cash mainnet chain
            ChainSelector.BCH -> Blockchain(
              ChainSelector.BCH,
              name ?: "BCH",
              cnxnMgr,
              genesisBlockHash = Hash256("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206"),
              checkpointPriorBlockId = Hash256("000000000000000002195635b4b14a0054eeaf6d219c521078e1297425381c3a"),
              checkpointId = Hash256("000000000000000004c366e89454a7c071e6293d398b45652128a10d38d21675"),
              checkpointHeight = 750500,
              checkpointWork = "19bba64c36dab2acd254f1c".toBigInteger(16),
              context = context, dbPrefix = dbPrefix
            )

            // Nexa regtest chain
            ChainSelector.NEXAREGTEST -> Blockchain(
              ChainSelector.NEXAREGTEST,
              name ?: "RNEX",
              cnxnMgr,
              genesisBlockHash = nexaRegTestGb,
              checkpointPriorBlockId = nexaRegTestGb,
              checkpointId = nexaRegTestGb,
              checkpointHeight = 0,
              checkpointWork = 0.toBigInteger(),
              context = context,
              dbPrefix = dbPrefix
            )
            // Nexa testnet chain
            ChainSelector.NEXATESTNET -> Blockchain(
              ChainSelector.NEXATESTNET,
              name ?: "TNEX",
              cnxnMgr,
              genesisBlockHash = nexaTestnetGb,
              checkpointPriorBlockId = Hash256("a37262e06459fb1c2e9ede06d2f040566c1428b53a40034734c6b3ba286d0f8d"),
              checkpointId = Hash256("b927b4ad1db13c2c32124f833cabeb0a69c61502ad388ebd7f596fa883adb3ec"),
              checkpointHeight = 130000,
              checkpointWork = "484b4becf5".toBigInteger(16),
              context = context,
              dbPrefix = dbPrefix
            )
            // Nexa mainnet chain
            ChainSelector.NEXA -> Blockchain(
              ChainSelector.NEXA,
              name ?: "NEX",
              cnxnMgr,
              genesisBlockHash = Hash256("edc7144fe1ba4edd0edf35d7eea90f6cb1dba42314aa85da8207e97c5339c801"),
              checkpointPriorBlockId = Hash256("a334c3b93b8e1c6c5a3379be407f034802be3b4f8e98c87738641d7308f6e56c"),
              checkpointId = Hash256("4121b1b8a7aa0456c0fd7f2684787d0473c053a2cf2a61949759cd3cee5b7c1b"),
              checkpointHeight = 6500,
              checkpointWork = 0x5c0efa47d0.toBigInteger(),
              context = context,
              dbPrefix = dbPrefix
            )
            else -> throw BadCryptoException()
        }

        if (start) result.start()
        blockchains[chainSelector] = result
        return result
    }
}
