package info.bitcoinunlimited.android

// / C-bindings to libfujisaki_ringsig, see `ringsigandroid.cpp`
typealias KeyPairPtr = Long
typealias TagPtr = Long

external fun generate_keypair(): KeyPairPtr
external fun generate_keypair_from_bits(bits: ByteArray): KeyPairPtr
external fun free_keypair(keyPair: KeyPairPtr)
external fun get_privkey(keyPair: KeyPairPtr): ByteArray
external fun get_pubkey(keyPair: KeyPairPtr): ByteArray
external fun init_tag(issue: ByteArray): TagPtr
external fun tag_add_pubkey(tag: TagPtr, pubkey: ByteArray)
external fun free_tag(tag: TagPtr)
external fun sign_message(msg: ByteArray, tag: TagPtr, privkey: ByteArray): ByteArray
external fun verify_message(msg: ByteArray, tag: TagPtr, signature: ByteArray): Boolean
external fun trace_signature(
    message1: ByteArray,
    signature1: ByteArray,
    message2: ByteArray,
    signature2: ByteArray,
    tag: TagPtr
): Array<Any>

public enum class TraceResult {
    /**
     * `Indep` indicates that the two given signatures were constructed with different private
     * keys.
     */
    Indep,

    /**
     * `Linked` indicates that the same private key was used to sign the same message under the
     * same tag. This does not reveal which key performed the double-signature.
     */
    Linked,

    /**
     * The same key was used to sign distinct messages under the same tag. `pubkey_out` reveales
     * that pubkey.
     */
    Revealed,
}

// This is an implementation of the Traceable Ring Signature algorithm by
// Eiichiro Fujisaki and Koutarou Suzuki. This crate uses the curve25519-dalek
// library. In particular, it uses the ristretto module for its elligator
// implementation.
//
// Warning: This crate should not be used in any serious contexts. It is not secure.
//
// To use this class, libbitcoincash needs to be compliled with `ringsignatures=true`
// set in `local.properties`. Otherwise, UnsatisfiedLinkError will be thrown.
class RingSignature {
    companion object {
        init
        {
            System.loadLibrary("ringsigandroid")
        }

        const val PRIVATE_SEED_LEN: Int = 32

        /**
         * Generate a new public/private keypair.
         *
         * Note: Creates a raw pointer that must be freed by calling freeKeyPair
         */
        fun newKeyPair(): KeyPairPtr {
            return generate_keypair()
        }

        /**
         * Generate keypair from seed (deterministic)
         */
        fun newKeyPairFromBits(seed: ByteArray): KeyPairPtr {
            if (seed.size != PRIVATE_SEED_LEN) {
                throw Error("Seed must be $PRIVATE_SEED_LEN bytes, got ${seed.size}")
            }
            return generate_keypair_from_bits(seed)
        }

        // // Free a keypair that was created with `newKeyPair`.
        fun freeKeyPair(keypair: KeyPairPtr) {
            free_keypair(keypair)
        }

        // / Fetch the private key from a keypair
        fun getPrivKey(keypair: KeyPairPtr): ByteArray {
            return get_privkey(keypair)
        }

        // / Fetch the public key from a keypair
        fun getPubKey(keypair: KeyPairPtr): ByteArray {
            return get_pubkey(keypair)
        }

        // / Craete a 'tag' of the ring of public keys which are being used for the ring signature, as well as the
        // / "issue", corresponding to what issue the signature corresponds to (e.g `b"auction number 15"`)
        // /
        // / Note: Creates a raw pointer that must be freed by calling freeTag
        fun createTag(issue: ByteArray, publicKeys: Array<ByteArray>): TagPtr {
            val tag = init_tag(issue)
            for (pk in publicKeys) {
                tag_add_pubkey(tag, pk)
            }
            return tag
        }

        fun freeTag(tag: TagPtr) {
            free_tag(tag)
        }

        fun signMessage(message: ByteArray, tag: TagPtr, privKey: ByteArray): ByteArray {
            return sign_message(message, tag, privKey)
        }

        fun verify(message: ByteArray, tag: TagPtr, signature: ByteArray): Boolean {
            return verify_message(message, tag, signature)
        }

        fun trace(
            message1: ByteArray,
            signature1: ByteArray,
            message2: ByteArray,
            signature2: ByteArray,
            tag: TagPtr
        ): Pair<TraceResult, ByteArray?> {
            val result: Array<Any> = trace_signature(message1, signature1, message2, signature2, tag)
            if (result.size != 2) {
                throw Error("Unexpected return value from `trace_signature`")
            }
            val traceResult: Int = result[0] as Int
            if (traceResult == 1) {
                return Pair(TraceResult.Indep, null)
            }
            if (traceResult == 2) {
                return Pair(TraceResult.Linked, null)
            }
            if (traceResult == 3) {
                return Pair(TraceResult.Revealed, result[1] as ByteArray)
            }
            throw Error("Unknown trace result value $traceResult")
        }
    }
}