// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.*
import info.bitcoinunlimited.multiplatform.platform.Logger
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.json.JsonElement

import java.math.BigInteger
import java.net.SocketException
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.coroutines.CoroutineContext

private val TAG = "BU.blockchain"

val BLOCK_REQ_TIMEOUT = 5000.toLong()
val ELECTRUM_REQ_TIMEOUT = 5000

fun WeekOfBlocks(cs: ChainSelector): Int
{
    return when (cs) {
        /* ChainSelector.BCHMAINNET -> 2016 */
        ChainSelector.NEXA -> 2016*5
        else -> 2016*5
    }
}

open class BlockchainException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) : BUException(msg, shortMsg, severity)
open class BlockNotForthcoming(val blockHash: Guid) : BlockchainException(appI18n(RblockNotForthcoming) + " " + blockHash.toHex(), appI18n(RblockNotForthcoming), ErrorSeverity.Expected)
open class HeadersNotForthcoming(val blockHash: Guid) : BlockchainException(appI18n(RheadersNotForthcoming) + " " + blockHash.toHex(), appI18n(RheadersNotForthcoming), ErrorSeverity.Expected)
open class RequestedPrehistoryHeader(blockHash: Hash256, val lastBlock: iBlockHeader?) : HeadersNotForthcoming(Guid(blockHash))
{
    override fun toString(): String
    {
        return super.toString() + "Prehistory block: " + blockHash.toHex()
    }
}


//? The request manager handles all interactions between this client and the rest of the network.
//  Other software layers should not concern itself with individual nodes or connections.  They ask the Request Manager for data, and the RM determines where and how to get it
@cli(Display.Simple, "Manage requests & responses from the blockchain network")
class RequestMgr(@cli(Display.Simple, "Access the blockchain network directly") val net: CnxnMgr, val genesisBlockHash: Hash256)
{
    val MAX_RECENT_BLOCK_CACHE = 25
    val MAX_RECENT_MERKLE_BLOCK_CACHE = 1000  // bigger because merkle blocks are a lot smaller
    val MAX_RECENT_TX_CACHE = 1000
    val MAX_RECENT_HEADER_CACHE = 400
    val blkRequestsLock: Mutex = Mutex()

    @cli(Display.Dev, "In-progress block header requests")
    var blkRequests: MutableMap<Hash256, MutableSet<Channel<iBlockHeader>>> = mutableMapOf()

    @cli(Display.Dev, "Recently received block cache")
    val recentBlocks: MutableMap<Hash256, iBlock> = mutableMapOf()
    val blockArrival = mutableListOf<Hash256>()

    @cli(Display.Dev, "Recently received merkle block cache")
    val recentMerkleBlocks: MutableMap<Hash256, iMerkleBlock> = mutableMapOf()
    val merkleBlockArrival = mutableListOf<Hash256>()
    val recentHeaderLock = ThreadCond()

    @cli(Display.Dev, "Recently received block header cache")
    val recentHeaders: MutableMap<Hash256, iBlockHeader> = mutableMapOf()
    val headerArrival = mutableListOf<Hash256>()

    var allowFilteredBlock = true  // Set to true to allow bloom filtered blocks (merkle blocks)

    protected val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    //<! woken when any header arrives
    val headerWaiter = CoCond<MutableList<out iBlockHeader>>(coScope)

    @cli(Display.Dev, "Recently received transaction cache")
    val recentTxs: MutableMap<Hash256, iTransaction> = mutableMapOf()
    val txArrival = mutableListOf<Hash256>()

    val partialBlocks = mutableMapOf<Hash256, iMerkleBlock>()

    val unconfTxCallbacks = mutableListOf<(List<iTransaction>) -> Unit>()

    val txBlkSync = ThreadCond()

    @cli(Display.Dev, "Clears the cache of all recent merkle blocks")
    fun clearMerkleBlockCache()
    {
        synchronized(recentHeaderLock)
        {
            partialBlocks.clear()
            recentMerkleBlocks.clear()
            merkleBlockArrival.clear()
        }
    }

    @cli(Display.Dev, "Add a callback whenever an unconfirmed tx arrived")
    fun addUnconfirmedTxHandler(cb: (List<iTransaction>) -> Unit)
    {
        unconfTxCallbacks.add(cb)
    }

    @cli(Display.Simple, "Get details about a transaction, given its hash")
    fun getTxDetails(txHash: Hash256) = getTxDetails(txHash.toHex())

    @cli(Display.Simple, "Get details about a transaction")
    fun getTxDetails(txHash: String): JsonElement
    {
        val ec = net.getElectrum()
        val ret = ec.getTxDetails(txHash, ELECTRUM_REQ_TIMEOUT)
        // TODO retry on timeout
        return ret
    }

    @cli(Display.Simple, "Get a transaction, given its hash")
    fun getTx(txHash: Hash256) = getTx(txHash.toHex())

    @cli(Display.Simple, "Get a transaction, given its hex-encoded hash")
    fun getTx(txHash: String): iTransaction
    {
        val ec = net.getElectrum()
        val ret = ec.getTx(txHash, ELECTRUM_REQ_TIMEOUT)
        // TODO retry on timeout
        return ret
    }


    @cli(Display.User, "Get list of block headers after a certain position")
    fun getBlockHeadersAfter(loc: BlockLocator, stopAt: Hash256 = Hash256()): MutableList<out iBlockHeader>
    {
        var tries = 0
        while (tries < 2)  // After a few tries, I need to give up and try some other headers -- for example, its possible that a longer chain appeared so nobody is serving this chain
        {
            tries += 1

            val waiter = ThreadCond()

            var rcvdHeaders: MutableList<out iBlockHeader>? = null
            var rcvdFrom: P2pClient? = null
            val cb: (hdrs: MutableList<out iBlockHeader>, P2pClient) -> Boolean = { hdrs, node ->
                rcvdHeaders = hdrs
                rcvdFrom = node
                waiter.wake()
                true
            }

            var count = 0
            val alreadyUsed: MutableSet<P2pClient> = mutableSetOf()
            try
            {
                while ((rcvdHeaders == null) && (count < 10))
                {
                    count += 1
                    if (count < 7)
                    {
                        val node = net.getAnotherNode(alreadyUsed)
                        if (node != null)
                        {
                            Logger.info(TAG,sourceLoc() + " " + net.chainName + " " + node.logName + ": Send getheaders for ${loc.have[0].toHex()}")
                            try
                            {
                                node.getHeaders(loc, stopAt, cb)
                            }
                            catch (e: SocketException)  // try some other node
                            {
                                Logger.info(TAG,sourceLoc() + " " + net.chainName + " socket exception: " + e.toString())
                                Thread.sleep(50)
                            }
                            catch (e: P2PDisconnectedException)
                            {
                                Logger.info(TAG,sourceLoc() + " " + net.chainName + " disconnected exception: " + e.toString())
                                node.close()
                                net.report(node)
                            }
                            alreadyUsed.add(node)
                        }

                    }
                    waiter.delay(1000) { rcvdHeaders == null }
                    // Logger.info(TAG,sourceLoc() + " " + net.chainName + " getheaders delay complete")
                }
            }
            finally
            {
                for (node in alreadyUsed) node.cleanupExclusiveHeaders(cb)
            }

            val headers = rcvdHeaders

            if (headers == null) // if we get nothing, after some time try other nodes
            {

                var names = ""
                for (node in alreadyUsed) names += node.logName + ", "
                Logger.warning(TAG,sourceLoc() + " " + net.chainName + " " + names + " didn't receive headers after ${loc.have[0].toHex()}")
                Thread.sleep(50)
            }
            else
            {
                // Not even our checkpoint is common with this node -- its on a fork so disconnect
                if ((headers.size > 0) && (loc.have[loc.have.size-1] == genesisBlockHash) && (headers[0].hashPrevBlock != genesisBlockHash))
                {
                    Logger.warning(TAG,sourceLoc() + " " + net.chainName + " " + rcvdFrom?.logName + " Returned ${headers.size} headers starting at the genesis block. It must be a separate fork")
                    Logger.warning(TAG,sourceLoc() + " " + net.chainName + " Request was " + loc.toString())
                    rcvdFrom?.close()
                    // We don't want to give up if we get bad headers from a node, we should try some other node
                    // throw P2PDisconnectedException(rcvdFrom?.logName + " Contains no common post-checkpoint blocks -- must be a separate fork")
                }
                else return headers
            }

        }
        throw HeadersNotForthcoming(Guid(loc.have[0].hash))
    }


    @cli(Display.User, "Get list of block headers after a certain block")
    fun getBlockHeaders(hash: Hash256): MutableList<out iBlockHeader>
    {
        val loc = BlockLocator()
        loc.add(hash)

        Logger.info(TAG,sourceLoc() + " " + net.chainName + ": Send getheaders for ${hash.toHex()}")
        var hdrs = getBlockHeadersAfter(loc, hash)
        return hdrs
    }

    @cli(Display.User, "Get specific block header")
    suspend fun getBlockHeaderByHash(hash: Hash256): iBlockHeader?
    {
        // Return this header if we've already gotten it
        while (true)
        {
            try
            {
                synchronized(recentHeaderLock)
                {
                    val cached = recentHeaders[hash]
                    if (cached != null) return cached
                }

                // If we don't have it, then request it
                val node = net.getp2p()
                val loc = BlockLocator()
                node.sendGetHeaders(loc, hash)
                headerWaiter.yield({ recentHeaders[hash] != null })
                return recentHeaders[hash]
            }
            catch (e: SocketException)  // Try some other node
            {
                delay(50)
            }
            catch (e: P2PDisconnectedException)
            {
                delay(50)
            }
        }
    }

    // TODO change cur into a list of blockheaders that summarize our chain
    // TODO: but this doesn't really work because nodes don't dynamically update their currentHeight
    fun getNextBlockHeaders(cur: Hash256): MutableList<out iBlockHeader>
    {
        return getBlockHeaders(cur)
    }

    /** Asynchronously pre-request a block whose transactions I'm sure I'll need */
    suspend fun coEarlyRequestTxInBlock(blocks: List<Guid>)
    {
        while (true)
        {

            val node = try
            {
                net.getp2p()
            }
            catch (e: P2PNoNodesException) // we can't prerequest the block if we don't have any connections so just return
            {
                return
            }

            try
            {
                synchronized(txBlkSync)
                {
                    //val needed = blocks.filter { recentBlocks[it.data] == null }.map { Inv(Inv.Types.BLOCK, it) }.toMutableList()
                    val needed = blocks.filter { recentBlocks[it.data] == null && recentMerkleBlocks[it.data] == null }.map { Inv(Inv.Types.FILTERED_BLOCK, it) }.toMutableList()
                    val blocksString = blocks.map { it.data.toHex() }.joinToString(" ")
                    Logger.info(TAG,sourceLoc() + " " + net.chainName + ": co-Requesting merkle blocks ${blocksString}")
                    if (needed.size > 0) node.sendGetData(needed)
                }
                return
            }
            catch (e: P2PDisconnectedException)
            {
                node.close()
                net.report(node)
                delay(50)
            }
            catch (e: SocketException)
            {
                delay(50)
            }
        }
    }

    /** Asynchronously pre-request a block whose transactions I'm sure I'll need */
    fun earlyRequestTxInBlock(blocks: List<Guid>)
    {
        while (true)
        {

            val node = try
            {
                net.getNode(false)
            }
            catch (e: P2PNoNodesException) // we can't prerequest the block if we don't have any connections so just return
            {
                return
            }

            try
            {
                synchronized(txBlkSync)
                {
                    //val needed = blocks.filter { recentBlocks[it.data] == null }.map { Inv(Inv.Types.BLOCK, it) }.toMutableList()
                    val needed = blocks.filter { recentBlocks[it.data] == null && recentMerkleBlocks[it.data] == null }.map {
                        if (allowFilteredBlock && node.bloomCount == net.bloomCount)
                            Inv(Inv.Types.FILTERED_BLOCK, it)
                        else
                            Inv(Inv.Types.BLOCK, it)
                    }.toMutableList()
                    //val blocksString = blocks.map { it.data.toHex() }.joinToString(" ")
                    //Logger.info(TAG,sourceLoc() + " " + net.chainName + ": Requesting ${needed.size} merkle blocks ${blocksString}")
                    if (needed.size > 0) node.sendGetData(needed)
                }
                return
            }
            catch (e: P2PDisconnectedException)
            {
                node.close()
                net.report(node)
                Thread.sleep(50)
            }
            catch (e: SocketException)
            {
                Thread.sleep(50)
            }
        }
    }


    suspend fun requestTxInBlock(blockHash: Guid): List<out iTransaction>
    {
        val waiter = Channel<iBlockHeader>()
        try
        {
            blkRequestsLock.withLock {
                blkRequests.getOrPut(blockHash.data, { mutableSetOf() }).add(waiter)
            }

            var blk: iBlockHeader? = null

            var count = 0
            val alreadyTriedNodes = mutableSetOf<P2pClient>()
            while (blk == null) // Keep asking from a node until we get a response
            {
                count += 1

                // first check to see if we have already received it
                blk = recentBlocks[blockHash.data]
                if (blk != null)
                {
                    //Logger.info(TAG,sourceLoc() + " " + net.chainName + ": returning cached block ${blk.hash.toHex()}:${blk.height}")
                    return blk.txes
                }
                blk = synchronized(txBlkSync) { recentMerkleBlocks[blockHash.data] }
                if (blk != null)
                {
                    //Logger.info(TAG,sourceLoc() + " " + net.chainName + ": returning cached merkle block ${blk.hash.toHex()}:${blk.height}")
                    return blk.txes
                }

                var node = net.getAnotherNode(alreadyTriedNodes)
                try
                {
                    if (node != null)
                    {
                        alreadyTriedNodes.add(node)
                        // If we've installed the latest bloom, ask for a filtered block
                        // otherwise drop back to a full block
                        if (allowFilteredBlock && node.bloomCount == net.bloomCount)
                            node.sendGetData(listOf(Inv(Inv.Types.FILTERED_BLOCK, blockHash)))
                        else
                            node.sendGetData(listOf(Inv(Inv.Types.BLOCK, blockHash)))
                        //Logger.info(TAG,sourceLoc() + ": Send filtered block request")
                    }  // If the node is null, we'll wait anyway, in the assumption that the block might still come in from a prior request
                    else
                    {
                        alreadyTriedNodes.clear() // Restart the try loop
                    }
                    withTimeout(BLOCK_REQ_TIMEOUT) {
                        blk = waiter.receive()
                    }
                }
                catch (e: DeserializationException)
                {
                    node?.misbehavingBadMessage(5)
                }
                catch (e: TimeoutCancellationException) // Node did not respond to us
                {
                    if (node != null)
                    {
                        // TODO: if too many nodes are silent, the problem may be that we are following a minority chain
                        node.misbehavingQuiet(1)
                        Logger.info(TAG,sourceLoc() + " " + node.logName + ": block request timeout for block " + blockHash.toHex())
                    }
                }
                catch (e: P2PDisconnectedException)
                {
                    if (node != null)
                    {
                        node.close()  // CnxnMgr detects closed connections and cleans them up
                        net.report(node)
                    }
                }
                catch (e: SocketException)  // Try some other node
                {
                    Logger.info(TAG,"socket exception")
                    delay(50)
                }

                val b = blk
                if (b == null)
                {
                    if (count >= 2)  // Nodes silently drop requests for blocks off the main chain, so after a bit we need to give up
                        throw BlockNotForthcoming(blockHash)
                    node?.let { Logger.info(TAG,it.logName + ": Requesting block " + blockHash.toHex() + " again") }
                }
                else
                {
                    if (b is iBlock)
                        return b.txes.toMutableList()
                    else if (b is iMerkleBlock)
                    {
                        // Logger.info(TAG,"merkle block ${b.hash}:${b.height} fully assembled")
                        return b.txes
                    }
                }
            }

            throw BlockNotForthcoming(blockHash) // can't ever get here anyway
        }
        finally
        {
            blkRequestsLock.withLock {
                blkRequests[blockHash.data]?.remove(waiter)  // clean up
            }

        }
    }

    /** Insert a partial block into the cache.  If a block is returned, its complete */
    fun insertPartialBlocks(blk: iMerkleBlock): iMerkleBlock?
    {
        val existingBlk = partialBlocks[blk.hash]
        if (existingBlk == null)
        {
            partialBlocks[blk.hash] = blk
        }
        else  // Merge these two merkle blocks
        {
            for (tx in blk.txes)
                existingBlk.txArrived(tx)

            if (existingBlk.complete)   // It was completed so remove and return it
            {
                partialBlocks.remove(blk.hash)
                return existingBlk
            }
        }
        return null
    }

    fun onPartialBlocks(blks: List<iMerkleBlock>)
    {
        val readyBlocks = mutableListOf<iBlockHeader>()
        synchronized(txBlkSync)
        {
            for (blk in blks)
            {
                val interestingTx = blk.txHashes.size + blk.txes.size
                Logger.info(TAG,sourceLoc() + " " + net.chainName + ": processing merkle block " + blk.hash.toHex() + " TX: " + interestingTx + " of " + blk.txCount)
                txloop@ for (tx in recentTxs)  // txes might come in before the merkle block it refers to
                {
                    if (blk.txArrived(tx.value))  // If we consumed the tx
                    {
                        if (blk.complete) break@txloop
                    }
                }
                if (blk.complete) readyBlocks.add(blk)
                else
                {
                    insertPartialBlocks(blk)?.let { readyBlocks.add(it) }
                }
            }
        }

        if (readyBlocks.size > 0) launch { onBlocks(readyBlocks) }
    }

    // If a block comes in, search through the list of requesters and provide it to them
    suspend fun onBlocks(blks: List<iBlockHeader>)
    {
        for (blk in blks)
        {
            synchronized(txBlkSync)
            {
                if (blk is iBlock)
                {
                    recentBlocks[blk.hash] = blk  // Put this block into the recently requested cache
                    blockArrival.add(blk.hash)
                }
                else if (blk is iMerkleBlock)
                {
                    val numTx = (blk as? NexaBlockHeader)?.txCount.toString() ?: "unknown"
                    // Logger.info(TAG,sourceLoc() + " " + net.chainName + ": merkle block ready " + blk.hash.toHex() + " TX: " + blk.txes.size + " of " + numTx)
                    recentMerkleBlocks[blk.hash] = blk  // Put this block into the recently requested cache
                    merkleBlockArrival.add(blk.hash)
                }
                null
            }

            while (blockArrival.size > MAX_RECENT_BLOCK_CACHE)  // Remove older recent blocks
            {
                synchronized(txBlkSync)
                {
                    recentBlocks.remove(blockArrival[0])
                    blockArrival.removeAt(0)
                }
            }

            while (merkleBlockArrival.size > MAX_RECENT_MERKLE_BLOCK_CACHE)  // Remove older recent merkle blocks
            {
                synchronized(txBlkSync)
                {
                    recentMerkleBlocks.remove(merkleBlockArrival[0])
                    merkleBlockArrival.removeAt(0)
                }
            }

            val waiters = blkRequestsLock.withLock {
                blkRequests[blk.hash]?.let { it.toList() }
            }

            if (waiters != null)
            {
                for (w in waiters)
                    launch { w.send(blk) }
            }

        }
    }

    /** Called when transactions arrive */
    fun onTx(txes: List<iTransaction>)
    {
        val readyBlocks = mutableListOf<iBlockHeader>()
        val unconfTxes = mutableListOf<iTransaction>()

        synchronized(txBlkSync)
        {
            for (tx in txes)
            {
                // Logger.info(TAG,"TX: " + tx.hash.toHex())
                recentTxs[tx.idem] = tx
                txArrival.add(tx.idem)

                var txConfirmed = false
                /* Provide this transaction to any of our in-progress merkle blocks and if this transaction completes the merkle block then process it */
                merkle@ for ((hash, blk) in partialBlocks)
                {
                    if (blk.txArrived(tx))
                    {
                        txConfirmed = true
                        if (blk.complete)
                        {
                            readyBlocks.add(blk)
                            partialBlocks.remove(hash)
                        }
                        break@merkle  // a particular tx will only be confirmed in one block
                    }
                }
                if (!txConfirmed) unconfTxes.add(tx)
            }

            // Clear out old values
            while (txArrival.size > MAX_RECENT_TX_CACHE)
            {
                recentTxs.remove(txArrival[0])
                txArrival.removeAt(0)
            }
        }

        if (unconfTxes.size > 0) onUnconfirmedTx(unconfTxes)
        if (readyBlocks.size > 0) launch { onBlocks(readyBlocks) }
    }

    fun onUnconfirmedTx(txes: List<iTransaction>)
    {
        for (cb in unconfTxCallbacks)
            cb(txes)
    }

    init
    {
        net.addPartialBlockHandler { blockLst -> onPartialBlocks(blockLst) }
        net.addBlockHeadersHandler { headerLst ->
            if (headerLst.size > 0)
            {
                synchronized(recentHeaderLock)
                {
                    for (hdr in headerLst)
                    {
                        recentHeaders[hdr.hash] = hdr
                        headerArrival.add(hdr.hash)
                        while (headerArrival.size > MAX_RECENT_HEADER_CACHE)  // Remove items from queue
                        {
                            recentHeaders.remove(headerArrival[0])
                            headerArrival.removeAt(0)
                        }
                    }
                }
            }
            headerWaiter.wake(headerLst)
        }

        net.addBlockHandler { blkLst -> onBlocks(blkLst) }

        net.addTxHandler { txLst -> onTx(txLst) }
    }


}


/// Checkpoint will always be at least the genesis block, except in regtest where we may not know even that, in which case its Hash256()
@cli(Display.Simple, "Access and track a blockchain database")
class Blockchain(
  @cli(Display.Simple, "Which blockchain is being tracked")
  val chainSelector: ChainSelector,
  @cli(Display.Simple, "Blockchain name")
  val name: String,
  @cli(Display.User, "Direct access to specific blockchain data providers")
  val net: CnxnMgr,
  val genesisBlockHash: Hash256,
  var checkpointPriorBlockId: Hash256,
  val checkpointId: Hash256,
  val checkpointHeight: Long,
  val checkpointWork: BigInteger,
  val context: PlatformContext,
  val dbPrefix: String)
{
    /** Change this to increase privacy at the expense of bandwidth */
    var bloomFalsePositiveRate: Double = 0.00000001

    /** Increase this to increase the interval of time before a bloom filter needs to be refreshed.
     * Bloom filters are modified by full nodes to automatically include information "of interest" based on current "of interest" information.
     * For example if a tx is interesting, then its outputs will be added into the bloom filter.  But the existence of false positives (and because
     * the wallet is likely interested in only a subset of the outputs) means that the bloom filter will slowly fill up with useless information.
     * Setting this multiplier the N will make a bloom filter that is N times your current data size meet the bloomFalsePositiveRate. */
    var bloomCapacityMultiplier: Int = 10

    var processingThread: Thread? = null

    var done = false

    @cli(Display.User, "Access stored block headers")
    lateinit var db: BlockHeaderPersist

    @cli(Display.User, "Access blockchain data providers")
    val req = RequestMgr(net, genesisBlockHash)

    @cli(Display.Simple, "Current blockchain height")
    public val curHeight: Long
        get() = nearTip?.height ?: checkpointHeight

    var unAttachedHdrs: MutableMap<Hash256, MutableList<out iBlockHeader>> = mutableMapOf()

    /// a cached value that is the tipFrac or nearly it for use in non-suspendable functions
    @cli(Display.Simple, "tip of this blockchain (named nearTip because at any moment a new tip can be discovered)")
    var nearTip: iBlockHeader? = null

    /// This chooses which tipFrac to use in a tie -- the chosen tipFrac is changed randomly (if a tie) so all paths are explored
    var tipTieBreaker: Int = 1

    /** wake up the blockchain header processing loop */
    val wakey = ThreadCond()

    @cli(Display.Dev, "How many wallets are using this as a data source")
    var attachedWallets: Int = 0

    var MED_DELAY_INTERVAL: Int = 10000
    var LONG_DELAY_INTERVAL: Int = 30000

    // During tests, we want delays to be much lower because we expect lots of blocks
    fun setTestDelayIntervals()
    {
        MED_DELAY_INTERVAL = 500
        LONG_DELAY_INTERVAL = 1000
    }

    override fun toString():String
    {
        val s = StringBuilder()
        s.append("type=Blockchain name=")
        s.append(name)
        s.append(" curHeight=")
        s.append(curHeight)
        s.append(" attachedWallets=")
        s.append(attachedWallets)
        return s.toString()
    }

    init
    {
        var tried = 0
        while(tried < 2)
        {
            tried++
            Logger.info(TAG,sourceLoc() + " " + name + ": Open Blockchain DB")
            var nexadb: NexaBlockHeaderDatabase? = null
            var bchdb: BchBlockHeaderDatabase? = null
            db = if (chainSelector.isNexaFamily)
            {
                nexadb = NexaOpenBlockHeaderDB(context, dbPrefix + name)
                nexadb.blockHeaderDao()
            }
            else if (chainSelector.isBchFamily)
            {
                bchdb = BchOpenBlockHeaderDB(context, dbPrefix + name)
                bchdb.blockHeaderDao()
            }
            else
            {
                throw UnimplementedException("Only NEXA or BCH blockchains supported")
            }

            try
            {
                nearTip = db.getCachedTipHeader()
                if (nearTip == null) nearTip = recalcTip()
                break  // All good!
            }
            catch (e: java.lang.IllegalStateException)  // java.lang.IllegalStateException: Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.
            {
                Logger.info(TAG,sourceLoc() + " " + name + ": Deleting, format changed")
                // close whichever one was opened
                nexadb?.close()
                bchdb?.close()
                // I have to actually delete the entire database file to destroy the schema.
                context.deleteDatabase(dbPrefix + name)
            }
        }
        Logger.info(TAG,sourceLoc() + " " + name + ": Open Blockchain DB completed")
    }

    val uriScheme: String
        get()
        {
            return chainToURI[chainSelector]!!
        }

    var onChange: ((Blockchain) -> Unit)? = null


    /** Add a wallet to start accessing the blockchain
     * The blockchain will stop updating if no wallets are attached
     */
    @Synchronized
    fun attachWallet()
    {
        attachedWallets += 1
    }

    /** remove a wallet from accessing this blockchain
     * Right now its 1 wallet per chain, so stop this chain, but keep the DB around for a new wallet
     */
    @Synchronized
    fun detachWallet(filterHandle: Int?)
    {
        // Clear out the bloom filter
        if (filterHandle != null) setFilterObjects(arrayOf(), filterHandle, null)
        attachedWallets -= 1
        if (attachedWallets <= 0)
        {
            stop()
            unAttachedHdrs.clear()
        }
    }

    var filterData: MutableMap<Int, Array<Any>> = mutableMapOf()
    var lastFilterHandle = 0

    /** tell all nodes, including future connections, that we are only interested in these things.
     * a handle to this data is returned that can be used to update or delete the data.  Pass null to create a new entry
     * This blockchain will take data from all callers, and combine them into a single Bloom filter for installation
     * into connected full nodes. */
    fun setFilterObjects(objects: Array<Any>, handle: Int?, onBloomInstalled: (() -> Unit)?): Int
    {
        // I don't want to be trying to get a merkle block while simultaneously updating the merkle filter
        // This won't work perfectly because getting a merkle block is a long protocol.  However, previous requested blocks ought to have
        // not have new addresses anyway, so this sync is more for extra care than required.
        synchronized(req.recentHeaderLock)
        {
            val hdl = handle ?: { lastFilterHandle += 1; lastFilterHandle }()

            if (objects.size == 0) filterData.remove(hdl)
            else filterData[hdl] = objects

            updateBloomFilter(onBloomInstalled)
            return hdl
        }
    }

    fun updateBloomFilter(onBloomInstalled: (() -> Unit)?)
    {
        // Get all filter data installed by any wallet that's using this blockchain, and flatten it into a big array of bytearrays
        val vals: Array<Array<Any>> = filterData.values.toTypedArray()
        val allData: Array<Any> = vals.flatten().toTypedArray()

        // BLOOM_UPDATE_ALL will automatically add new outpoints into the bloom filter, for txes that match.
        // But since there will be some false positives, this means the filter performance will slowly degrade, so we need to periodically "refresh" it with only the info we are interested in.
        val bloom = Wallet.CreateBloomFilter(allData, bloomFalsePositiveRate, allData.size * bloomCapacityMultiplier, Wallet.MAX_BLOOM_SIZE, Wallet.Companion.BloomFlags.BLOOM_UPDATE_ALL.v, 1)
        Logger.info(TAG,sourceLoc() + " " + name + ": num entries: ${allData.size} fpRate: ${bloomFalsePositiveRate} bloom size: ${bloom.size}")
        req.clearMerkleBlockCache()
        net.setAddressFilter(bloom, onBloomInstalled)
    }

    @cli(Display.User, "return the main chain block header at this height")
    fun getBlockHeader(height: Long): iBlockHeader
    {
        val blocksAtHeight = db.getHeadersAtHeight(height)
        if (blocksAtHeight.size == 1) return blocksAtHeight[0]
        // Otherwise I need to find the one on the main chain
        for (i in blocksAtHeight)
        {
            if (isInMostWorkChain(height, i.hash)) return i
        }
        throw HeadersNotForthcoming(Guid()) //"main chain block header at ${height} is not in the database")
    }

    /*
    @cli(Display.User, "return the main chain block header at this height")
    fun getBefore(date: Long):BlockHeader
    {
        val block = db!!.blockHeaderDao().getBefore(date)
        if (block.size >= 1) return block[0]
        throw HeadersNotForthcoming(Guid()) //"main chain block header at ${height} is not in the database")
    }
    */

    /** Erase all previous blockchain knowledge and re-download it */
    @cli(Display.User, "Erase all previous blockchain knowledge and re-download")
    fun rediscover()
    {
        nearTip = null
        db.clear()
        net.clear()  // I clear incoming messages so that transaction messages that relate to a prior sync don't get applied
        wakey.wake()
    }

    //? Return the end of the most work chain
    fun coRecalcTip(): iBlockHeader?
    {
        val blks = db.getMostWorkHeaders()
        if (blks.size == 0) return null
        val oldnearTip = nearTip

        val b = blks[tipTieBreaker % blks.size]
        if (true) // b != null)
        {
            nearTip = b
            db.setCachedTipHeader(b)
            Logger.info(TAG,sourceLoc() + " " + name + ": Set nearTip to: " + b.hash.toHex() + ":" + b.height)
        }

        if (oldnearTip != nearTip) onChange?.invoke(this)
        return nearTip
    }

    //? Return the end of the most work chain
    @cli(Display.Dev, "Rediscover the chain tip by looking at the block header database")
    fun recalcTip(): iBlockHeader?
    {
        val blks = db.getMostWorkHeaders()
        if (blks.size == 0) return null
        val oldnearTip = nearTip

        val b = blks[tipTieBreaker % blks.size]
        if (true) //(b != null)
        {
            nearTip = b
            db.setCachedTipHeader(b)
            Logger.info(TAG,sourceLoc() + " " + name + ": Set nearTip to: " + b.hash.toHex() + ":" + b.height)
        }

        if (oldnearTip != nearTip) onChange?.invoke(this)
        return nearTip
    }

    //? In rare cases of a tie where one block is invalid, this will kick the wallet over to a different block
    @cli(Display.Dev, "In rare cases of a tie where one block is invalid, this will kick the wallet over to a different block")
    fun findAnotherTip()
    {
        tipTieBreaker += 1
        recalcTip()
    }

    //? Return the header corresponding to this hash
    @cli(Display.Simple, "Return the block header corresponding to this hash")
    fun blockHeader(hash: Hash256): iBlockHeader?
    {
        val dbao = db!!
        val blk = dbao.getHeader(hash.hash)
        return blk
    }

    @cli(Display.Simple, "Restart block processing if stopped")
    fun restart()
    {
        if ((processingThread == null) || (processingThread?.state == Thread.State.TERMINATED))
        {
            processingThread = thread(true, true, null, name + "_chain") { run() }
        }
    }

    fun start()
    {
        uriScheme // do nothing with this valuse but assert right away if the chainToURI dictionary isn't properly filled out
        req.net.addInvHandler { invLst -> this.invHandler(invLst, null) }
        restart()
    }

    @cli(Display.Simple, "Stop processing incoming blocks")
    fun stop()
    {
        done = true
    }

    /** Returns the header nearest to but before the passed epoch time.
     * If the passed epoch time is later than the tip, the tip is returned.
     */
    fun findClosestBefore(epochTime: Long): iBlockHeader?
    {
        var blk = nearTip ?: return null  // I don't know anything about this blockchain
        if (blk.time < epochTime) return blk
        while(blk.time >= epochTime)
        {
            if (blk.height == 0L) return blk
            if (blk.hashAncestor != Hash256())
            {
                val tmp = db.getHeader(blk.hashAncestor.hash)
                if ((tmp != null) && (tmp.time >= epochTime))
                {
                    blk = tmp
                    continue
                }
            }

            val tmp = db.getHeader(blk.hashPrevBlock.hash)
            if (tmp == null) return null // TODO: need to load precheckpoint headers

            // This one is before the the prior was after so its this one
            if (tmp.time < epochTime) return tmp
            blk = tmp
        }
        return blk
    }

    //? return true if the passed block is on the most work chain
    @cli(Display.Simple, "Return true if the passed block is on the most work chain")
    fun isInMostWorkChain(height: Long, hash: Hash256): Boolean
    {
        val tip: iBlockHeader = nearTip ?: return false
        var blk: iBlockHeader? = tip
        var lastBlk = blk

        if (height < tip.height - 2000) return true  // Assume no huge reorgs for performance reasons

        while (true)
        {
            val b: iBlockHeader = blk ?: throw RequestedPrehistoryHeader(hash, lastBlk)  // If we rewind all the way so we run out of headers, we can't switch.
            lastBlk = b
            if (height == b.height)
            {
                return hash == b.hash
            }
            // Skip back a bunch of blocks if we can
            if (blk.hashAncestor != Hash256())
            {
                val tmp = db.getHeader(blk.hashAncestor.hash)
                if ((tmp != null) && (tmp.height >= height))
                {
                    blk = tmp
                    continue
                }
            }
            // If we can't then skip back by 1
            blk = db.getHeader(b.hashPrevBlock.hash)
        }
    }

    //? return true if the passed block is on the most work chain
    @cli(Display.Simple, "Return the ancestor of the passed block at the passed height, or the iBlockHeader of the passed hash is the height is larger.  Throws RequestedPrehistoryHeader if you go back too far")
    fun ancestorAtHeight(hash: Hash256, height: Long): iBlockHeader
    {
        // Logger.info(TAG,"ancestorAtHeight: " + height + "  request:" + hash.toHex())

        // this if both optimizes this common lookup and decouples the setting of nearTip with database storage
        var blk: iBlockHeader? = if (hash == nearTip?.hash) nearTip else db.getHeader(hash.hash)
        if (blk != null)
        {
            if (height > blk.height) return blk  // Not an ancestor, so return this block
        }
        val startHeight = blk?.height
        var lastBlk = blk

        var steps = 0
        while (true)
        {
            steps += 1
            val b: iBlockHeader = blk ?: throw RequestedPrehistoryHeader(hash, lastBlk)  // If we rewind all the way so we run out of headers, we can't switch.
            lastBlk = blk
            if (height == b.height)
            {
                Logger.info(TAG,sourceLoc() + name + ": ancestorAtHeight from $startHeight to $height took $steps accesses")
                return blk
            }

            // Skip back a bunch of blocks if we can
            if (blk.hashAncestor != Hash256())
            {
                val tmp = db.getHeader(blk.hashAncestor.hash)
                if ((tmp != null) && (tmp.height >= height) && (tmp.height < blk.height))
                {
                    // Logger.info(TAG,"get height ${tmp.height}")
                    blk = tmp
                    continue
                }
            }
            // If we can't then skip back by 1
            // Logger.info(TAG,"get height ${b.height-1}")
            blk = db.getHeader(b.hashPrevBlock.hash)
        }
    }


    fun coGetHeaderChain(height: Long, endAt: Hash256): MutableList<iBlockHeader>
    {
        var result = mutableListOf<iBlockHeader>()
        var cur = endAt
        while (true)
        {
            val hdr = db.getHeader(cur.hash)
            if (hdr == null) break
            result.add(hdr)

            cur = hdr.hashPrevBlock
            if (hdr.height <= height) break
        }
        result.reverse()
        return result
    }

    @cli(Display.Dev, "Return a list of headers starting at some height and ending at a particular block hash")
    fun getHeaderChain(height: Long, endAt: Hash256, maxCount: Int = Int.MAX_VALUE): MutableList<iBlockHeader>
    {
        var result = mutableListOf<iBlockHeader>()

        val lastH = ancestorAtHeight(endAt, height + maxCount)
        var cur = lastH.hash

        while (true)
        {
            val nt = nearTip
            val hdr = if (cur.hash == nt?.hash?.hash) nt else db.getHeader(cur.hash)
            if (hdr == null) break
            result.add(hdr)

            cur = hdr.hashPrevBlock
            // Logger.info(TAG,"getHeaderChain height ${hdr.height} ${hdr.hash.toHex()} prev: ${cur.toHex()}")
            if (hdr.height <= height) break
        }
        result.reverse()
        return result
    }

    @Suppress("UNUSED_PARAMETER")
    suspend fun invHandler(invLst: MutableList<Inv>, source: P2pClient?)
    {
        for (inv in invLst)
        {
            if (inv.type == Inv.Types.BLOCK)
            {
                // TODO: skip if already being handled by some other INV
                Logger.info(TAG,sourceLoc() + " " + name + ": processing INV for block " + inv.id.toHex())
                    val header = if (nearTip?.hash == inv.id.data) nearTip else db.getHeader(inv.id.data.hash)
                    if (header != null)
                    {
                        Logger.info(TAG,sourceLoc() + " " + name + ": already have header for block " + inv.id.toHex())
                        continue
                    }  // I already have this header so skip

                    // We want to get the header here rather than just skipping to the full block because the block
                    // could be on some other chain
                    val result = req.getBlockHeaderByHash(inv.id.data)
                    if (result != null)
                        processBlockHeaders(mutableListOf(result), db)
                    else
                    {
                        Logger.warning(TAG,sourceLoc() + " " + name + ": unable to get header for block " + inv.id.toHex())
                    }
            }
        }

    }

    suspend fun coEarlyRequestTxInBlock(blocks: List<Guid>)
    {
        req.coEarlyRequestTxInBlock(blocks)
    }

    fun earlyRequestTxInBlock(blocks: List<Guid>)
    {
        req.earlyRequestTxInBlock(blocks)
    }


    // Gets all transactions in the provided block, that match the installed bloom filter
    suspend fun getTxInBlock(blockHash: Guid): List<out iTransaction>
    {
        return req.requestTxInBlock(blockHash)
    }

    /** Attach these headers to existing chains in our DB and commit them.  The passed hdrs list are expected to be in parent->child order
     * @Return false if given nothing useful: every header provided was already known to us, or no headers were provided
     */
    fun processBlockHeaders(hdrs: MutableList<out iBlockHeader>, dbdao: BlockHeaderPersist): Boolean
    {
        if (hdrs.count() == 0) return false
        var lastHdr: iBlockHeader? = dbdao.getHeader(hdrs[hdrs.size - 1].hash.hash)
        if (lastHdr != null)
        {
            Logger.info(TAG,sourceLoc() + " " + name + ": Received already stored headers up to " + lastHdr.height + " hash: " + lastHdr.hash.toHex())
            return false
        }
        val beforeFirstHash = hdrs[0].hashPrevBlock
        var prevHdr: iBlockHeader? = if (nearTip?.hash == beforeFirstHash) nearTip else dbdao.getHeader(beforeFirstHash.hash)  // likely its the previous tip...
        if ((prevHdr == null) || (prevHdr.height == -1L)) // I can't hook this up, so cache it and ask for its parent
        {
            // TODO validate POW before caching it to stop data consumption attacks
            unAttachedHdrs[beforeFirstHash] = hdrs
            // a long chain of missing previous headers could create a huge nested recursion of coroutines
            // so we will just place the results of this request into an array for processing by run.
            // that also will make it work more cleanly if the header is simultaneously received via some other coroutine
            launch {
                req.getBlockHeaderByHash(beforeFirstHash)
            }
            return true
        }

        val oldtip = nearTip
        val ancestorHashes = mutableMapOf<Long, Hash256>()
        for (hdr in hdrs)
        {
            hdr.calcHash()
            if (chainSelector.hasBitcoinLikeHeader)
            {
                hdr.chainWork = prevHdr!!.chainWork + hdr.work
                hdr.txCount = -1  // I don't know
                hdr.size = -1  // I don't know
                if (prevHdr.height == -1L) return true
                hdr.height = prevHdr.height + 1

                // BCH only: find appropriate ancestor by moving back in variable sized jumps
                if (hdr.hashPrevBlock != prevHdr?.hash)
                {
                    Logger.warning(TAG,"Supplied header chain does not link properly at height: " + hdr.height.toString())
                    hdr.height = -1L
                    return true  // It was useful even if broken
                }

                ancestorHashes[hdr.height] = hdr.hash

                var ancestorHeight = hdr.height and (hdr.height - 1)
                // If the ancestor is the same as the prev, then jump back a human-time useful linear amount
                if (ancestorHeight == hdr.height - 1) ancestorHeight = hdr.height - 2*WeekOfBlocks(chainSelector)
                hdr.hashAncestor = if (ancestorHeight < checkpointHeight)
                {
                    beforeFirstHash
                }
                else
                {
                    try
                    {
                        val r = ancestorHashes.get(ancestorHeight)
                        if (r != null) r
                        else
                        {
                            val t = ancestorAtHeight(hdr.hashPrevBlock, ancestorHeight).hash
                            ancestorHashes[ancestorHeight] = t
                            t
                        }
                    }
                    catch (e: java.lang.Exception)  // Goes back before the checkpoint block, so just jump back as far as we can efficiently do so
                    {
                        beforeFirstHash
                    }
                }
                //Logger.info(TAG,"Height ${hdr.height} using ancestor at ${ancestorHeight} with hash ${hdr.hashAncestor.toHex()}")
            }

            //Logger.info(TAG,sourceLoc() + " " +name + " header: " + hdr.height + " hash: " + hdr.hash.toHex() + " work: " + hdr.cumulativeWork + " diffBits: " + hdr.diffBits)
            assert(hdr.hashAncestor != Hash256())
            dbdao.diffUpsert(hdr)
            val nt = nearTip
            if (nt == null)
            {
                nearTip = hdr
                onChange?.invoke(this)
            }
            else
            {
                // If we got the next block, then transition to it
                //if (hdr.hashPrevBlock == nt.hash)
                //   nearTip = hdr

                // Whatever header is being served to us has a greater cumulative work than our tipFrac so its the new tipFrac
                // the blockchain does not need to rewind the chain to undo and redo state, since only the wallet keeps state
                if (hdr.chainWork > nt.chainWork)
                {
                    nearTip = hdr
                    onChange?.invoke(this)
                }
            }

            prevHdr = hdr

            // If we previously got some child headers, then queue those up for processing, and remove them from the cache
            var unAttachedChildren = unAttachedHdrs[hdr.hash]
            if (unAttachedChildren != null)
            {
                unAttachedHdrs.remove(hdr.hash)
                launch { processBlockHeaders(unAttachedChildren, dbdao) }
            }
        }
        nearTip?.let { if (it != oldtip) dbdao.setCachedTipHeader(it) }
        Logger.info(TAG,sourceLoc() + name +": Processed ${hdrs.size} headers ${hdrs[0].height} to ${hdrs[hdrs.size-1].height}, or ${hdrs[0].hash.toHex()} to ${hdrs[hdrs.size-1].hash.toHex()}")

        wakey.wake()
        return true
    }

    //? Restart this blockchain for testing initial sync
    // Only call just after construction and before you've called start()/run() or attached any wallets
    fun deleteAllPersistentData()
    {
        db.clear()
        nearTip = null
    }

    /** Download and save some headers that aren't at the end of the blockchain */
    fun reacquireHeaders(height: Long, endAt: Hash256)
    {
        throw UnimplementedException("reacquireHeaders")
        /*
        val hdrs = req.getBlockHeaders(endAt, endAt)

        if (hdrs.size > 0)  // First header we receive is the one we know about
        {
            if (lastKnownHeaderHash != genesisBlockHash)
            // Get rid of the one we already have if it was sent
                if (hdrs[0].hashPrevBlock == lastKnownHeader.hashPrevBlock) hdrs.removeAt(0)

            // Store any new headers into the blockchain DB
            processBlockHeaders(hdrs, dbao)
        }
        */
    }

    /** Takes a chain tip and produces a set of blocks that on that chain that allow someone to find
     * the approximate fork location if they are on a different tip
     */
    @cli(Display.Dev, "Takes a tip block hash and produces a set of block hashes that on that chain that allow someone to find an approximate fork location if they are on a different tip")
    fun chainSummary(tip: Hash256): BlockLocator
    {
        val bl = BlockLocator()
        bl.add(tip)
        var mask = 0xffffffffffff
        val tiphdr = if (nearTip?.hash == tip) nearTip else db.getHeader(tip.hash)
        if (tiphdr == null) return bl
        var hdr: iBlockHeader = tiphdr
        val tipheight = hdr.height
        var newheight: Long
        var pow2 = 1
        while (pow2 < tipheight)
        {
            mask = mask shl 1
            pow2 = pow2 shl 1
            newheight = tipheight - pow2 and mask

            if (newheight < checkpointHeight) break  // Exit if we got to our known good blockchain
            val headers = db.getHeadersAtHeight(newheight)
            if (headers.size == 1)  // I only know about 1 blockchain at this height
            {
                bl.add(headers[0].hash)
                hdr = headers[0]
                // null headers can't be stored in the DB: if (hdr == null) return bl
            }
            else  // I have to figure out which block is this one's parent
            {
                // trace backwards from our tip, until we get to the expected height
                while (hdr.height != newheight)
                {
                    hdr = db.getHeader(hdr.hashPrevBlock.hash) ?: return bl
                }
                // then add that block's hash to the locator
                bl.add(hdr.hash)
            }

        }

        // Always add our known good blockchain point to the locator to ensure that the other node
        // isn't on a completely different persistent fork (aka forked coin)
        bl.add(checkpointId)
        return bl
    }

    //? Endless suspending function that keeps this blockchain in sync with the live network
    // call start()
    fun run()
    {
        while (!done)
        {
            try
            {
                var doADelay = MED_DELAY_INTERVAL / 2
                try
                {
                    var lastKnownHeaderHash: Hash256

                    // TODO: monitor unAttachedHdrs size and delete some items if it gets too big (stop mem exhaustion attack)

                    //Logger.info(TAG,name + ": getMostWork")
                    val lastKnownHeaders = if (nearTip == null) listOf() else listOf(nearTip)   // TODO check that switching to a more difficult tip works
                    //Logger.info(TAG,name + ": getMostWork complete")

                    if (lastKnownHeaders.size == 0)  // DB was blown away, we need to get the genesis or checkpointed block back
                    {
                        var loadhash = checkpointPriorBlockId
                        if (loadhash == Hash256())
                        {
                            val gb = runBlocking { req.getBlockHeaderByHash(genesisBlockHash) }
                            assert(gb != null)
                            gb?.let {
                                if (chainSelector.hasBitcoinLikeHeader)
                                {
                                    it.chainWork = it.work  // for the first block, the chainWork is the work
                                    it.height = 0
                                }
                                db.diffUpsert(it) // add the checkpointed header into our DB
                                loadhash = it.hash
                            }
                        }
                        val hdrs = req.getBlockHeaders(loadhash)
                        if (hdrs.size == 0)
                        {
                            Logger.warning(TAG,"Got no headers")
                        }
                        else
                        {
                            // Sanity check the header we got before writing it as the starting block
                            if ((hdrs[0].hash == checkpointId) && (hdrs[0].hashPrevBlock == checkpointPriorBlockId))
                            {
                                hdrs[0].let {
                                    if (chainSelector.hasBitcoinLikeHeader)
                                    {
                                        it.chainWork = checkpointWork  // Fill in information that the node did not give us from the checkpoint configuration
                                        it.height = checkpointHeight
                                    }
                                    if (checkpointHeight != 0L && it.hash != checkpointId)  // checkpoint IS the genesis block so we should expect it to be returned
                                    {
                                        Logger.warning(TAG,sourceLoc() + " " + name + ": Node has a different history than our checkpoint.  Might be forked node or wallet misconfiguration.")
                                        return
                                    }  // node giving us bad data
                                    // TODO validate POW
                                    db.diffUpsert(it)  // add the checkpointed block into our DB
                                    hdrs.removeAt(0) // delete from out headers list so we don't reprocess it when processing the header "normally"
                                }
                            }
                            else
                            {
                                if (hdrs[0].hashPrevBlock == genesisBlockHash)  // Special case genesis block is checkpoint because GETHEADERS doesn't give us the GB
                                {
                                    hdrs[0].let {
                                        if (chainSelector.hasBitcoinLikeHeader)
                                        {
                                            it.chainWork = checkpointWork  // Fill in information that the node did not give us from the checkpoint configuration
                                            it.height = 1
                                        }
                                        db.diffUpsert(it)  // add the block into our DB
                                        hdrs.removeAt(0)
                                    }
                                }
                                else
                                {
                                    Logger.warning(TAG,sourceLoc() + " " + name + ": Node has a different history than our checkpoint.  Might be forked node or wallet misconfiguration.")
                                    return
                                }
                            }
                            processBlockHeaders(hdrs, db)
                            doADelay = 0
                            onChange?.invoke(this)
                        }
                    }

                    Logger.info(TAG,sourceLoc() + " " + name + ": Pursuing ${lastKnownHeaders.size} chain tips")
                    for (lastKnownHeader in lastKnownHeaders)
                    {
                        if (lastKnownHeader == null) // A blockchain reload can remove the entire DB leaving us with no headers
                        {
                            lastKnownHeaderHash = genesisBlockHash
                        }
                        else
                        {
                            lastKnownHeaderHash = lastKnownHeader.hash
                        }
                        //Logger.info(TAG,sourceLoc() + " " + name + ": get next block headers")
                        // periodically poll to see if there are new blocks in the chain.  We'll also get INVs if we are on the tipFrac

                        val hdrs = req.getBlockHeadersAfter(chainSummary(lastKnownHeaderHash))
                        if (hdrs.size > 0)  // First header we receive is the one we know about
                        {
                            Logger.info(TAG,sourceLoc() + " " + name + ": next block headers complete with ${hdrs.size} headers, starting with ${hdrs[0].hash} and ending at ${hdrs[hdrs.size - 1].hash}")
                            doADelay = 0 // After processing these headers, look for more right away
                            if (lastKnownHeaderHash != genesisBlockHash)
                            // Get rid of the one we already have if it was sent
                                if ((lastKnownHeader != null) && (hdrs[0].hashPrevBlock == lastKnownHeader.hashPrevBlock)) hdrs.removeAt(0)
                            // Store any new headers into the blockchain DB
                            if (!processBlockHeaders(hdrs, db)) doADelay = LONG_DELAY_INTERVAL
                            onChange?.invoke(this)
                        }
                        else
                        {
                            Logger.info(TAG,sourceLoc() + " " + name + ": Empty getheaders implies we are at the tip -- long poll interval")
                            doADelay = LONG_DELAY_INTERVAL
                        }
                    }
                }
                catch (e: HeadersNotForthcoming)
                {
                    Logger.info(TAG,name + ": connectivity problems (no headers).  Nothing to do but keep trying")
                    doADelay = MED_DELAY_INTERVAL
                }
                catch (e: P2PNoNodesException)
                {
                    Logger.info(TAG,name + ": connectivity problems (no nodes).  Nothing to do but keep trying")
                    doADelay = MED_DELAY_INTERVAL
                }

                if (doADelay > 0)
                {
                    Logger.info(TAG,name + ": blockchain delay for ${doADelay} ms")
                    wakey.delay(doADelay.toLong())
                    Logger.info(TAG,name + ": blockchain delay complete")
                }
            }
            catch (e: Exception)
            {
                handleThreadException(e, sourceLoc() + " " + name + ": ")
            }
        }
        Logger.warning(TAG,name + ": Blockchain sync loop exited. Quitting")
    }


    companion object
    {
        @JvmStatic
        external fun getWorkFromDifficultyBits(nBits: Long): ByteArray

        fun workFromDifficultyBits(nBits: Long): BigInteger
        {
            val workBytes = getWorkFromDifficultyBits(nBits)
            return BigInteger(1, workBytes)
        }

    }
}
