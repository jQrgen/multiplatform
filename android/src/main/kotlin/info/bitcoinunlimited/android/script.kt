// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android

import info.bitcoinunlimited.multiplatform.BUException
import info.bitcoinunlimited.multiplatform.ErrorSeverity
import info.bitcoinunlimited.multiplatform.RnotSupported
import info.bitcoinunlimited.multiplatform.UnimplementedException

open class ScriptException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) :
  BUException(msg, shortMsg, severity)

open class UnsupportedInBlockchain(msg: String) : ScriptException(msg, appI18n(RnotSupported))

data class ScriptTemplate(val groupInfo:GroupInfo?, val templateHash:ByteArray?, val wellKnownId:Long?, val argsHash:ByteArray, val rest:List<ByteArray>)
{
    fun p2pkt(cs: ChainSelector): SatoshiScript
    {
        return SatoshiScript(cs, SatoshiScript.Type.TEMPLATE, OP.FALSE, P2PKT_ID, OP.push(argsHash))
    }
    fun p2t(cs: ChainSelector): SatoshiScript =
        when (wellKnownId)
        {
            null -> SatoshiScript(cs, SatoshiScript.Type.TEMPLATE, OP.FALSE, OP.push(templateHash!!), OP.push(argsHash))  // if wellKnown is null templatehash must be something
            1L -> p2pkt(cs)
            else -> throw UnsupportedInBlockchain("unknown well known template identifier")
        }
}

// Note enum does not work for these script opcodes because I want the opcodes to be the same type as ByteArray so that data and opcodes can be pushed in the same parameter, list, or varargs
// without qualifying the enum with .ordinal
class OP(val v: ByteArray)
{
    companion object
    {
        val FALSE = byteArrayOf(0x0.toByte())
        val TRUE = byteArrayOf(0x51.toByte())

        val NOP = byteArrayOf(0x61.toByte())    // control operations
        val VER = byteArrayOf(0x62.toByte())
        val IF = byteArrayOf(0x63.toByte())
        val NOTIF = byteArrayOf(0x64.toByte())
        val VERIF = byteArrayOf(0x65.toByte())
        val VERNOTIF = byteArrayOf(0x66.toByte())
        val ELSE = byteArrayOf(0x67.toByte())
        val ENDIF = byteArrayOf(0x68.toByte())
        val VERIFY = byteArrayOf(0x69.toByte())
        val RETURN = byteArrayOf(0x6a.toByte())

        val TOALTSTACK = byteArrayOf(0x6b.toByte())     // stack operations
        val FROMALTSTACK = byteArrayOf(0x6c.toByte())
        val DROP2 = byteArrayOf(0x6d.toByte())
        val DUP2 = byteArrayOf(0x6e.toByte())
        val DUP3 = byteArrayOf(0x6f.toByte())
        val OVER2 = byteArrayOf(0x70.toByte())
        val ROT2 = byteArrayOf(0x71.toByte())
        val SWAP2 = byteArrayOf(0x72.toByte())
        val IFDUP = byteArrayOf(0x73.toByte())
        val DEPTH = byteArrayOf(0x74.toByte())
        val DROP = byteArrayOf(0x75.toByte())
        val DUP = byteArrayOf(0x76.toByte())
        val NIP = byteArrayOf(0x77.toByte())
        val OVER = byteArrayOf(0x78.toByte())
        val PICK = byteArrayOf(0x79.toByte())
        val ROLL = byteArrayOf(0x7a.toByte())
        val ROT = byteArrayOf(0x7b.toByte())
        val SWAP = byteArrayOf(0x7c.toByte())
        val TUCK = byteArrayOf(0x7d.toByte())

        val CAT = byteArrayOf(0x7e.toByte())      // string operations
        val SPLIT = byteArrayOf(0x7f.toByte())
        val NUM2BIN = byteArrayOf(0x80.toByte())  // number conversion
        val BIN2NUM = byteArrayOf(0x81.toByte())
        val SIZE = byteArrayOf(0x82.toByte())

        val INVERT = byteArrayOf(0x83.toByte())
        val AND = byteArrayOf(0x84.toByte())
        val OR = byteArrayOf(0x85.toByte())
        val XOR = byteArrayOf(0x86.toByte())
        val EQUAL = byteArrayOf(0x87.toByte())
        val EQUALVERIFY = byteArrayOf(0x88.toByte())
        val RESERVED1 = byteArrayOf(0x89.toByte())
        val RESERVED2 = byteArrayOf(0x8a.toByte())

        val ADD1 = byteArrayOf(0x8b.toByte())
        val SUB1 = byteArrayOf(0x8c.toByte())
        val MUL2 = byteArrayOf(0x8d.toByte())
        val DIV2 = byteArrayOf(0x8e.toByte())
        val NEGATE = byteArrayOf(0x8f.toByte())
        val ABS = byteArrayOf(0x90.toByte())
        val NOT = byteArrayOf(0x91.toByte())
        val NOTEQUAL0 = byteArrayOf(0x92.toByte())

        val ADD = byteArrayOf(0x93.toByte())
        val SUB = byteArrayOf(0x94.toByte())
        val MUL = byteArrayOf(0x95.toByte())
        val DIV = byteArrayOf(0x96.toByte())
        val MOD = byteArrayOf(0x97.toByte())
        val LSHIFT = byteArrayOf(0x98.toByte())
        val RSHIFT = byteArrayOf(0x99.toByte())

        val BOOLAND = byteArrayOf(0x9a.toByte())
        val BOOLOR = byteArrayOf(0x9b.toByte())
        val NUMEQUAL = byteArrayOf(0x9c.toByte())
        val NUMEQUALVERIFY = byteArrayOf(0x9d.toByte())
        val NUMNOTEQUAL = byteArrayOf(0x9e.toByte())
        val LESSTHAN = byteArrayOf(0x9f.toByte())
        val GREATERTHAN = byteArrayOf(0xa0.toByte())
        val LESSTHANOREQUAL = byteArrayOf(0xa1.toByte())
        val GREATERTHANOREQUAL = byteArrayOf(0xa2.toByte())
        val MIN = byteArrayOf( 0xa3.toByte())
        val MAX = byteArrayOf( 0xa4.toByte())        

        val RIPEMD160 = byteArrayOf(0xa6.toByte())
        val SHA1 = byteArrayOf(0xa7.toByte())
        val SHA256 = byteArrayOf(0xa8.toByte())
        val HASH160 = byteArrayOf(0xa9.toByte())
        val HASH256 = byteArrayOf(0xaa.toByte())
        val CODESEPARATOR = byteArrayOf(0xab.toByte())
        val CHECKSIG = byteArrayOf(0xac.toByte())
        val CHECKSIGVERIFY = byteArrayOf(0xad.toByte())
        val CHECKMULTISIG = byteArrayOf(0xae.toByte())
        val CHECKMULTISIGVERIFY = byteArrayOf(0xaf.toByte())

        val CHECKLOCKTIMEVERIFY = byteArrayOf(0xb1.toByte())
        val CHECKSEQUENCEVERIFY = byteArrayOf(0xb2.toByte())

        val PUSHDATA1 = byteArrayOf(0x4c.toByte())
        val PUSHDATA2 = byteArrayOf(0x4d.toByte())
        val PUSHDATA4 = byteArrayOf(0x4e.toByte())

        val C0 = FALSE
        val CNEG1 = byteArrayOf(0x4f.toByte())
        val C1 = TRUE
        val C2 = byteArrayOf(0x52.toByte())
        val C3 = byteArrayOf(0x53.toByte())
        val C4 = byteArrayOf(0x54.toByte())
        val C5 = byteArrayOf(0x55.toByte())
        val C6 = byteArrayOf(0x56.toByte())
        val C7 = byteArrayOf(0x57.toByte())
        val C8 = byteArrayOf(0x58.toByte())
        val C9 = byteArrayOf(0x59.toByte())
        val C10 = byteArrayOf(0x5a.toByte())
        val C11 = byteArrayOf(0x5b.toByte())
        val C12 = byteArrayOf(0x5c.toByte())
        val C13 = byteArrayOf(0x5d.toByte())
        val C14 = byteArrayOf(0x5e.toByte())
        val C15 = byteArrayOf(0x5f.toByte())
        val C16 = byteArrayOf(0x60.toByte())

        // Nextchain
        val PLACE = byteArrayOf(0xe9.toByte())
        val PUSH_TX_STATE = byteArrayOf(0xea.toByte())
        val SETBMD = byteArrayOf(0xeb.toByte())
        val BIN2BIGNUM = byteArrayOf(0xec.toByte())
        val EXEC = byteArrayOf(0xed.toByte())

        // Not real opcodes -- used to match data types in script identification comparisons
        val TMPL_BIGINTEGER = byteArrayOf(0xf0.toByte())
        val TMPL_DATA = byteArrayOf(0xf1.toByte())
        val TMPL_SCRIPT = byteArrayOf(0xf2.toByte())  // replace with any constraint script
        val TMPL_SMALLINTEGER = byteArrayOf(0xfa.toByte())
        val TMPL_PUBKEYS = byteArrayOf(0xfb.toByte())
        val TMPL_PUBKEYHASH = byteArrayOf(0xfd.toByte())
        val TMPL_PUBKEY = byteArrayOf(0xfe.toByte())

        val LAST_DATAPUSH = 0x4b

        /* Return TRUE if the passed byte is a constant quantity pushdata opcode */
        fun isPushData0(v: Byte): Int
        {
            if ((v >= 1) && (v <= LAST_DATAPUSH)) return v.toPositiveInt()
            return 0
        }

        /** Convert from an opcode into a assembly instruction */
        data class ParsedInst(val inst:ByteArray, val data:ByteArray?, val number: Long?)
        fun parse(b: ByteArray): ParsedInst
        {
            val inst = b[0]
            val iba = byteArrayOf(inst)
            return when (inst)
            {
                in 1..75 -> ParsedInst(iba, scriptDataFrom(b), scriptNumFrom(b))
                FALSE[0] -> ParsedInst(iba, null, 0)
                TRUE[0] -> ParsedInst(iba, null, 1)
                CNEG1[0] -> ParsedInst(iba, null, -1)
                C2[0] -> ParsedInst(iba, null, 2)
                C3[0] -> ParsedInst(iba, null, 3)
                C4[0] -> ParsedInst(iba, null, 4)
                C5[0] -> ParsedInst(iba, null, 5)
                C6[0] -> ParsedInst(iba, null, 6)
                C7[0] -> ParsedInst(iba, null, 7)
                C8[0] -> ParsedInst(iba, null, 8)
                C9[0] -> ParsedInst(iba, null, 9)
                C10[0] -> ParsedInst(iba, null, 10)
                C11[0] -> ParsedInst(iba, null, 11)
                C12[0] -> ParsedInst(iba, null, 12)
                C13[0] -> ParsedInst(iba, null, 13)
                C14[0] -> ParsedInst(iba, null, 14)
                C15[0] -> ParsedInst(iba, null, 15)
                C16[0] -> ParsedInst(iba, null, 16)
                PUSHDATA1[0], PUSHDATA2[0], PUSHDATA4[0] -> ParsedInst(iba, scriptDataFrom(b), scriptNumFrom(b))
                else -> ParsedInst(iba, null, null)
            }
        }

                /** Convert from an opcode into a assembly instruction */
        fun toAsm(b: ByteArray): String
        {
            val inst = b[0]
            val ret = when (inst)
            {
                in 1..75 -> "0x" + b.drop(1).toHex()
                FALSE[0] -> "0"
                TRUE[0] -> "1"
                NOP[0] -> "NOP"
                VER[0] -> "VER(REMOVED)"
                IF[0] -> "IF"
                NOTIF[0] -> "NOTIF"
                VERIF[0] -> "VERIF(REMOVED)"
                VERNOTIF[0] -> "VERNOTIF(REMOVED)"
                ELSE[0] -> "ELSE"
                ENDIF[0] -> "ENDIF"
                VERIFY[0] -> "VERIFY"
                RETURN[0] -> "UNSPENDABLE"
                
                TOALTSTACK[0] -> "TOALTSTACK"
                FROMALTSTACK[0] -> "FROMALTSTACK"
                DROP2[0] -> "DROP2"
                DUP2[0] -> "DUP2"
                DUP3[0] -> "DUP3"
                OVER2[0] -> "OVER2"
                ROT2[0] -> "ROT2"
                SWAP2[0] -> "SWAP2"
                IFDUP[0] -> "IFDUP"
                DEPTH[0] -> "DEPTH"
                DROP[0] -> "DROP"
                DUP[0] -> "DUP"
                NIP[0] -> "NIP"
                OVER[0] -> "OVER"
                PICK[0] -> "PICK"
                ROLL[0] -> "ROLL"
                ROT[0] -> "ROT"
                SWAP[0] -> "SWAP"
                TUCK[0] -> "TUCK"
                
                CAT[0] -> "CAT"
                SPLIT[0] -> "SPLIT"
                NUM2BIN[0] -> "NUM2BIN"
                BIN2NUM[0] -> "BIN2NUM"
                SIZE[0] -> "SIZE"
                
                INVERT[0] -> "INVERT(REMOVED)"
                AND[0] -> "AND"
                OR[0] -> "OR"
                XOR[0] -> "XOR"
                EQUAL[0] -> "EQUAL"
                EQUALVERIFY[0] -> "EQUALVERIFY"
                RESERVED1[0] -> "RESERVED1"
                RESERVED2[0] -> "RESERVED2"
                
                ADD1[0] -> "INC"
                SUB1[0] -> "DEC"
                MUL2[0] -> "MUL2(REMOVED)"
                DIV2[0] -> "DIV2(REMOVED)"
                NEGATE[0] -> "NEGATE"
                ABS[0] -> "ABS"
                NOT[0] -> "NOT"
                NOTEQUAL0[0] -> "NOTEQUAL0"
                
                ADD[0] -> "ADD"
                SUB[0] -> "SUB"
                MUL[0] -> "MUL"
                DIV[0] -> "DIV"
                MOD[0] -> "MOD"
                LSHIFT[0] -> "LSHIFT"
                RSHIFT[0] -> "RSHIFT"

                BOOLAND[0] -> "BOOLAND"
                BOOLOR[0] -> "BOOLOR"
                NUMEQUAL[0] -> "NUMEQUAL"
                NUMEQUALVERIFY[0] -> "NUMEQUALVERIFY"
                NUMNOTEQUAL[0] -> "NUMNOTEQUAL"
                LESSTHAN[0] -> "LESSTHAN"
                GREATERTHAN[0] -> "GREATERTHAN"
                LESSTHANOREQUAL[0] -> "LESSTHANOREQUAL"
                GREATERTHANOREQUAL[0] -> "GREATERTHANOREQUAL"
                MIN[0] -> "MIN"
                MAX[0] -> "MAX"

                RIPEMD160[0] -> "RIPEMD160"
                SHA1[0] -> "SHA1"
                SHA256[0] -> "SHA256"
                HASH160[0] -> "HASH160"
                HASH256[0] -> "HASH256"
                CODESEPARATOR[0] -> "CODESEPARATOR"
                CHECKSIG[0] -> "CHECKSIG"
                CHECKSIGVERIFY[0] -> "CHECKSIGVERIFY"
                CHECKMULTISIG[0] -> "CHECKMULTISIG"
                CHECKMULTISIGVERIFY[0] -> "CHECKMULTISIGVERIFY"

                CHECKLOCKTIMEVERIFY[0] -> "CHECKLOCKTIMEVERIFY"
                CHECKSEQUENCEVERIFY[0] -> "OP_CHECKSEQUENCEVERIFY"

                PUSHDATA1[0] -> "PUSH1 0x" + b.drop(2).toHex()
                PUSHDATA2[0] -> "PUSH2 0x" + b.drop(3).toHex()
                PUSHDATA4[0] -> "PUSH4 0x" + b.drop(5).toHex()

                CNEG1[0] -> "-1"

                C2[0] -> "2"
                C3[0] -> "3"
                C4[0] -> "4"
                C5[0] -> "5"
                C6[0] -> "6"
                C7[0] -> "7"
                C8[0] -> "8"
                C9[0] -> "9"
                C10[0] -> "10"
                C11[0] -> "11"
                C12[0] -> "12"
                C13[0] -> "13"
                C14[0] -> "14"
                C15[0] -> "15"
                C16[0] -> "16"

                // Nexa
                PLACE[0] -> "PLACE"
                PUSH_TX_STATE[0] -> "PUSHTXSTATE"
                SETBMD[0] -> "SETBMD"
                BIN2BIGNUM[0] -> "BIN2BIGNUM"
                EXEC[0] -> "EXEC"

                // Not real opcodes -- used to match data types in script identification comparisons
                TMPL_BIGINTEGER[0] -> "<biginteger>"
                TMPL_DATA[0] -> "<bytes>"
                TMPL_SCRIPT[0] -> "<script>"
                TMPL_SMALLINTEGER[0] -> "<integer>"
                TMPL_PUBKEYS[0] -> "<pubkeys>"
                TMPL_PUBKEYHASH[0] -> "<pubkeyhash>"
                TMPL_PUBKEYHASH[0] -> "<pubkeyhash>"
                TMPL_PUBKEY[0] -> "<pubkey>"
                else -> "undefined"
            }
            return ret
        }

        /**
         * If the opcode is a "direct numeric push" (opcodes OP1NEGATE -> OP16),
         * return the value OP code represents as ByteArray. Otherwise, null.
         */
        fun getDirectPush(opcode: Byte): ByteArray?
        {
            when (opcode)
            {
                C0[0] ->
                {
                    return ByteArray(0)
                }
                CNEG1[0],
                C1[0],
                C2[0],
                C3[0],
                C4[0],
                C5[0],
                C6[0],
                C7[0],
                C8[0],
                C9[0],
                C10[0],
                C11[0],
                C12[0],
                C13[0],
                C14[0],
                C15[0],
                C16[0] ->
                {
                    val value: Int = opcode.toPositiveInt() - (C1[0].toPositiveInt() - 1)
                    return byteArrayOf(value.toByte())
                }
                else ->
                {
                    return null
                }
            }
        }

        fun push(data: ByteArray): ByteArray
        {
            if (data.size <= LAST_DATAPUSH)
            {
                return byteArrayOf(data.size.toByte()) + data
            }
            if (data.size <= 0xff)
            {
                return PUSHDATA1 + data.size.toByte() + data
            }
            if (data.size <= 0xffff)
            {
                // Size as little endian 16 bits
                val size = ByteArray(2)
                size[0] = (data.size and 0xff).toByte()
                size[1] = (data.size shr 8 and 0xff).toByte()
                return PUSHDATA2 + size + data
            }
            else
                throw UnimplementedException("Pushing more data then stack allows")
            //    return PUSHDATA4 + data.size.toInt() + data
        }

        fun push(num: Int): ByteArray = push(num.toLong())
        fun push(num: Long): ByteArray // Pushes a scriptnum
        {
            if (num == 0L) return C0
            if (num <= 16L)
            {
                var ret = C1.clone()
                ret[0] = (ret[0] + (num - 1)).toByte()
                return ret
            }
            throw UnimplementedException("Pushing bigger number")
        }

        // Little endian serialization to 2, 4, or 8 bytes as defined in the Group Tokenization Consensus Specification
        fun groupAmount(value: Long): ByteArray
        {
            if (value >= 0 && value < 0xffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte()))
            if (value >= 0 && value < 0xffffffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte()))
            else return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte(),
              (value shr 32).toByte(), (value shr 40).toByte(), (value shr 48).toByte(), (value shr 56).toByte()))
        }

    }
}


fun scriptNumFrom(scriptBytes: ByteArray): Long?
{
    if (scriptBytes[0] == OP.C0[0]) return 0
    if ((scriptBytes[0] >= OP.C1[0]) && (scriptBytes[0] <= OP.C16[0])) return (scriptBytes[0].toPositiveInt() - (OP.C1[0].toPositiveInt() - 1)).toLong()
    // TODO implement
    return null
    // throw UnimplementedException("bigger scriptnum parsing is not implemented")
}

fun scriptDataFrom(scriptBytes: ByteArray): ByteArray?
{
    var i = 0
    val cmd = scriptBytes[i]
    if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
    {
        val len = cmd.toInt()
        val slice = scriptBytes.sliceArray(range(i + 1, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA1[0])
    {
        val len = scriptBytes[i + 1].toInt()
        val slice = scriptBytes.sliceArray(range(i + 2, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA2[0])
    {
        assert(false)
    }
    else if (cmd == OP.PUSHDATA4[0])
    {
        assert(false)
    }

    return null
}


fun NexaScript(chainSelector: ChainSelector):SatoshiScript
{
    val ret = SatoshiScript(chainSelector)
    ret.type = SatoshiScript.Type.TEMPLATE
    return ret
}

@cli(Display.Simple, "Scripts constrain spending an output or satisfy other scripts' constraints")
class SatoshiScript(val chainSelector: ChainSelector) : BCHserializable
{
    enum class Type(val v: Byte)
    {
        SATOSCRIPT(0),
        TEMPLATE(1),
        PUSH_ONLY(2),

        UNKNOWN(-1);

        companion object
        {
            private val map = values().associateBy(Type::v)
            fun fromByte(type: Byte) = map[type]
        }
    }
    var type: Type = Type.SATOSCRIPT
    var data = MutableList<ByteArray>(0, { _ -> ByteArray(0) })

    // Construction
    @cli(Display.User, "Construct a script from a hex string")
    constructor(chainSelector: ChainSelector, script: String, typ: Type = Type.SATOSCRIPT) : this(chainSelector)
    {
        data.add(element = script.fromHex())
        type = typ
    }

    @cli(Display.User, "Construct a script from a list of raw bytes")
    constructor(chainSelector: ChainSelector, exactBytes: MutableList<ByteArray>, typ: Type = Type.SATOSCRIPT) : this(chainSelector)
    {
        for (b in exactBytes)
        {
            data.add(b)
        }
        type = typ
    }

    @cli(Display.User, "Construct a script from raw bytes")
    constructor(chainSelector: ChainSelector, typ: Type, vararg instructions: ByteArray) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i)
        }
        type = typ
    }

    @cli(Display.User, "Construct a script from individual instructions")
    constructor(chainSelector: ChainSelector, typ: Type, vararg instructions: OP) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i.v)
        }
        type = typ
    }

    /** The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script */
    @cli(Display.User, "The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script")
    fun scriptHash(): Hash256
    {
        return Hash256(Hash.sha256(BCHserialize(SerializationType.SCRIPTHASH).flatten()))
    }

    @cli(Display.User, "The RIPEMD160 of the SHA256 hash of this script.  Used by P2SH scripts")
    fun scriptHash160(): ByteArray
    {
        return Hash.hash160(BCHserialize(SerializationType.SCRIPTHASH).flatten())
    }


    @cli(Display.User, "Are 2 scripts the same?")
    infix fun contentEquals(other: SatoshiScript): Boolean
    {
        val a = flatten()
        val b = other.flatten()
        return a.contentEquals(b)
    }

    override fun equals(other: Any?): Boolean
    {
        if (other !is SatoshiScript) return false
        return this.contentEquals(other)
    }

    @cli(Display.User, "The length of this script in bytes")
    val size: Int
        get() = this.flatten().size

    @cli(Display.User, "Make a copy of this script")
    fun copy(): SatoshiScript
    {
        return SatoshiScript(chainSelector, type, this.flatten().clone())
    }

    @cli(Display.User, "Append raw script bytes to this script, returning a new script.  Note! Use  '+ OP.push(...)' to push data into a script")
    operator fun plus(rawscript: ByteArray): SatoshiScript
    {
        var ret = this.copy()
        ret.add(rawscript)
        return ret
    }

    @cli(Display.User, "concatenate a script to the end of this one, returning the joined script")
    operator fun plus(script: SatoshiScript): SatoshiScript
    {
        var ret = this.copy()
        ret.add(script.flatten())
        return ret
    }

    // Composition
    @cli(Display.User, "Append a new instruction")
    fun add(cmd: Byte): SatoshiScript
    {
        data.add(ByteArray(1, { _ -> cmd }))
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    fun add(cmds: ByteArray): SatoshiScript
    {
        data.add(cmds)
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    fun add(cmds: List<ByteArray>): SatoshiScript
    {
        for (c in cmds)
        {
            data.add(c)
        }
        return this
    }

    // Converts the internal representation to a single ByteArray
    @cli(Display.User, "Converts the internal representation to a single ByteArray")
    fun flatten(): ByteArray
    {
        if (data.size == 0) return byteArrayOf()
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        return data[0]
    }

    // Parse is the opposite of flatten in the sense that each element in data will be 1 instruction
    fun parsed(): MutableList<ByteArray>
    {
        val scriptBytes = flatten()
        var i = 0
        val parsed = MutableList<ByteArray>(0, { _ -> ByteArray(0) })
        while (i < scriptBytes.size)
        {
            val cmd = scriptBytes[i]
            if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
            {
                val len = cmd.toInt()
                val slice = scriptBytes.sliceArray(range(i, len + 1))
                parsed.add(slice)
                i = i + len + 1
            }
            else if (cmd == OP.PUSHDATA1[0])
            {
                val j = i + 1
                val len = scriptBytes[j].toInt()
                val slice = scriptBytes.sliceArray(range(i, len + 2))
                parsed.add(slice)
                i = i + len + 2
            }
            else if (cmd == OP.PUSHDATA2[0])
            {
                assert(false)
            }
            else if (cmd == OP.PUSHDATA4[0])
            {
                assert(false)
            }
            else
            {
                parsed.add(byteArrayOf(cmd))
                i++
            }

        }
        return parsed
    }

    // Serialization
    @cli(Display.User, "Create a script from serialized data")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.User, "Overwrite this script with serialized data")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        // Type of the script is serialized whenever the script is stored to disk
        if (stream.format == SerializationType.DISK)
        {
            val sval = stream.deuint8()
            val tmp = Type.fromByte(sval.toByte())
            if (tmp == null)
            {
                throw DeserializationException("Invalid script type")
            }
            type = tmp
        }
        else type = Type.UNKNOWN  // Caller needs to set this type after this call returns (presumably based on the context the script appears in)
        data.clear()
        var len = stream.decompact()
        data.add(stream.debytes(len))
        return stream
    }

    @cli(Display.User, "Serialize this script")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var alldata: ByteArray = data.join()
        if (format == SerializationType.SCRIPTHASH)
        {
            return BCHserialized(alldata, format)
        }
        if (format == SerializationType.DISK)
            return BCHserialized.uint8(type.v) + BCHserialized.compact(alldata.size.toLong(), format) + BCHserialized(alldata, format)

        return BCHserialized.compact(alldata.size.toLong(), format) + BCHserialized(alldata, format)
    }

    @cli(Display.User, "Convert to serialized byte array (suitable for network, execution or hashing)")
    fun toByteArray(): ByteArray
    {
        return BCHserialize(SerializationType.SCRIPTHASH).flatten()
    }


    data class MatchResult(val type: PayAddressType, val params: MutableList<ByteArray>, val grouped: Boolean = false)
    {
    }

    @cli(Display.User, "Parse common script types (P2PKH and P2SH) and return useful data")
    fun match(): MatchResult?
    {
        matches(P2PKHtemplate)?.run { return MatchResult(PayAddressType.P2PKH, this) }
        matches(P2SHtemplate)?.run { return MatchResult(PayAddressType.P2SH, this) }
        matches(P2PKTtemplate)?.run { return MatchResult(PayAddressType.P2PKH, this, true) }
        return null
    }

    fun replace(mapper: (ByteArray) -> SatoshiScript): SatoshiScript
    {
        val script = flatten()
        var ret = SatoshiScript(chainSelector)
        ret.type = type

        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH[0])
            {
                ret += mapper(OP.TMPL_PUBKEYHASH)
                i++

            }
            else if (script[i] == OP.TMPL_SCRIPT[0])
            {
                ret += mapper(OP.TMPL_SCRIPT)
                i++
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1[0])
            {
                i++
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                ret.add(script[i])
                i++
            }
        }
        return ret
    }

    // Returns true if this script has TMPL parameters in it (its not a valid executable script yet).
    fun parameterized(): Boolean
    {
        val script = flatten()
        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH[0])
            {
                return true
            }
            else if (script[i] == OP.TMPL_SCRIPT[0])
            {
                return true
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1[0])
            {
                i++
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                i++
            }
        }
        return false
    }

    @cli(Display.Dev, "If this is a P2SH satisfier script, get its redeem script.\n" +
      "Warning, this just returns the last script data push as a script, so if this is NOT a P2SH satisfier script, you will get weird results.")
    fun getRedeemFromSatisfier(): SatoshiScript
    {
        val satisfier = parsed()
        val redeemRaw = scriptDataFrom(satisfier[satisfier.size - 1]) ?: byteArrayOf()
        val redeemScript = SatoshiScript(chainSelector, Type.SATOSCRIPT, redeemRaw)
        return redeemScript
    }

    @cli(Display.Dev, "Attempt to match this script (or just the beginning of it) to a template")
    fun matches(template: SatoshiScript, prefix: Boolean = false): MutableList<ByteArray>?
    {
        val script = parsed()
        val tmpl = template.parsed()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            val scrInst = OP.parse(script[i])  // Parse the instructions
            val tmplInst = OP.parse(tmpl[ti])

            if (scrInst.number != null)
            {
                i+=1
                ti+=1
                // If the script has a number, expect the template has the same number or a placeholder for any number
                if ((tmplInst.inst contentEquals OP.TMPL_SMALLINTEGER) || (tmplInst.inst contentEquals OP.TMPL_DATA))
                {
                    ret.add(byteArrayOf())  // todo figure out what to do with numbers
                    //ret.add(scrInst.data)
                    continue
                }
                else if (tmplInst.number == scrInst.number) continue

            }
            else if (scrInst.data != null)
            {
                i+=1
                ti+=1
                if (tmplInst.inst contentEquals OP.TMPL_PUBKEYHASH)
                {
                    if (scrInst.data.size != 20) return null  // Pubkeyhash must be 20 bytes
                    ret.add(scrInst.data)
                    continue
                }

                if ((tmplInst.inst contentEquals OP.TMPL_DATA) ||
                 (tmplInst.inst contentEquals OP.TMPL_BIGINTEGER) ||
                 (tmplInst.inst contentEquals OP.TMPL_SCRIPT) ||
                 (tmplInst.inst contentEquals OP.TMPL_PUBKEY))
                {
                    ret.add(scrInst.data)
                    continue
                }
                // TODO: PUBKEYS is trickier because it can match multiple data items but must match at least 1

                // the data in the template must match the script (but the script can have additional data)
                if ((tmplInst.data != null) && (scrInst.data.size >= tmplInst.data.size))
                  if (tmplInst.data contentEquals scrInst.data.sliceArray(0 until tmplInst.data.size)) continue
            }
            else
            {
                i+=1
                ti+=1
                if (tmplInst.inst contentEquals scrInst.inst) continue  // Instructions match
            }

            // Didn't successfully match so abort
            return null
        }

        if (prefix && (ti == tmpl.size)) return ret  // if looking for a prefix, just check that the template ended
        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }

    @cli(Display.Dev, "Attempt to match this script (or just the beginning of it) to a template")
    fun matchesByBytes(template: SatoshiScript, prefix: Boolean = false): MutableList<ByteArray>?
    {
        val script = flatten()
        val tmpl = template.flatten()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            if (tmpl[ti] == script[i])
            {
                i += 1; ti += 1; continue
            }
            if (tmpl[ti] == OP.TMPL_PUBKEYHASH[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }
            if (tmpl[ti] == OP.TMPL_DATA[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }
                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }

            // TODO the other template items
            return null
        }

        if (prefix && (ti == tmpl.size)) return ret  // if looking for a prefix, just check that the template ended
        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }


    // Analysis
    @cli(Display.User, """Parse common script types (P2PKH, P2SH, script template) and return the address or null if the script is not standard.  
        This returns the minimum spendable address extractable from this script.  That is, Group annotations are removed""")
    val address: PayAddress? // nullable because its possible that I can't understand this script or that it is an anyone-can-pay (has no constraint)
        get()
        {
            if (type==Type.TEMPLATE)
            {
                val tp = parseTemplate(-1)  // We don't care about a fenced group amount since we are going to ignore group data anyway, so pass a bogus number for the native amount
                if (tp == null) return null
                if (tp.wellKnownId == 1L)  // 1 is well known template P2PKT
                    return PayAddress(chainSelector, PayAddressType.P2PKT, tp.p2pkt(chainSelector).asSerializedByteArray())
                else
                    return PayAddress(chainSelector, PayAddressType.TEMPLATE, tp.p2t(chainSelector).asSerializedByteArray())
            }
            else
            {
                matches(P2PKHtemplate)?.run {
                    return PayAddress(chainSelector, PayAddressType.P2PKH, this[0])
                }

                matches(P2SHtemplate)?.run {
                    return PayAddress(chainSelector, PayAddressType.P2SH, this[0])
                }
            }
            return null
        }

    // turn a byte array (from the script stack) into a group amount
    @kotlin.ExperimentalUnsignedTypes
    fun decodeGroupAmount(ser: ByteArray): Long?
    {
        if (!((ser.size == 2) || (ser.size == 4) || (ser.size == 8))) return null  // amount size is incorrect
        if (ser.size == 2)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8)).toLong()
        }
        if (ser.size == 4)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8) + (ser[2].toULong() shl 16) + (ser[3].toULong() shl 24)).toLong()
        }
        if (ser.size == 8)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8) + (ser[2].toULong() shl 16) + (ser[3].toULong() shl 24) +
              (ser[4].toULong() shl 32) + (ser[5].toULong() shl 40) + (ser[6].toULong() shl 48) + (ser[7].toULong() shl 56)
              ).toLong()
        }
        return null
    }

    @cli(Display.User, "Return the Group annotation associated with this script, or null if ungrouped")
    @kotlin.ExperimentalUnsignedTypes
    fun groupInfo(nativeAmount: Long): GroupInfo?
    {
        if (type != Type.TEMPLATE) return null

        val parameters = parsed()
        if (parameters[0] == OP.C0)  // OP0 means no group
            return null
        val gid = scriptDataFrom(parameters[0])
        if (gid == null) return null
        if (gid.size < GroupId.GROUP_ID_MIN_SIZE) return null  // TODO return something indicating illegal script?
        if (gid.size > GroupId.GROUP_ID_MAX_SIZE) return null
        val gamt = parameters[1]  // group amount is a pushdata of 2, 4, or 8 bytes
        OP.isPushData0(gamt[0]).let {
            if (!((it == 2) || (it == 4) || (it == 8))) return null
        }
        var amt = decodeGroupAmount(gamt.drop(1).toByteArray()) ?: return null
        var authority = 0.toULong()
        val groupId = GroupId(gid)
        if (amt < 0)   // Handle authority flags
        {
            authority = amt.toULong()
            amt = 0
        }
        else
        {
            if (groupId.isFenced())
            {
                if (amt != 0L) return null // return something indicating illegal script?
                amt = nativeAmount
            }
        }
        return GroupInfo(GroupId(gid), amt, authority)
    }

    @cli(Display.User, "Given an output script in template format, parse its data items")
    @kotlin.ExperimentalUnsignedTypes
    fun parseTemplate(nativeAmount: Long): ScriptTemplate?
    {
        if (type != Type.TEMPLATE) return null

        try
        {
            var curParam = 0
            val parameters = parsed()
            val groupInfo: GroupInfo? = if (parameters[curParam] contentEquals OP.C0)  // OP0 means no group
            {
                curParam++
                null
            }
            else
            {
                curParam += 2
                groupInfo(nativeAmount)
            }

            var wellKnownId: Long? = null
            val templateHash = scriptDataFrom(parameters[curParam])
            if (templateHash != null)
            {
                // bad hash size
                if (!((templateHash.size == 20) || (templateHash.size == 32))) return null
            }
            else
            {
                wellKnownId = scriptNumFrom(parameters[curParam])
            }

            curParam++
            val argsHash = scriptDataFrom(parameters[curParam])
            if (argsHash == null) return null
            curParam++
            return ScriptTemplate(groupInfo, templateHash, wellKnownId, argsHash, parameters.drop(curParam))
        }
        catch (e: IndexOutOfBoundsException)  // If the template script is incorectly formatted, we can't return information about it
        {
            return null
        }
    }


    @cli(Display.User, "Return the P2SH constraint (output) script corresponding to this redeem script")
    fun P2SHconstraint(): SatoshiScript
    {
        return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.HASH160, OP.push(scriptHash160()), OP.EQUAL)
    }

    @cli(Display.User, "Return the P2SH part of the satisfier (input) script corresponding to this redeem script.  You must subsequently append a script that actually satisfies the redeem script. ")
    fun P2SHsatisfier(): SatoshiScript
    {
        return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.push(toByteArray()))
    }

    @cli(Display.Simple, "Convert to hex notation")
    fun toHex(): String
    {
        return flatten().toHex()
    }

    @cli(Display.Simple, "Convert to assembly code")
    fun toAsm(separator: String = " "): String
    {
        val ps = parsed()  // break into instructions
        val ret = java.util.StringJoiner(separator)
        for (i in ps)
        {
            ret.add(OP.toAsm(i))
        }
        return ret.toString()
    }

    @cli(Display.User, ("Take a copy of the first pushed stack item as it "
      + "would appear on the stack, returning the copy and the range copied "
      + "from. Throws if first OP is not a PUSH operation."))
    fun copyFront(): Pair<ByteArray, IntRange>
    {
        return copyStackElementAt(0)
    }

    @cli(Display.User, ("Take a copy of stack element at offset as it would "
      + "appear on the stack, returning the copy and the range copied from. "
      + "Offset MUST point to a PUSH operation."))
    fun copyStackElementAt(offset: Int): Pair<ByteArray, IntRange>
    {
        val script = flatten()
        var i = offset
        val pushOP = script.getOrNull(i++) ?: throw IllegalArgumentException("Offset $offset is out of bounds. Size of script is ${script.size}.")
        OP.getDirectPush(pushOP)?.let {
            return Pair(it, IntRange(offset, offset + it.size))
        }
        if (OP.isPushData0(pushOP) != 0)
        {
            val range = IntRange(i, i + pushOP.toPositiveInt() - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA1[0])
        {
            val stackItemLen = script[i++].toPositiveInt()
            val range = IntRange(i, i + stackItemLen - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA2[0])
        {
            val stackItemLen: ByteArray = script.copyOfRange(i++, ++i)

            val stackItemLenBE = (
              (stackItemLen[1].toUint() shl 8) or stackItemLen[0].toUint())

            val range = IntRange(i, i + stackItemLenBE.toInt() - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA4[0])
        {
            TODO("PUSHDATA4")
        }
        throw IllegalArgumentException("Element at offset ${offset} is not a PUSH operation (found ${pushOP.toPositiveInt()})")
    }

    /** wraps this script into a serialized byte array.  That is, a byte array of <compact int script length><script bytes> */
    fun asSerializedByteArray(st: SerializationType = SerializationType.UNKNOWN):ByteArray
    {
        return (BCHserialized(st) + variableSized(flatten())).flatten()
    }

    override fun toString(): String = when (type)
    {
        Type.SATOSCRIPT -> "SatoScript(\"" + toAsm() + "\")"
        Type.TEMPLATE ->
        {
            val ret = StringBuilder()
            ret.append("TemplateScript")
            val gi = groupInfo(0)  // not going to use the native amount anyway
            if (gi != null)
            {
                ret.append(" " + gi + " ")
            }
            ret.append("(\"" + toAsm() + "\")")
            ret.toString()
        }
        Type.PUSH_ONLY -> "DataScript(\"" + toAsm() + "\")"
        else -> "Script(\"" + toAsm() + "\")"
    }

    // Execution
    companion object
    {
        // Right now, the chain is ignored in these templates since multiple blockchains support the same script templates
        val P2PKHtemplate = SatoshiScript(ChainSelector.NEXA, Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUALVERIFY, OP.CHECKSIG)
        val P2SHtemplate = SatoshiScript(ChainSelector.BCH, Type.SATOSCRIPT, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUAL)
        // Pay to pub key template
        val P2PKTtemplate = SatoshiScript(ChainSelector.NEXA, Type.TEMPLATE, OP.TMPL_DATA, OP.TMPL_DATA,OP.TMPL_DATA)  // TODO
        /*  TODO Identify template contract output
        // These do not match that the group data is the correct size so more checking is needed
        val GroupPrefixTemplate = BCHscript(ChainSelector.BCHMAINNET, OP.TMPL_DATA, OP.TMPL_DATA, OP.GROUP)
        val GroupP2PKHtemplate = GroupPrefixTemplate + P2PKHtemplate
        val GroupP2SHtemplate = GroupPrefixTemplate + P2SHtemplate

        init
        {
            GroupP2PKHtemplate.flatten()
            GroupP2SHtemplate.flatten()
        }
         */

        val asm2Bin:MutableMap<String, OP> = mutableMapOf()
        @cli(Display.User, "Compile text asm into a script")
        fun fromAsm(asm: Array<String>, chainSelector: ChainSelector = ChainSelector.NEXA): SatoshiScript
        {
            val prog = SatoshiScript(chainSelector)
            if (asm2Bin.size == 0)  // populate the first time
            {
                for (i in 0 .. 256)
                {
                    val tgt = byteArrayOf(i.toByte())
                    val asm2 = OP.toAsm(tgt)
                    if (!asm2.startsWith("0x"))
                    {
                        asm2Bin[asm2] = OP(tgt)
                    }
                }
                // Now add a few hard-coded synonyms
                asm2Bin["FALSE"] = OP(OP.FALSE)
                asm2Bin["TRUE"] = OP(OP.TRUE)
                asm2Bin["-1"] = OP(OP.CNEG1)
            }

            for(i in asm)
            {
                if (i.uppercase() in asm2Bin)
                {
                    prog.add(asm2Bin[i.uppercase()]!!.v)
                }
                else
                {
                    var str=i
                    println(str)
                    if (str.startsWith("0x") || str.startsWith("0X"))
                    {
                        str = str.drop(2)  // Remove the prefix
                        val pushdata: ByteArray = str.fromHex()
                        println(pushdata.size)
                        println(pushdata.toHex())
                        prog.add(OP.push(pushdata))
                    }
                    else
                    {
                        var v = i.toLongOrNull()  // Try to convert to a decimal number
                        if (v != null)
                        {
                            prog.add(OP.push(v))
                        }
                        else
                        {
                            throw ScriptException("instruction not understood: $i", "bad $i")
                        }
                    }
                }
            }
            return prog
        }

        @cli(Display.Simple, "Create a pay-to-public-key-hash constraint script")
        fun p2pkh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.NEXA): SatoshiScript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2pkh is unspendable
            return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.DUP, OP.HASH160, OP.push(rawAddr), OP.EQUALVERIFY, OP.CHECKSIG)
        }

        @cli(Display.Simple, "Create a BCH pay-to-script-hash constraint script")
        fun p2sh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCH): SatoshiScript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2sh is unspendable
            return SatoshiScript(chainSelector, Type.SATOSCRIPT, OP.HASH160, OP.push(rawAddr), OP.EQUAL)
        }

        @cli(Display.Simple, "Create a grouped pay-to-public-key-hash constraint script.")
        fun gp2pkt(chainSelector: ChainSelector, grpId: GroupId, tokenAmt: Long, pubkey: ByteArray): SatoshiScript
        {
            val gid = grpId.toByteArray()
            // A GroupId object programatically can't actually be an invalid group (if you stay within the GroupId API) so these checks are for misbehaving code
            if (gid.size == 0) return ungroupedP2pkt(chainSelector, pubkey)
            assert(gid.size >= 32)  // GroupIds must be 32 bytes or more
            if (!chainSelector.hasGroupTokens) throw UnsupportedInBlockchain("group tokens")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = Hash.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(gid), OP.groupAmount(tokenAmt), P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a grouped output script prefix (you need to append the script hash, args hash, etc to create a correct output script template).")
        fun grouped(chainSelector: ChainSelector, grpId: GroupId, tokenAmt: Long): SatoshiScript
        {
            val gid = grpId.toByteArray()
            if (!chainSelector.hasGroupTokens) throw UnsupportedInBlockchain("group tokens")
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(gid), OP.groupAmount(tokenAmt))
        }

        @cli(Display.Simple, "Create a pay-to-public-key-template output script suffix -- group information must be prepended")
        fun p2pktSuffix(chainSelector: ChainSelector, pubkey: ByteArray): SatoshiScript
        {
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = Hash.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a raw nexa coin (no group) pay-to-public-key-template output script")
        fun ungroupedP2pkt(chainSelector: ChainSelector, pubkey: ByteArray): SatoshiScript
        {
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            val constraintArgsScript = SatoshiScript(chainSelector, Type.PUSH_ONLY, OP.push(pubkey))
            val constraintArgsHash = Hash.hash160(constraintArgsScript.toByteArray())
            return SatoshiScript(chainSelector, Type.TEMPLATE, OP.FALSE, P2PKT_ID, OP.push(constraintArgsHash))
        }

        @cli(Display.Simple, "Create a pay-to-template output script. If grpId is null, or not provided, nexa coin is used")
        fun p2t(chainSelector: ChainSelector, templateScriptHash: ByteArray, constraintArgsHash: ByteArray, constraintPublicArgs: SatoshiScript?=null, grpId: GroupId? = null, tokenAmt: Long? = null): SatoshiScript
        {
            if ((grpId == null) xor (tokenAmt == null)) throw ScriptException("invalid args combination.  Must provide both group and amount or neither")
            if (!chainSelector.hasTemplates) throw UnsupportedInBlockchain("templates")
            assert(templateScriptHash.size == 20 || templateScriptHash.size == 32)
            assert(constraintArgsHash.size == 20 || constraintArgsHash.size == 32)
            val ret = if (grpId != null)
            {
                SatoshiScript(chainSelector, Type.TEMPLATE, OP.push(grpId.toByteArray()), OP.groupAmount(tokenAmt!!), OP.push(templateScriptHash), OP.push(constraintArgsHash))
            }
            else
            {
                SatoshiScript(chainSelector, Type.TEMPLATE, OP.FALSE, OP.push(templateScriptHash), OP.push(constraintArgsHash))
            }
            if (constraintPublicArgs != null) ret.add(constraintPublicArgs.flatten())
            return ret
        }

    }


}

//* Well known template type identifier: pay-to-public-key-template
val P2PKT_ID = OP.C1