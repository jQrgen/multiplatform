// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android


import info.bitcoinunlimited.multiplatform.UnimplementedException
import info.bitcoinunlimited.multiplatform.platform.Logger
import java.io.InputStream
import java.io.OutputStream
import java.net.InetSocketAddress
import java.net.Socket

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import javax.net.ssl.SSLSocketFactory
import kotlin.concurrent.thread

open class ElectrumException(msg: String) : NetException(msg)

/** Electrum server never replied */
class ElectrumRequestTimeout() : ElectrumException("timeout")

/** The reply doesn't adhere to the protocol in some manner */
class ElectrumIncorrectReply(what: String) : ElectrumException(what)

/** A passed entity was not found (transaction, block, scripthash, etc). */
class ElectrumNotFound(msg: String) : ElectrumException(msg)

/** The request was not accepted by the server */
class ElectrumIncorrectRequest(what: String) : ElectrumException(what)

private val TAG = "BU.electrumclient"

const val HEADER_SIZE_BYTES = 80
const val DEFAULT_TCP_ELECTRUM_PORT = 50001
const val DEFAULT_SSL_ELECTRUM_PORT = 50002
const val DEFAULT_NEXA_TCP_ELECTRUM_PORT = 20001
const val DEFAULT_NEXA_SSL_ELECTRUM_PORT = 20002
const val DEFAULT_NEXATEST_TCP_ELECTRUM_PORT = 30001
const val DEFAULT_NEXATEST_SSL_ELECTRUM_PORT = 30002

const val DEFAULT_TCP_ELECTRUM_PORT_REGTEST = 60401

const val ELECTRUM_INVALID_PARAMS = -32602

/** Open a JSON-RPC over TCP connection to an Electrum X server */
class ElectrumClient(val chainSelector: ChainSelector, name: String, port: Int = DEFAULT_NEXA_SSL_ELECTRUM_PORT, logName: String = name + ":" + port, autostart: Boolean = true, useSSL:Boolean=true):
  JsonRpc(name, port, logName, useSSL = useSSL)
{
    // This protocol is specified at:  https://bitcoincash.network/electrum

    @Serializable
    data class ErrorCodeMessage(val code: Int, val message: String)

    @Serializable
    data class ErrorCodeReply(val error: ErrorCodeMessage, val id: Int, val jsonrpc: String)

    @Serializable
    data class ErrorReply(val error: String)

    @Serializable
    data class VersionReply(val result: List<String>)

    @Serializable
    data class StringReply(val result: String)

    @Serializable
    data class HeadersResult(val count: Int, val hex: String, val max: Int)

    @Serializable
    data class HeadersReply(val result: HeadersResult)

    @Serializable
    data class HeaderResult(val height: Long, val hex: String)

    @Serializable
    data class HeaderReply(val result: HeaderResult)

    @Serializable
    data class HeaderNotification(val params: Array<HeaderResult>)

    @Serializable
    data class GetHistoryResult(val height: Int, val tx_hash: String)

    @Serializable
    data class GetHistoryReply(val result: Array<GetHistoryResult>)

    @Serializable
    data class FeaturesResult(val genesis_hash: String, val hash_function: String, val server_version: String, val protocol_max: String, val protocol_min: String, val pruning: Int? = null)

    @Serializable
    data class FeaturesReply(val result: FeaturesResult)

    @Serializable
    data class FirstUseResult(val block_hash: String? = null, val block_height: Int? = null, val tx_hash: String? = null)

    @Serializable
    data class FirstUseReply(val result: FirstUseResult)

    @Serializable
    data class BalanceResult(val confirmed: Int = 0, val unconfirmed: Int = 0)

    @Serializable
    data class BalanceReply(val result: BalanceResult)

    @Serializable
    data class GetUtxoSpentInfo(val height: Int?, val tx_hash: String?, val tx_pos: Int?)

    @Serializable
    data class GetUtxoResult(val amount: Long, val height: Int, val scripthash: String, val status: String, val spent: GetUtxoSpentInfo)

    @Serializable
    data class GetUtxoReply(val result: GetUtxoResult)

    @Serializable
    data class ListUnspentResult(
      val height: Int,
      val tx_pos: Int,
      val tx_hash: String,
      val value: Long)

    @Serializable
    data class ListUnspentReply(val result: Array<ListUnspentResult>)


    /** The version this electrum client supports */
    val MY_VERSION = listOf("4.0.1", "1.4")

    /** how long to wait before giving up on a request */
    var requestTimeout: Int = 5000

    init
    {
        if (autostart) start()
    }

    /** Get the server version */
    fun version(timeoutInMs: Int = requestTimeout): Pair<String, String>
    {
        val ret = call("server.version", MY_VERSION, timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        val parsed = json.decodeFromString(VersionReply.serializer(), ret)
        return Pair(parsed.result[0], parsed.result[1])
    }

    fun features(timeoutInMs: Int = requestTimeout): FeaturesResult
    {
        val ret = call("server.features", MY_VERSION, timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        Logger.info(TAG,ret.toString())
        val parsed = json.decodeFromString(FeaturesReply.serializer(), ret)
        return parsed.result
    }

    /* Given an outpoint (txid and index) returns information about that UTXO, including whether it is spent */
    fun getUtxo(hexTxHash: String, outIdx: Int, timeoutInMs: Int = requestTimeout): GetUtxoResult
    {
        val ret = call("blockchain.utxo.get", listOf(hexTxHash, outIdx), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        Logger.info(TAG,ret.toString())
        try
        {
            val parsed = json.decodeFromString(GetUtxoReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"getUtxo error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("UTXO not found")
            throw ElectrumIncorrectRequest("getUtxo error")  // Do not expose the untrusted server's error message to higher layers so it is not accidentally shown to end users
        }
    }

    fun getUtxo(inp: iTxOutpoint?, timeoutInMs: Int = requestTimeout):iTxOutput
    {
        // TODO electrum
        //getUtxo(inp.txid.toHex(), inp.idx.toInt(), timeoutInMs)
        throw UnimplementedException("electrum protocol getUtxo")
    }
    fun getUtxo(inp: NexaTxInput, timeoutInMs: Int = requestTimeout) = getUtxo(inp.spendable.outpoint, timeoutInMs)

    fun getFirstUse(script: SatoshiScript, timeoutInMs: Int = requestTimeout): FirstUseResult = getFirstUse(script.scriptHash(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getFirstUse(scriptHash: Hash256, timeoutInMs: Int = requestTimeout): FirstUseResult = getFirstUse(scriptHash.toHex(), timeoutInMs)

    fun getFirstUse(scriptHash: String, timeoutInMs: Int = requestTimeout): FirstUseResult
    {
        val ret = call("blockchain.scripthash.get_first_use", listOf(scriptHash), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        Logger.info(TAG,ret.toString())
        try
        {
            val parsed = json.decodeFromString(FirstUseReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"getfirstuse error string: " + parsed.error.message)
            if (parsed.error.code == -32004) throw ElectrumNotFound("Scripthash not found")
            throw ElectrumIncorrectRequest("getFirstUse error")
        }
    }

    /** Get the serialized header for the block at the supplied height
     * @param height The height of the desired block header
     * @param serialized header bytes or an empty array if there is no block at that height */
    fun getHeader(height: Int, timeoutInMs: Int = requestTimeout): ByteArray
    {
        val ret = call("blockchain.block.header", listOf(height, 0), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return parsed.result.fromHex()
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"getheader error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getheader error")
        }
    }

    /** Return serialized headers for the blocks beginning at the supplied height
     * @param height The height of the first block header to return
     * @param count The number of block headers to return
     * @param timeoutInMs (optional) Override the default request timeout
     * @return A list of serialized block headers.  The list will contain fewer than count headers if count exceeds the maximum number of headers this server supports or if the chain has no more headers
     * */
    fun getHeaders(height: Int, count: Int, timeoutInMs: Int = requestTimeout): List<ByteArray>
    {
        val ret = call("blockchain.block.headers", listOf(height, count, 0), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        val parsed = json.decodeFromString(HeadersReply.serializer(), ret)
        //Logger.info(TAG,parsed.toString())
        val arr = mutableListOf<ByteArray>()
        if (parsed.result.hex.length != parsed.result.count * HEADER_SIZE_BYTES * 2) throw ElectrumIncorrectReply("Header count does not match data size")
        for (i in 0 until parsed.result.count)
        {
            val slic = parsed.result.hex.slice(range(i * HEADER_SIZE_BYTES * 2, HEADER_SIZE_BYTES * 2))
            arr.add(slic.fromHex())
        }
        return arr
    }

    /** Get the transaction hash at the specified block height and offset.
     * @param height The height of the block that contains the desired transaction
     * @param idx The 0-based index of the transaction in the block
     * @param blockMerkleRoot (optional) If provided, this API will request the merkle proof of this transaction and throw ElectrumIncorrectReply if the proof is incorrect
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction hash
     * * */
    fun getTxHashAt(height: Int, idx: Int, blockMerkleRoot: Hash256? = null, timeoutInMs: Int = requestTimeout): Hash256
    {
        if (blockMerkleRoot != null)
        {
            throw UnimplementedException("Merkle proof verification of requested transactions")
        }
        else
        {
            val ret = call("blockchain.transaction.id_from_pos", listOf(height, idx, false), timeoutInMs)
            if (ret == null) throw ElectrumRequestTimeout()

            try
            {
                val parsed = json.decodeFromString(StringReply.serializer(), ret)
                return Hash256(parsed.result)
            }
            catch (e: SerializationException)
            {
                val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
                Logger.info(TAG,"getTxHashAt error string: " + parsed.error.message)
                throw ElectrumIncorrectRequest("getTxHashAt error")
            }
        }
    }

    /** Get the transaction at the specified block height and offset.  Requires multiple server calls.
     * @param height The height of the block that contains the desired transaction
     * @param idx The 0-based index of the transaction in the block
     * @param blockMerkleRoot (optional) If provided, this API will request the merkle proof of this transaction and throw ElectrumIncorrectReply if the proof is incorrect
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTxAt(height: Int, idx: Int, blockMerkleRoot: Hash256? = null, timeoutInMs: Int = requestTimeout): iTransaction
    {
        return getTx(getTxHashAt(height, idx, blockMerkleRoot, timeoutInMs))
    }

    /** Get a transaction by hash
     * @param txHash Transaction ID
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTx(txHash: Hash256, timeoutInMs: Int = requestTimeout): iTransaction = getTx(txHash.toHex(), timeoutInMs)

    /** Get a transaction by hash
     * @param txHash Transaction ID as a hex-encoded string
     * @param timeoutInMs (optional) Override the default request timeout
     * @return transaction
     */
    fun getTx(txHash: String, timeoutInMs: Int = requestTimeout): iTransaction
    {
        val ret = call("blockchain.transaction.get", listOf(txHash, false), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return txFromHex(chainSelector, parsed.result, SerializationType.NETWORK)
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"getTx error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("tx not in blockchain or mempool")
            if (parsed.error.code == 2) throw ElectrumNotFound("tx not in blockchain or mempool")  // Coming from Fulcrum 1.5.0
            throw ElectrumIncorrectRequest("getTx error: " + parsed.error.message)
        }
    }

    fun getTxOld(txHash: String, timeoutInMs: Int = requestTimeout): iTransaction
    {
        val ret = call("blockchain.transaction.get", listOf(txHash, false, false), timeoutInMs)
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return txFromHex(chainSelector, parsed.result, SerializationType.NETWORK)
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"getTx error string: " + parsed.error.message)
            if (parsed.error.code == ELECTRUM_INVALID_PARAMS) throw ElectrumNotFound("tx not in blockchain or mempool")
            throw ElectrumIncorrectRequest("getTx error: " + parsed.error.message)
        }
    }

    fun getTxDetails(txHash: String, timeoutInMs: Int = requestTimeout): JsonElement
    {
        val result = call("blockchain.transaction.get", listOf(txHash, true), timeoutInMs)
        if (result == null) throw ElectrumRequestTimeout()
        try
        {
            return json.parseToJsonElement(result)
        }
        catch (e: java.lang.Exception)
        {
            val msg = e.message ?: e.toString()
            throw ElectrumIncorrectReply(msg)
        }
    }

    /** Send out a transaction */
    fun sendTx(serializedTx: ByteArray, timeoutInMs: Int = requestTimeout): String
    {
        val ret = call("blockchain.transaction.broadcast", listOf(serializedTx.toHex()), timeoutInMs)
        Logger.info(TAG,ret ?: "nothing")
        if (ret == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(StringReply.serializer(), ret)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), ret)
            Logger.info(TAG,"sendTx error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("sendTx error")
        }
    }

    /** Get confirmed and unconfirmed activity in a script
     * @param script the relevant output script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(script: SatoshiScript, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>> = getHistory(script.scriptHash(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(scriptHash: Hash256, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>> = getHistory(scriptHash.toHex(), timeoutInMs)

    /** Get confirmed and unconfirmed activity in a script
     * @param scriptHash the hex-encoded SHA256 hash of the relevant script
     * @param timeoutInMs (optional) Override the default request timeout
     * @return array of (height, transaction hash) pairs
     */
    fun getHistory(scriptHash: String, timeoutInMs: Int = requestTimeout): Array<Pair<Int, Hash256>>
    {
        val result = call("blockchain.scripthash.get_history", listOf(scriptHash), timeoutInMs)
        if (result == null) throw ElectrumRequestTimeout()
        try
        {
            val parsed = json.decodeFromString(GetHistoryReply.serializer(), result)
            val ret = Array<Pair<Int, Hash256>>(parsed.result.size, { i: Int -> Pair(parsed.result[i].height, Hash256(parsed.result[i].tx_hash)) })
            return ret
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            Logger.info(TAG,"getheader error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getHistory error")
        }

    }

    private fun listUnspentInner(scripthash: String, timeoutInMs: Int): List<Spendable>
    {
        val result = call("blockchain.scripthash.listunspent", listOf(scripthash), timeoutInMs)
          ?: throw ElectrumRequestTimeout()

        val toUnconfirmedLength = fun(blockHeight: Int): Long
        {
            if (blockHeight > 0)
            {
                return 0
            }
            if (blockHeight == 0)
            {
                return 1
            }
            assert(blockHeight < 0)
            // We don't know the exact length of the unconfirmed chain, but it's at least 2.
            return 2
        }

        try
        {
            Logger.info(TAG,result)
            val ret = json.decodeFromString(ListUnspentReply.serializer(), result)

            assert(false) // TODO Nexa
            /*
            return ret.result.map {
                val output = Spendable(
                  chainSelector, it.tx_hash.fromHex(),
                  it.tx_pos.toLong(), it.value.toLong()
                )
                output.spendableUnconfirmed = toUnconfirmedLength(it.height)
                output
            }

             */
            return listOf()
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            Logger.info(TAG,"listUnspent error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("listUnspent error")
        }
    }

    fun getBalance(address: PayAddress, timeoutInMs: Int = requestTimeout): BalanceResult
    {
        val scriptHash = address.outputScript().scriptHash().toString()
        val result = call("blockchain.scripthash.get_balance", listOf(scriptHash), timeoutInMs)
          ?: throw ElectrumRequestTimeout()

        try
        {
            val parsed = json.decodeFromString(BalanceReply.serializer(), result)
            return parsed.result
        }
        catch (e: SerializationException)
        {
            val parsed = json.decodeFromString(ErrorCodeReply.serializer(), result)
            if (parsed.error.code == -32004) throw ElectrumNotFound(address.toString())
            Logger.info(TAG,"getBalance error string: " + parsed.error.message)
            throw ElectrumIncorrectRequest("getBalance error")
        }
    }

    /**
     * Get outputs for a pay destination you don't own.
     */
    fun listUnspent(address: PayAddress, timeoutInMs: Int = requestTimeout): List<Spendable>
    {
        val scripthash = address.outputScript().scriptHash().toString()
        return this.listUnspentInner(scripthash, timeoutInMs).map {
            it.addr = address
            it.priorOutScript = address.outputScript()
            it
        }
    }

    /**
     * Get spendable output for a pay destination you own.
     *
     * The secret in destination object will be passed on to the
     * BCHspendable instances.
     */
    fun listUnspent(destination: PayDestination, timeoutInMs: Int = requestTimeout): List<Spendable>
    {
        val scripthash = destination.ungroupedOutputScript().scriptHash().toString()
        return this.listUnspentInner(scripthash, timeoutInMs).map { output ->
            output.secret = destination.secret
            output.addr = destination.address
            output.addr?.let {
                output.priorOutScript = it.outputScript()
            }
            output.redeemScript = destination.redeemScript()

            // TODO: What is out.commitHeight? Can we store destination.height in it?
            // TODO: What is out.commitTxHash? Can we store destination.tx_hash in it?
            output
        }

    }

    fun ping(timeoutInMs: Int = requestTimeout)
    {
        call("server.ping", null, timeoutInMs) ?: throw ElectrumRequestTimeout()
    }

    /**
     * Subscribe to blockchain headers. Issues a callback with the blockchain tip when a new block is found.
     */
    @kotlin.ExperimentalUnsignedTypes
    fun subscribeHeaders(callback: (iBlockHeader) -> Unit)
    {
        val method = "blockchain.headers.subscribe"
        if (notifications.contains(method))
        {
            error("Only 1 header subscription at a time is supported")
        }
        subscribe(method, null) {
            if (it == null)
            {
                Logger.info(TAG,"Received 'null' header notification")
                return@subscribe
            }
            Logger.info(TAG,"Header notification: $it")
            try
            {
                try
                {
                    // First response is wrapped in 'result', so use HeaderReply
                    val parsed = json.decodeFromString(HeaderReply.serializer(), it)
                    val header = blockHeaderFromHex(chainSelector,parsed.result.hex)
                    header.height = parsed.result.height
                    callback(header)
                }
                catch (e: SerializationException)
                {
                    // Other responses are wrapped in params, use 'HeaderNotification'
                    val parsed = json.decodeFromString(HeaderNotification.serializer(), it)
                    val header = blockHeaderFromHex(chainSelector, parsed.params.last().hex)
                    header.height = parsed.params.last().height
                    callback(header)
                }
            }
            catch (e: Exception)
            {
                Logger.warning(TAG,"Error when handling block header notification: $e")
            }
        }
    }

    /**
     * Unsubscribes from blockchain headers.
     */
    fun unsubscribeHeaders()
    {
        notifications.remove("blockchain.headers.subscribe")

        // Some servers don't support unsubscribe, we don't really care about the return value either way.
        call("blockchain.headers.unsubscribe", null, requestTimeout)
    }
}

open class JsonRpc(val name: String, val port: Int, val logName: String = name + ":" + port, val useSSL: Boolean = false)
{
    private val CONNECT_TIMEOUT = 2000  //* How long to attempt to connect in milliseconds
    private val READ_BUF_SIZE = 0x40000

    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }

    var sock = if (useSSL) SSLSocketFactory.getDefault().createSocket() else Socket()
    var inp: InputStream
    var out: OutputStream

    var reqId = 0

    val outstanding: MutableMap<Int, ((String?) -> Unit)> = mutableMapOf()
    val notifications: MutableMap<String, ((String?) -> Unit)> = mutableMapOf()

    @Serializable
    data class ResultData(val id: Int, val jsonrpc: String)

    @Serializable
    data class NotificationData(val method: String)

    init
    {
        sock.connect(InetSocketAddress(name, port), CONNECT_TIMEOUT)
        inp = sock.getInputStream()
        out = sock.getOutputStream()
    }

    @Synchronized
    fun nextId(): Int
    {
        val ret = reqId
        reqId += 1
        return ret
    }

    fun call(method: String, params: List<Any>?, timeoutInMs: Int): String?
    {
        var ret: String? = null
        val wakey = ThreadCond()
        call(method, params) {
            ret = it
            wakey.wake()
        }
        wakey.delay(timeoutInMs.toLong())
        return ret
    }

    fun <T> parse(method: String, params: List<String>?, serializer: KSerializer<T>, response: (T?) -> Unit)
    {
        call(method, params) {
            if (it == null) response(null)
            else
            {
                try
                {
                    val result = json.decodeFromString<T>(serializer, it)
                    response(result)
                }
                catch (e: SerializationException)
                {
                    Logger.warning(TAG,sourceLoc() + " " + logName + ": Received unparseable reply: " + it)
                    response(null)
                }
            }
        }
    }

    @kotlin.ExperimentalUnsignedTypes
    fun subscribe(method: String, params: List<Any>? = null, response: (String?) -> Unit)
    {
        val cookie = Int.MAX_VALUE / 2 + nextId()


        val sendStr = if (params != null)
        {
            "{\"method\": \"${method}\",\"params\":${params.jsonify()},\"id\": ${cookie}}\n"
        }
        else
        {
            "{\"method\": \"${method}\",\"id\": ${cookie}}\n"
        }

        notifications[method] = response
        // The subscription call itself also responds with current state.
        outstanding[cookie] = response
        Logger.info(TAG,sourceLoc() + " " + logName + ": Issuing request:" + sendStr)
        out.write(sendStr.toByteArray())
    }

    @kotlin.ExperimentalUnsignedTypes
    fun call(method: String, params: List<Any>?, response: (String?) -> Unit)
    {
        val cookie = nextId()

        val sendStr = if (params != null)
        {
            "{\"method\": \"${method}\",\"params\":${params.jsonify()},\"id\": ${cookie}}\n"
        }
        else
        {
            "{\"method\": \"${method}\",\"id\": ${cookie}}\n"
        }

        outstanding[cookie] = response
        Logger.info(TAG,sourceLoc() + " " + logName + ": Issuing request:" + sendStr)
        out.write(sendStr.toByteArray())
    }

    fun start()
    {
        thread(true, true, null, name + "_chain") { run() }
    }

    fun stop()
    {
        close()  // thread kicks out if the socket closes

    }

    fun run()
    {
        try
        {
            val dataBuf = ByteArray(READ_BUF_SIZE)
            var priorData: String = ""
            while (true)
            {
                val amtRead = inp.read(dataBuf)
                if (amtRead > 0)
                {
                    val responses = (priorData + String(dataBuf, 0, amtRead)).split("\n")
                    if (responses.size > 0)
                    {
                        var end = responses.size - 1
                        // The last item will always be the next data fragment -- if the cut was clean then it'll be ""
                        priorData = responses[end]

                        for (idx in 0 until end)
                        {
                            val response = responses[idx]
                            Logger.info(TAG,"JSON response:" + response)
                            val parsedreply = try
                            {
                                val r = json.decodeFromString(ResultData.serializer(), response)
                                Logger.info(TAG,r.toString())
                                r
                            }
                            catch (e: SerializationException)  // Missing the "id" field -- this is probably a notification
                            {
                                val notificationReply = try
                                {
                                    json.decodeFromString(NotificationData.serializer(), response)
                                }
                                catch (e: SerializationException)
                                {
                                    Logger.warning(TAG,sourceLoc() + " " + logName + ": Received unparseable reply: " + response)
                                    null
                                }
                                if (notificationReply != null)
                                {
                                    val handler = notifications[notificationReply.method]
                                    if (handler == null)
                                    {
                                        Logger.warning(TAG,sourceLoc() + " " + logName + ": Received notification for ${notificationReply.method} that matches no subscription: " + response)
                                    }
                                    else
                                    {
                                        handler(response)
                                    }
                                }

                                null // it was a notification so parsedreply should be null
                            }

                            if (parsedreply != null)
                            {
                                val handler = outstanding[parsedreply.id]
                                if (handler == null)
                                {
                                    Logger.warning(TAG,sourceLoc() + " " + logName + ": Received reply for ${parsedreply.id} that matches no request: " + response)
                                }
                                else  // Call the handler and remove it from the outstanding list
                                {
                                    handler(response)
                                    if (parsedreply.id < Int.MAX_VALUE / 2)  // Anything above is a subscription
                                        outstanding.remove(parsedreply.id)
                                }
                            }
                        }

                    }
                }
            }
        }
        catch (e: java.io.IOException)  // connection is closed
        {
        }
        catch (e: Exception)
        {
            handleThreadException(e, sourceLoc() + " electrumClient: ")
        }

        abortAllOutstandingRequests() // Tell everyone who is expecting a reply that they aren't going to get one
    }

    fun abortAllOutstandingRequests()
    {
        for (out in outstanding)
        {
            out.value(null)
        }
        outstanding.clear()
    }

    fun close()
    {
        try
        {
            sock.shutdownInput()
        }
        catch (e: java.io.IOException)
        {
        }  // possibly already shutdown
        try
        {
            sock.shutdownOutput()
        }
        catch (e: java.io.IOException)
        {
        }
        try
        {
            sock.close()
        }
        catch (e: java.io.IOException)
        {
        }
    }

}
