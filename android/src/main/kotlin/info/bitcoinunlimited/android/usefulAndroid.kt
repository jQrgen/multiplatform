// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package info.bitcoinunlimited.android

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.net.ConnectivityManager
import android.text.Editable
import android.text.TextWatcher
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView
import info.bitcoinunlimited.multiplatform.BUException
import info.bitcoinunlimited.multiplatform.ErrorSeverity
import info.bitcoinunlimited.multiplatform.platform.Logger
import kotlinx.coroutines.*
import java.io.File

private val TAG = "BU.usefulAndroid"

val blockchainLibName = "nexandroid"

// Application that uses this library needs to set these variables
//public var appResources: Resources? = null
public var appContext: PlatformContext? = null

open class AssertException(why: String) : BUException(why, "Assertion", ErrorSeverity.Abnormal)

/** Do whatever you pass within the user interface context, asynchronously */
fun laterUI(fn: suspend () -> Unit): Unit
{
    MainScope().launch {
        try
        {
            fn()
        }
        catch (e: Exception)
        {
            Logger.warning(TAG,"Exception in laterUI: " + e.toString())
        }
    }
}


// you can install your own coroutine threads here and this common code will use that instead of GlobalScope
var notInUIscope: CoroutineScope? = null

/** execute the passed code block directly if not in the UI thread, otherwise defer it */
fun notInUI(fn: () -> Unit)
{
    val tname = Thread.currentThread().name
    if (tname == "main")  // main is the UI thread so need to launch this
    {
        (notInUIscope ?:GlobalScope).launch {
            try
            {
                fn()
            }
            catch (e: Exception)
            {
                Logger.warning(TAG,"Exception in notInUI: " + e.toString())
            }
        }
    }
    else // otherwise just call it
    {
        try
        {
            fn()
        }
        catch (e: Exception)
        {
            Logger.warning(TAG,"Exception in notInUI: " + e.toString())
        }
    }
}

fun <RET> syncNotInUI(fn: () -> RET): RET
{
    val tname = Thread.currentThread().name
    if (tname == "main")
    {
        val ret = runBlocking(Dispatchers.IO) {
            fn()
        }
        return ret
    }
    else
    {
        return fn()
    }
}

/** Connects a gui switch to a preference DB item.  To be called in onCreate.
 * Returns the current state of the preference item.
 * Uses setOnCheckedChangeListener, so you cannot call that yourself.  Instead pass your listener into this function
 * */
fun SetupBooleanPreferenceGui(key: String, db: SharedPreferences, button: CompoundButton, onChecked: ((CompoundButton?, Boolean) -> Unit)? = null): Boolean
{
    val ret = db.getBoolean(key, false)
    button.setChecked(ret)

    button.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener
    {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean)
        {
            with(db.edit())
            {
                putBoolean(key, isChecked)
                commit()
            }
            if (onChecked != null) onChecked(buttonView, isChecked)
        }
    })
    return ret
}

/** Connects a gui text entry field to a preference DB item.  To be called in onCreate */
fun SetupTextPreferenceGui(key: String, db: SharedPreferences, view: EditText)
{
    view.text.clear()
    view.text.append(db.getString(key, ""))

    view.addTextChangedListener(object : TextWatcher
    {
        override fun afterTextChanged(p: Editable?)
        {
            dbgAssertGuiThread()
            if (p == null) return
            val text = p.toString()
            with(db.edit())
            {
                putString(key, text)
                commit()
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
        {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
        {
        }
    })
}

class TextViewReactor<T>(public val gui: TextView) : Reactor<T>()
{
    override fun change(obj: Reactive<T>)
    {
        val v = obj.access()
        if (v != null)
        {
            val vstr = v.first.toString()
            laterUI {
                if (vstr != gui.text)
                    gui.text = v.first.toString()
            }
        }
        else
        {
            // We already saw this change
            // LogIt.info(sourceLoc() +": Rewrite GUI change skipped")
        }
    }
}

fun RunningTheTests(): Boolean
{
    try
    {
        /* I can search for either UnitTest or GuiTest here because both are included in the
        test image.
         */
        Class.forName("bitcoinunlimited.wally.androidTestImplementation.UnitTest")
        return true
    }
    catch (e: ClassNotFoundException)
    {
        return false
    }
}


fun dbgAssertGuiThread()
{
    val tname = Thread.currentThread().name
    if (tname != "main")
    {
        Logger.warning(TAG,"ASSERT GUI operations in thread " + tname)
        throw AssertException("Executing GUI operations in thread " + tname)
    }
}

fun dbgAssertNotGuiThread()
{
    val tname = Thread.currentThread().name
    if (tname == "main")
    {
        Logger.warning(TAG,"ASSERT blocking operations in GUI thread " + tname)
        throw AssertException("Executing blocking operations in GUI thread " + tname)
    }
}
// String translation and display

// Match the API with usefulJvm.kt
class PlatformContext(val context: Context)  // context is ANDROID only -- do not use except in the android only portions of the code!
{
    val filesDir: File?
        get() = context.filesDir

    fun deleteDatabase(path: String)
    {
        context.deleteDatabase(path)
    }

    fun iHaveInternet(): Boolean
    {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetwork != null
    }
}


fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit)
{
    this.addTextChangedListener(object : TextWatcher
    {
        override fun afterTextChanged(s: Editable?)
        {
            afterTextChanged.invoke(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int)
        {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
        {
        }
    })
}

fun EditText.validate(validator: (String) -> Boolean, message: String)
{
    this.afterTextChanged {
        this.error = if (validator(it)) null else message
    }
    this.error = if (validator(this.text.toString())) null else message
}


