buildscript {
    val kotlinVersion by extra("1.7.21")
    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.21")
        classpath("com.android.tools.build:gradle:7.1.3")
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlinVersion)
    }
}

group = "info.bitcoinunlimited"
version = "1.0.0"

allprojects {
    repositories {
        mavenCentral()
    }
}